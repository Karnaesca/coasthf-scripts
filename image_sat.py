import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

import load_copernicus as coper
import read_geojson

df_loc = read_geojson.from_Point_FeatureCollection(read_geojson.fname_estuaries_loc)
zones = read_geojson.build_zones(df_loc)
names = df_loc.name.values

satds = coper.SatDataSet()

#timeframe = pd.to_datetime(["2021-09-01","2021-10-01"])
timeframe = pd.date_range(*["2020-01-28","2020-01-28"],freq="1D")

norm = Normalize(0,50)

index = 5

for date in timeframe:
    chunk = satds.getchunk("TUR", *zones[index],date)
    
    if np.sum(~np.isnan(chunk.values.flatten())) > 0:
        fig, ax = plt.subplots()
        m1 = ax.matshow(chunk,norm=norm)
        fig.suptitle(f"{names[index]} - {date}")
        fig.colorbar(m1)

