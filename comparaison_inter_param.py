# logs
execfile("/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/logs/HEAD.py")
# imports
import pandas as pd
import read_ctd as ctd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
from matplotlib import cm
from matplotlib.colors import Normalize

# https://matplotlib.org/stable/gallery/statistics/confidence_ellipse.html#sphx-glr-gallery-statistics-confidence-ellipse-py
#temperature_degc salinity fluorescence_rfu turbidity_ntu oxygen_mgl mdates

# ldata = [ctd.ctd(ctd.fname_POEM_ctd_2020),ctd.ctd(ctd.fname_POEM_ctd_2021),ctd.ctd(ctd.fname_SOLA_ctd_2021),ctd.ctd(ctd.fname_SOLA_ctd_2022)]
# data_ctd=pd.concat(ldata)

data_ctd = ctd.ctd(ctd.fname_SOLA_ctd_2021)

#%%
def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    x, y : array-like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    **kwargs
        Forwarded to `~matplotlib.patches.Ellipse`

    Returns
    -------
    matplotlib.patches.Ellipse
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x, y)
    print(x)
    print(y)
    print(cov)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

#%%

data_ctd.dropna(inplace=True)

keys = list(data_ctd.keys())
keys.remove("datetime")
keys.remove("mdates")

dkeys = {}
for i,e in enumerate(keys):
    dkeys[i]=e

nb_param = len(keys)

# {0: 'temperature_degc',
#  1: 'salinity',
#  2: 'fluorescence_rfu',
#  3: 'turbidity_ntu',
#  4: 'oxygen_mgl'}

# coloration en fonction du mois
color = data_ctd.datetime.dt.strftime("%m").astype(int)
labels = """
janvier
février
mars
avril
mai
juin
juillet
aout
septembre
octobre
novembre
decembre
""".split()
ticks = np.arange(1,len(labels)+1)

select = []
for i in range(0,nb_param):
    for j in range(i+1,nb_param):
        select.append([i,j])

select=[[1,3],[0,4]]

print(keys)

for i,j in select:
    fig,ax = plt.subplots()
    cmap = "viridis"
    norm = Normalize(vmin=1,vmax=12)
    im = cm.ScalarMappable(norm=norm, cmap=cmap)
    cbar = fig.colorbar(im, ticks=ticks)
    cbar.ax.set_yticklabels(labels) 
    colx = dkeys[i]
    coly = dkeys[j]
    ax.scatter(data_ctd[colx],data_ctd[coly],marker=".",alpha=0.1,c=color,norm=norm,cmap=cmap)
    ax.set_xlabel(colx)
    ax.set_ylabel(coly)
    #ax.set_aspect("equal")
    #confidence_ellipse(data_ctd[colx],data_ctd[coly],ax,n_std=1,edgecolor='grey',alpha=0.3)
    
