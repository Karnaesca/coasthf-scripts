import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta
import os
import sys
from matplotlib.colors import Normalize
from  matplotlib.cm import ScalarMappable

import load_hydroportail as hydro
import read_chemistry as chem
import read_profil_ctd as pctd
import read_ctd as ctd
import read_met as met
import read_composite as cmp
import file_manager as fm

# This script graph correlation between temperature et oxygen before and after
# relcalibration of oxgen to see if the calibration is successful.

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_raw = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_SOLA_raw = ctd.ctds(ctd.fname_SOLA_ctd_2021,ctd.fname_SOLA_ctd_2022)
data_o2_fixed = cmp.cmp(cmp.fname_POEM_o2_recalibrated)

#%% ===========================================================================
# GRAPH raw POEM
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

cmap = "viridis"
norm = Normalize(vmin=data_POEM_raw.mdates.iloc[0],vmax=data_POEM_raw.mdates.iloc[-1])

ax.scatter(data_POEM_raw.temperature_degc,data_POEM_raw.oxygen_mgl, c=data_POEM_raw.mdates, cmap=cmap, norm=norm,)
ax.set_xlabel("temperature_degc")
ax.set_ylabel("oxygen_mgl")


cax = fig.colorbar(ScalarMappable(norm=norm, cmap=cmap), ax=ax, format=mdates.DateFormatter('%Y-%m'),ticks=mdates.MonthLocator())
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","calibration","temp_vs_o2_POEM_raw")
fig.savefig(fname,bbox_inches='tight')
plt.close(fig)

#%% ===========================================================================
# GRAPH raw SOLA
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

cmap = "viridis"
norm = Normalize(vmin=data_SOLA_raw.mdates.iloc[0],vmax=data_SOLA_raw.mdates.iloc[-1])

ax.scatter(data_SOLA_raw.temperature_degc,data_SOLA_raw.oxygen_mgl, c=data_SOLA_raw.mdates, cmap=cmap, norm=norm,)
ax.set_xlabel("temperature_degc")
ax.set_ylabel("oxygen_mgl")


cax = fig.colorbar(ScalarMappable(norm=norm, cmap=cmap), ax=ax, format=mdates.DateFormatter('%Y-%m'),ticks=mdates.MonthLocator())
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","calibration","temp_vs_o2_SOLA_raw")
fig.savefig(fname,bbox_inches='tight')
plt.close(fig)
      
#%% ===========================================================================
# GRAPH with o2 fixed
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

cmap = "viridis"
norm = Normalize(vmin=data_POEM_raw.mdates.iloc[0],vmax=data_POEM_raw.mdates.iloc[-1])

ax.scatter(data_POEM_raw.temperature_degc,data_o2_fixed.oxygen_mgl, c=data_POEM_raw.mdates, cmap=cmap, norm=norm, alpha=0.5)
ax.set_xlabel("temperature_degc")
ax.set_ylabel("oxygen_mgl fixed")

cax = fig.colorbar(ScalarMappable(norm=norm, cmap=cmap), ax=ax, format=mdates.DateFormatter('%Y-%m'),ticks=mdates.MonthLocator())
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","calibration","temp_vs_o2_POEM_postcalib_o2")
fig.savefig(fname,bbox_inches='tight')
plt.close(fig)

#%% =============================================================================
# Temporal graph with ox2 and temp
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

fig, axs = plt.subplots(2,sharex=True)
fig.set_size_inches(*SIZE)

axs[0].plot(data_POEM_raw.mdates,data_POEM_raw.temperature_degc,label="température $(°C)$")
axs[0].set_ylabel("température $(°C)$")
axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
axs[0].xaxis.set_major_locator(mdates.MonthLocator())
axs[0].legend()

axs[1].plot(data_POEM_raw.mdates,data_POEM_raw.oxygen_mgl,alpha=0.5,label="oxygène raw $(mg/L)$")
axs[1].plot(data_POEM_raw.mdates,data_o2_fixed.oxygen_mgl,label="oxygène recalibrated $(mg/L)$")
axs[1].set_ylabel("oxygène $(mg/l)$")
axs[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
axs[1].xaxis.set_major_locator(mdates.MonthLocator())
axs[1].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","calibration","POEM_temp_o2_simple_graph")
fig.savefig(fname,bbox_inches='tight')
plt.close(fig)