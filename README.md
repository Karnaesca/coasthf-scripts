# What does everything ?

## Load Data

All the scripts that load data are located in `readers`.

+ readers : all the file that are used to read date from files
  + read_ctd : read ctd files
  + read_met : read met files
  + load_hydroportail : load hydroportail data from the web
  + read_chemistry : read chemistry data files
  + read_profil_ctd : read profil ctd data

  + read_debit : not used anymore, remplaced by hydroportail
  + load_coriolis_coiter : not used anymore, use read ctd instead with local files, load coriolis cotier data from the web
  + test_coriolis : not used anymore, graph data from coriolis côtier

## Some tools about files, files names and folder

+ file_manager : give some handy function

## selector

+ selector
  + App : an attempt to select data from matplotlib plot
  + App-v2 :  an second attemp to select data from matplotlib plot

## An attempt to manage logs

+ logs

## Process data

+ fix_o2 : Recalibre les données oxygène
+ 

# readers - scripts to read data
## Add readers libs

### in code
Lines to add at the begining at the code to enable importing lib from those paths

```{python}
import sys
sys.path.append('/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/readers/')
```

### in spyder path

add the path:
```
/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/readers/
```

## Add file_manager libs

### in spyder path

add the path:
```
/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/file_mananger/
```

## Libs and usage

### 

# Tips and Tricks

date range panda
https://www.geeksforgeeks.org/python-pandas-date_range-method/

