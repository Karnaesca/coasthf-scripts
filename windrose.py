import read_met as met
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from size import cm
import file_manager as fm

# https://gist.github.com/phobson/41b41bdd157a2bcf6e14

# =============================================================================
# La direction du vent c'est la direction de provenance du vent, pour les
# courants marin c'est le contraire. Bisou.
# =============================================================================

def windrose(tdir_deg,tspeed_kn,fname=None,nb_branch=12):
    """
    Produce a windrose from windspeed and wind direction.

    Parameters
    ----------
    tdir_deg : numpy.array
        array of wind direction in degrees between 0 and 360 degrees
        
    tspeed_kn : numpy.array
        array of wind speed in knots, must be the same size as tdir_deg and
        must have the same index
        
    fname : str
        file_name where where should store the figure, if none, not saved.
        The default is None.
        
    nb_branch : int, optional
        The number of branch of the windrose. The default is 12.

    Returns
    -------
    None.

    """
    categories = pd.DataFrame()
    
    bindir = np.linspace(0,360,nb_branch+1)
    rbindir = bindir[:-1]/360*2*np.pi
    rbarw = bindir[1]/360*2*np.pi
    keydir = [f"{int(bindir[i])}-{int(bindir[i+1])}" for i in range(len(bindir)-1)]
    grpdir = pd.cut(tdir_deg,bindir,labels=keydir)
    categories["grpdir"] = grpdir
    
    binspe = np.append(np.linspace(0,30,7),np.nanmax(tspeed_kn))
    keyspe = [f"{int(binspe[i])}-{int(binspe[i+1])}" for i in range(len(binspe)-1)]
    grpspe = pd.cut(tspeed_kn,binspe,labels=keyspe,right=True)
    lblspe = [f"{int(binspe[i])} - {int(binspe[i+1])} noeuds" for i in range(len(binspe)-2)]
    lblspe.append(f"> {int(binspe[-2])} noeuds")
    categories["grpspe"] = grpspe
    
    sumcat = pd.DataFrame(0,index = keyspe, columns = keydir)
    
    for s in keyspe:
        for d in keydir:
            cnt = categories[(categories.grpdir==d) & (categories.grpspe==s)].count()[0]
            sumcat.loc[s,d] = cnt
    
    cmap = matplotlib.cm.plasma
    
    fig, ax = plt.subplots(figsize=(15*cm,15*cm), subplot_kw=dict(polar=True))
    ax.set_theta_direction('clockwise')
    ax.set_theta_zero_location('N')
    ax.grid(True)
    
    for i,s in enumerate(keyspe):
        ax.bar(rbindir,
               sumcat.loc[s],
               width=rbarw,
               color=cmap(i/(len(keyspe)-1)),
               bottom=sumcat.cumsum().loc[s]-sumcat.loc[s],
               label=lblspe[i],
               linewidth=0,
               zorder=10
               )
        
    ax.set_xticklabels(['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'])
    ax.legend()
    fig.tight_layout(pad=0)
    if fname != None:
        fig.savefig(fname,bbox_inches='tight')
        
if __name__ == '__main__':
    # windspeed_kn
    # windirection_deg
    
    poem = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021)
    sola = met.mets(met.fname_SOLA_met_2021,met.fname_SOLA_met_2022)
    
    poem.dropna(how="all",inplace=True)
    sola.dropna(how="all",inplace=True)
    
    fname = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/mesurho/mooring-buoys-time-series-6100284.csv"
    mesurho = pd.read_csv(fname)
    #['WDIR LEVEL0 (degree)', 'WSPD LEVEL0 (meter/second)']
    mesurho["wdir"] = mesurho["WDIR LEVEL0 (degree)"]
    mesurho["wspe"] = mesurho["WSPD LEVEL0 (meter/second)"] * 3.6 / 1.852
    
    fname = fm.build_path(fm.root,"_build","windrose","windrose_poem_2020_2021")
    windrose(poem.windirection_deg, poem.windspeed_kn, fname=fname, nb_branch=45)
    
    fname = fm.build_path(fm.root,"_build","windrose","windrose_sola_2021_2022")
    windrose(sola.windirection_deg, sola.windspeed_kn, fname=fname, nb_branch=45)
    
    fname = fm.build_path(fm.root,"_build","windrose","windrose_rhone_2018_2021")
    windrose(mesurho_wdir, mesurho_wspe, fname=fname, nb_branch=45)
    
    for i in range(4):
        sample = poem[(poem.datetime.dt.month>=1+3*i) & (poem.datetime.dt.month<=3+3*i)]
        windrose(sample.windirection_deg,sample.windspeed_kn)
        
    for i in [0,2,3]:
        sample = sola[(sola.datetime.dt.month>=1+3*i) & (sola.datetime.dt.month<=3+3*i)]
        windrose(sample.windirection_deg,sample.windspeed_kn)
    


