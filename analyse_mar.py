import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import read_mar as mar
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait

# =============================================================================
# LOAD DATA
# =============================================================================

data_mar_PN = cmp.cmp(cmp.fname_data_tide_PN_3h_med)
data_mar_PV = cmp.cmp(cmp.fname_data_tide_PV_3h_med)

start = "2020-01-01"
end = "2021-12-31"

data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

#%% ===========================================================================
# GRAPH TIME MET
# =============================================================================

datetime_col_name="datetime"
keys = ["h_mer_m"]

y = data_mar_PV[keys]
y1 = y.rolling(10).mean()
    
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

axes = [axes]

for i,key in enumerate(keys):
    # HF data
    #axes[i].plot(data_mar_PN.mdates,data_mar_PN[key],label="PN",c=tl.color(0))
    axes[i].plot(data_mar_PV.mdates,data_mar_PV[key],label="PV",c=tl.color(0),alpha=1)
    axes[i].plot(data_mar_PV.mdates,y1,label="PV",c=tl.color(1),alpha=1)
    # debit Têt
    ax2 = axes[i].twinx()
    ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="Têt $(L/s)$",c=tl.color(2),alpha=0.5)
    
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())
    axes[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)