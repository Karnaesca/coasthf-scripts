import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.colors import Normalize
from matplotlib.widgets import RectangleSelector
import matplotlib
import matplotlib.dates as mdates
import xarray as xr
from pydap.client import open_url
from pydap.cas.get_cookies import setup_session
import os
import tools as tl
import file_manager as fm
from size import A4_paysage, A4_portait
import load_copernicus as coper

# cap bear : 42.516103 , 3.136532

def onselect(*args):
    x1, y1, x2, y2 = args[0].xdata, args[0].ydata, args[1].xdata, args[1].ydata
    
    loc = [43.671118, 2.923778]
    lat = loc[0]
    lon = loc[1]
    
    x1 = x1 + satds.lon2i(lon)
    x2 = x2 + satds.lon2i(lon)
    
    y1 = y1 + satds.lat2i(lat)
    y2 = y2 + satds.lat2i(lat)
    
    x1 = satds.i2lon(int(x1))
    x2 = satds.i2lon(int(x2))
    y1 = satds.i2lat(int(y1))
    y2 = satds.i2lat(int(y2))
    
    res = np.abs(np.array([y1,x1,y2,x2]))
    res = np.around(res,decimals=3)
    print(f'[{",".join([str(e) for e in res])}]')

satds = coper.SatDataSet()

zone = [43.671118 ,5.163132, 42.463854, 2.923778]
date = pd.to_datetime("2021-07-05 00:00:00")

data = satds.getchunk("TUR", *zone, date)

norm = Normalize(np.nanmin(np.log10(data)),np.nanpercentile(np.log10(data),99))

fig, ax = plt.subplots()
ax.matshow(np.log10(data),norm=norm)
fig.tight_layout(pad=0)
fig.subplots_adjust(wspace=0, hspace=0)



rs = RectangleSelector(ax, onselect)