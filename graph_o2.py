import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl

# =============================================================================
# LOAD DATA
# =============================================================================
data_POEM_ctd = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_POEM_o2_recal = 

# =============================================================================
#  GRAPH
# =============================================================================

fig, axs = plt.subplots()
fig.set_size_inches(*SIZE)

cmap = "viridis"
norm = Normalize(vmin=data_POEM_BF.mdates.iloc[0],vmax=data_POEM_BF.mdates.iloc[-1])

index = 0
for x in range(ncols):
    for y in range(nrows):
        if index < nselect:
            ax = axs[x][y]
            col = dkeys[select[index][0]]
            row = dkeys[select[index][1]]
            ax.scatter(data_POEM_BF[col],data_POEM_BF[row],marker=".",c=data_POEM_BF.mdates,alpha=0.5)
            ax.set_xlabel(col)
            ax.set_ylabel(row)
        index += 1
        
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","raw","correlation_POEM_BF_raw")
fig.savefig(fname,bbox_inches='tight')