import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait

#%% ===========================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

start = data_POEM_HF.datetime.iloc[0].strftime("%Y-%m-%d")
end = data_POEM_HF.datetime.iloc[-1].strftime("%Y-%m-%d")

data_SOLA_HF = ctd.ctds(ctd.fname_SOLA_ctd_2021)

data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

#data_POEM_met_HF = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021)

data_chem = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)
data_chem = tl.timecut(data_chem, start, end)

data_pctd = pctd.manual_ctd_2m(pctd.fname_CHIFRE_POEM_ctd_2017_2021)
data_pctd = tl.timecut(data_pctd, start, end)

POEM_o2_recalibrated = cmp.cmp(cmp.fname_POEM_o2_recalibrated)
POEM_o2_recalibrated = tl.addmdates(POEM_o2_recalibrated)

#%% =============================================================================
# CONVERT
# =============================================================================

dO2 = 1.42763 #kg·m-3, mg/mL
data_chem["oxygen_mgl"] = data_chem["o2_ml_L"]*dO2


#%% ===========================================================================
# GRAPH CTD
# =============================================================================

datetime_col_name="datetime"
keys = [key for key in list(data_POEM_HF.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
    
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    # HF data
    if key == "oxygen_mgl":
        axes[i].plot(POEM_o2_recalibrated.mdates,POEM_o2_recalibrated[key],label="POEM HF recalibrated",c=tl.color(0),zorder=1)
    else:
        axes[i].plot(data_POEM_HF.mdates,data_POEM_HF[key],label="POEM HF",c=tl.color(0),zorder=1)
    
    axes[i].plot(data_SOLA_HF.mdates,data_SOLA_HF[key],label="SOLA HF",c=tl.color(1),alpha=0.5,zorder=1)
    # BF data
    # chem
    if key in list(data_chem.keys()):
        axes[i].scatter(data_chem.mdates,data_chem[key],label="chem BF",c="lime",marker="x",zorder=2)
    if key in list(data_pctd.keys()):
        axes[i].scatter(data_pctd.mdates,data_pctd[key],label="pctd BF",c="fuchsia",marker="x",zorder=2)
    
    ax2 = axes[i].twinx()
    ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="débit Têt",c=tl.color(4),alpha=0.5,zorder=1)
    
    axes[i].set_ylabel(tl.col2name(key))
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())
    #axes[i].xaxis.set_minor_locator(mdates.DayLocator())
    axes[i].legend(loc=4)
    ax2.legend(loc=1)
    ax2.set_ylabel("Têt $L/s$")

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(hspace=0)

fname = fm.build_path(os.getcwd(),"_build","raw","full_graph_POEM_RAW_more_data",ext=".svg")
fig.savefig(fname,bbox_inches='tight')