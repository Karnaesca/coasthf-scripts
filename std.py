import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import read_ctd

data_ctd = read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
datetime_name = "datetime"
data_ctd["mdates"] = [mdates.date2num(e) for e in data_ctd.datetime]

keys = list(data_ctd.keys())
keys.remove(datetime_name)
keys.remove("mdates")

t_std = data_ctd[keys].rolling(1000).std()
slope = (data_ctd[keys]-data_ctd[keys].shift()).abs()
slope = -slope

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],data_ctd[key],label="RAW",alpha=0.2)
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    axes2 = axes[i].twinx()
    axes2.plot(data_ctd["mdates"],t_std[key],label="std",c="tab:red",alpha=0.5)
    axes2.plot(data_ctd["mdates"],slope[key],label="slope",c="tab:green",alpha=0.5)
    axes2.legend()
    
plt.gcf().autofmt_xdate()
