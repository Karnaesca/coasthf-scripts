import read_ctd as ctd
import read_met as met
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
import matplotlib.dates as mdates
from datetime import timedelta
import matplotlib as mpl
import file_manager as fm

s_trad="""
oxygen_mgl:Oxygène $(mg/L)$
atmosphericpressure_mbar:Pression Atmo. $(mbar)$
airtemperature_degc:Température Air $(°C)$
windspeed_kn:Vitesse Vent $(kn)$
windirection_deg:Direction Vent $(°)$
temperature_degc:Température Air $(°C)$
salinity:Salinité $(ratio)$
fluorescence_rfu:Fluoréscence $(RFU)$
turbidity_ntu:Turbidité $(NTU)$
"""
t_trad = np.array([e.split(":") for e in s_trad.strip().split("\n")])
d_trad = {k:v for k,v in t_trad}

def trad_all(val):
    b = val.split("_")[-1]
    a = "".join([*val[:-2]])
    if b == "y":
        buoy = "POEM"
    else :
        buoy = "SOLA"
    sres = f"{buoy} - {d_trad[a]}"
        
    return sres

def trad_POEM(val):
    buoy = "POEM"
    sres = f"{buoy} - {d_trad[val]}"
        
    return sres

def trad_SOLA(val):
    buoy = "SOLA"
    sres = f"{buoy} - {d_trad[val]}"
        
    return sres

data_ctd0 = ctd.ctd(ctd.fname_POEM_ctd_2020)
data_ctd1 = ctd.ctd(ctd.fname_POEM_ctd_2021)
data_ctd_POEM = pd.concat((data_ctd0,data_ctd1))
data_met0 = met.met(met.fname_POEM_met_2020)
data_met1 = met.met(met.fname_POEM_met_2021)
data_met_POEM = pd.concat((data_met0,data_met1))

data_ctd0 = ctd.ctd(ctd.fname_SOLA_ctd_2021)
data_ctd1 = ctd.ctd(ctd.fname_SOLA_ctd_2022)
data_ctd_SOLA = pd.concat((data_ctd0,data_ctd1))
data_met0 = met.met(met.fname_SOLA_met_2021)
data_met1 = met.met(met.fname_SOLA_met_2022)
data_met_SOLA = pd.concat((data_met0,data_met1))

data_POEM = pd.merge(data_met_POEM,data_ctd_POEM,how="outer",on="datetime")
data_SOLA = pd.merge(data_met_SOLA,data_ctd_SOLA,how="outer",on="datetime")

data_ALL = pd.merge(data_POEM,data_SOLA,how="outer",on="datetime")

#%%

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

def graph(data,trad_col_func,fname):
    keys = list(data.keys())
    keys.remove("datetime")
    keys = [key for key in keys if "mdates" not in key]
    
    names = []
    for key in keys:
        names.append(trad_col_func(key))
    
    # define colormap
    colors = ["r","gold","g"]
    cmap_custom = ListedColormap(colors)
    
    df = pd.DataFrame()
    df["group_unique"] = data.datetime.dt.strftime("%Y-%m-%d")
    for key in keys:
        df[key] = ~data[key].isnull().values
    
    #data = df.groupby("group_unique").sum().values.T
    build = df.groupby("group_unique").sum()
    build.where(~((build>0)&(build<288)),1,inplace=True)
    build.where(~(build==288),2,inplace=True)
    build = build.values.T
    nrow,ncol=build.shape
    
    x_lim = [data.datetime.iloc[0],data.datetime.iloc[-1]]
    x_lim = [mdates.date2num(e) for e in x_lim]
    y_lim = np.array([-0.5,nrow-0.5])
    
    extent = [x_lim[0],x_lim[1],y_lim[0],y_lim[1]]  
    
    fig, ax = plt.subplots()
    fig.set_size_inches(*SIZE)
    
    im = ax.imshow(build,interpolation="none",aspect=15,cmap=cmap_custom,extent=extent)
    
    ax.set_yticks(np.arange(nrow))
    ax.set_yticklabels(names)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    ax.xaxis.set_major_locator(mdates.MonthLocator())
    
    # graph horizontale grid
    for i in range(nrow):
        y = [i+0.5,i+0.5]
        x = x_lim
        plt.plot(x,y,color="white",lw=0.5)
    
    # graph vertical grid
    grid_dates = pd.date_range(str(data.datetime.iloc[0].year),str(data.datetime.iloc[-1].year), freq="1Y")
    
    print(grid_dates)
    
    grid_mdates = [mdates.date2num(e) for e in grid_dates]
    for grid_date in grid_dates:
        x = [grid_date,grid_date]
        y = y_lim
        plt.plot(x,y,color="blue",lw=0.5,ls="--")
    
    cbar = plt.colorbar(im,fraction=0.016, pad=0.01)
    cbar.set_ticks([0,1,2])
    cbar.set_ticklabels(["no data","at least one value missing","complete data"])
    
    fig.tight_layout(pad=0)
    fig.autofmt_xdate()
    title = ""
    fig.suptitle(title)
    fig.savefig(fname,bbox_inches='tight')
    
def stat(data,trad_col_func,fname):
    keys = list(data.keys())
    keys.remove("datetime")
    keys = [key for key in keys if "mdates" not in key]
    
    names = []
    for key in keys:
        names.append(trad_col_func(key))
        
    data = data[keys]
    
    rows,cols = data.shape
    
    stats = pd.DataFrame()
    stats["available_data"] = (~data.isnull()).sum()
    stats["total_data"] = rows
    stats["percent_available_data"] = stats["available_data"]/rows*100
    
    stats.to_markdown(buf=fname)
    
    
    
#%%
if __name__ == '__main__':
    print(__file__)
    #%%graph ALL
    # fname = fm.build_path("_build","raw","availability_ALL",ext=".png")
    # print(fname)
    # graph(data_ALL,trad_all,fname)
    
    #%% graph POEM
    fname = fm.build_path("_build","raw","availability_POEM",ext=".png")
    print(fname)
    graph(data_POEM,trad_POEM,fname)
    
    # #%% graph POEM
    # fname = fm.build_path("_build","raw","availability_SOLA",ext=".png")
    # print(fname)
    # graph(data_SOLA,trad_SOLA,fname)
   
    #%% stat POEM
    fname = fm.build_path("_build","stat","availability_POEM_2020_2021",ext=".md")
    print(fname)
    stat(data_POEM,trad_POEM,fname)