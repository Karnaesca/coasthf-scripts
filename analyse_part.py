import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# %%=============================================================================
# LOAD DATA
# =============================================================================
data_POEM_HF = cmp.cmp(cmp.fname_POEM_HF_clean_part)
data_SOLA_HF = cmp.cmp(cmp.fname_SOLA_HF_clean_part)

keys = list(data_POEM_HF.keys()) 
keys = [key for key in keys if ("datetime" not in key) and ("mdates" not in key)]

# =============================================================================
# CORR
# =============================================================================


# %%=============================================================================
# GRAPH
# =============================================================================

fig, ax = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    
    ax[i].plot(data_POEM_HF.mdates,data_POEM_HF[key],label="POEM HF",c=tl.color(0))
    ax[i].plot(data_SOLA_HF.mdates,data_SOLA_HF[key],label="SOLA HF",c=tl.color(1))
    
    ax[i].set_ylabel(tl.col2name(key))
    ax[i].grid(True)
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax[i].xaxis.set_major_locator(mdates.MonthLocator())
    ax[i].xaxis.set_minor_locator(mdates.DayLocator())
    ax[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)