import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.colors import Normalize
import matplotlib.dates as mdates
import os

import tools as tl
import file_manager as fm
from size import A4_paysage, A4_portait
import load_hydroportail as hydro
import read_composite as cmp

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = cmp.cmp(cmp.fname_POEM_HF_clean_part)

timeframe = pd.to_datetime(["2021-05-01","2021-12-31"])
stations = list(hydro.d_stations.values())
statusdatas = ["most_valid", "validated", "pre_validated_and_validated", "raw"]

for statusdata in statusdatas:
    tmp = hydro.HydroSeries(station=stations[0],start=timeframe[0],end=timeframe[1],statusdata=statusdata).data[["datetime","volume_l_s"]]
    if not tmp.empty:
        break

data_hydro = tmp
data_hydro.rename(columns={"volume_l_s" : stations[0]},inplace=True)

for station in stations[1:]:
    for statusdata in statusdatas:
        tmp = hydro.HydroSeries(station=station,start=timeframe[0],end=timeframe[1],statusdata=statusdata).data[["datetime","volume_l_s"]]
        if not tmp.empty:
            break
    tmp.rename(columns={"volume_l_s" : station},inplace=True)
    
    data_hydro = data_hydro.merge(tmp,how="outer",on="datetime")
    data_hydro.reset_index()
    
data_hydro = tl.addmdates(data_hydro)
data_hydro.sort_values(by="datetime",inplace=True)

keys = [station for station in stations if 'V' not in station]

data_hydro["sum"] = data_hydro[keys].sum(axis=1)

    
# %%=============================================================================
# GRAPH
# =============================================================================
l = len(stations)+1
fig, ax = plt.subplots(l,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,station in enumerate(stations) :
    ax[i].plot(data_hydro.mdates,data_hydro[station],label=hydro.code2name(station))
    ax[i].grid(True)
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax[i].xaxis.set_major_locator(mdates.MonthLocator())
    ax[i].xaxis.set_minor_locator(mdates.DayLocator())
    ax[i].legend()
    ax[i].set_ylabel("débit L/s")
    
ax[l-1].plot(data_POEM_HF.mdates,data_POEM_HF.salinity,label="POEM salinity")
ax[l-1].grid(True)
ax[l-1].legend()
ax[l-1].set_ylabel("salinity (ratio)")
ax[l-1].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax[l-1].xaxis.set_major_locator(mdates.MonthLocator())
ax[l-1].xaxis.set_minor_locator(mdates.DayLocator())

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(wspace=0, hspace=0)
    
# %%=============================================================================
# GRAPH
# =============================================================================
fig, ax = plt.subplots(2,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,station in enumerate(stations) :
    ax[0].plot(data_hydro.mdates,data_hydro[station],label=hydro.code2name(station))

ax[0].plot(data_hydro.mdates,data_hydro["sum"],label="sum")

ax[0].grid(True)
ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax[0].xaxis.set_major_locator(mdates.MonthLocator())
ax[0].xaxis.set_minor_locator(mdates.DayLocator())
ax[0].semilogy()
ax[0].set_ylabel("débit (l/s)")
ax[0].legend()
    
ax[1].plot(data_POEM_HF.mdates,data_POEM_HF.salinity,label="POEM salinity")
ax[1].grid(True)
ax[1].legend()
ax[1].set_ylabel("salinity (ratio)")

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(wspace=0, hspace=0)
     