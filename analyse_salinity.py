import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

from meteostat import Stations
from meteostat import Hourly

# =============================================================================
# LOAD DATA
# =============================================================================

cut = ["2020-01-01","2021-12-31"]

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_SOLA_HF = ctd.ctds(ctd.fname_SOLA_ctd_2021)

data_debit = hydro.HydroSeries(station=hydro.station_Tet_Perpignan,start=cut[0],end=cut[1]).data

#data_chem = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)
#data_chem = tl.timecut(data_chem, *cut)

data_pctd = pctd.manual_ctd_2m(pctd.fname_CHIFRE_POEM_ctd_2017_2021)
data_pctd = tl.timecut(data_pctd, *cut)

data_TUR = coper.get_SPM_CHL_TUR()

coord_poem = {"lat" : 42.704167, "lon" : 3.066667}
timeframe = pd.to_datetime(["2020","2022"])

stations = Stations()
stations = stations.nearby(*list(coord_poem.values()))
stations = stations.fetch(3)

df_prcp = pd.DataFrame()
df_prcp[stations.index[0]] = Hourly(stations.index[0],*timeframe).fetch().prcp
df_prcp["datetime"] = pd.to_datetime(df_prcp.index)
for station in stations.index[1:] :
    print(station)
    df_tmp = pd.DataFrame()
    df_tmp[station] = Hourly(station,*timeframe).fetch().prcp
    df_tmp["datetime"] = pd.to_datetime(df_tmp.index)
    df_prcp = df_prcp.merge(df_tmp,how="outer",on="datetime")
    
df_prcp.index = df_prcp.datetime
df_prcp.drop(columns="datetime",inplace=True)
tl.warn()
# %%=============================================================================
# GRAPH
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(data_POEM_HF.mdates,data_POEM_HF.salinity,label="POEM HF Salinity")
ax.plot(data_SOLA_HF.mdates,data_SOLA_HF.salinity,label="SOLA HF Salinity")
ax.plot(data_pctd.mdates,data_pctd.salinity,label="POEM BF Salinity",marker="x")
ax.set_ylabel("Salinity")
ax.legend(loc=0)

ax2 = ax.twinx()
ax2.plot(data_debit.mdates,data_debit.volume_l_s,c=tl.color(3),label="Debit Têt")
ax2.set_ylabel("Debit L/s")
ax2.legend(loc=1)

ax2 = ax.twinx()
ax2.plot(data_TUR.mdates,data_TUR.tur,c=tl.color(4),label="TUR sat")
ax2.set_ylabel("TUR sat")
ax2.legend(loc=3)

ax2 = ax.twinx()
ax2.plot(df_prcp,c=tl.color(5),label="Precipitation Perpi")
ax2.set_ylabel("Precipitation perpi")
ax2.legend(loc=4)

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
# ax.xaxis.set_major_locator(mdates.MonthLocator())

fig.autofmt_xdate()
fig.tight_layout(pad=0)

#%% =============================================================================
# GRAPH PLUIE
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(df_prcp,c=tl.color(5),label="Precipitation Perpi")
ax.set_ylabel("Precipitation perpi")
ax.legend(loc=4)

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
# ax.xaxis.set_major_locator(mdates.MonthLocator())

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# =============================================================================
# GRAPH STD
# =============================================================================

t = data_POEM_HF.datetime
salinity = data_POEM_HF.salinity
mean = salinity.rolling(288,center=True).mean()
std = salinity.rolling(288,center=True).std()

over = mean+std
under = mean-std

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(t,salinity)
ax.plot(t,mean)
ax.plot(t,over)
ax.plot(t,under)
ax.set_ylabel("")
ax.legend()

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()
fig.tight_layout(pad=0)
