import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

from meteostat import Stations
from meteostat import Hourly

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

#%% =============================================================================
# COMPUTE STD
# =============================================================================
t = data_POEM_HF.datetime
salinity = data_POEM_HF.salinity

l = len(salinity)
window_mean = 288
window_std = int(window_mean*2)
f = 5


mean = salinity.rolling(window_mean,center=True,min_periods=1).mean()
std = salinity.rolling(window_std,center=True,min_periods=1).std()

threshold = 2
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (salinity <= over) & (salinity >= under)

nb_rejected = (~select).sum() - salinity.isnull().sum()

print(f"Nb rejected : {nb_rejected}")


if __name__ == '__main__' :
# =============================================================================
# GRAPH STD
# =============================================================================

    fig, ax = plt.subplots()
    fig.set_size_inches(21*cm,7*cm)
    
    ax.plot(t,salinity)
    ax.plot(t,mean)
    ax.plot(t,over)
    ax.plot(t,under)
    ax.scatter(t,salinity.where(~select),marker="x",c="y")
    ax.set_ylabel("")
    ax.legend()
    
    ax.grid(True)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    
    # =============================================================================
    # GRAPH CLEAN
    # =============================================================================
    
    fig, ax = plt.subplots()
    fig.set_size_inches(21*cm,7*cm)
    
    ax.plot(t,salinity.where(select))
    ax.plot(t,salinity,alpha=0.3,zorder=-10)
    ax.set_ylabel("")
    ax.legend()
    
    ax.grid(True)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    
# =============================================================================
#     Test
# %%=============================================================================
    
    wm = 12*24
    ws = 12*24
    
    isalinity = salinity.interpolate()
    
    mean = isalinity.rolling(wm,center=True,min_periods=1).mean()
    
    salhf = isalinity - mean
    
    std = salhf.rolling(ws,center=True,min_periods=1).std() * 3
    
    fil = salhf.abs() < std
    
    good = fil.sum()
    bad = (~fil).sum()
    l = len(t)
    pct = (good/l)*100
    
    print(f"good : {good}")
    
    print(f"bad : {bad}")
    
    print(f"ratio : {pct}")
    
    fig, ax = plt.subplots()
    ax.hist(salhf,bins=100)
    ax.semilogy()
    
    fig, ax = plt.subplots()
    ax.hist(salinity,bins=100)
    ax.semilogy()
    
    fig, ax = plt.subplots()
    ax.plot(t,salhf+mean+std)
    ax.plot(t,salhf+mean-std)
    ax.plot(t,salinity)
    
    fig, ax = plt.subplots()
    ax.plot(t,np.zeros(len(t))+std)
    ax.plot(t,np.zeros(len(t))-std)
    ax.plot(t,salhf)
    
    fig, ax = plt.subplots()
    ax.plot(t,salinity)
    ax.scatter(t,salinity.where(~fil),marker="x",c="r")
    
    fig, ax = plt.subplots()
    ax.plot(t,salinity.where(fil))
    ax.scatter(t,salinity.where(~fil),marker="x",c="r")
    
    fig, ax = plt.subplots()
    ax.hist(salinity.where(fil),bins=100)
    ax.semilogy()
    
    fig, ax = plt.subplots()
    ax.hist(salhf.where(fil),bins=100)
    ax.semilogy()
    