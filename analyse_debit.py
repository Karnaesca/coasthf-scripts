import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import read_debit
import read_ctd

#load data 2020
year = "all"

if year == "2020":
    data_ctd = read_ctd.ctd(read_ctd.fname_POEM_ctd_2020)
    data_debit = read_debit.debit(read_debit.fname_debit_2020_Tet)
if year == "2021":
    data_ctd = read_ctd.ctd(read_ctd.fname_POEM_ctd_2021)
    data_debit = read_debit.debit(read_debit.fname_debit_2021_Tet)
if year == "all":
    data_ctd0 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2020)
    data_debit0 = read_debit.debit(read_debit.fname_debit_2020_Tet)
    data_ctd1 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2021)
    data_debit1 = read_debit.debit(read_debit.fname_debit_2021_Tet)
    data_ctd = pd.concat([data_ctd0,data_ctd1],sort=False)
    data_debit = pd.concat([data_debit0,data_debit1],sort=False)

datetime_name = "datetime"
keys = list(data_ctd.keys())
keys.remove(datetime_name)
keys.remove("mdates")

# =============================================================================
#%% graph
# =============================================================================

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(29.7*(25/64),21*(25/64))


for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd.mdates,data_ctd[key],label="HF",alpha=1,zorder=-10)
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    #axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
    #axes[i].set_xticks(data_manual_ctd.mdates)
    axes[i].legend()
    ax2 = axes[i].twinx()
    ax2.plot(data_debit.mdates,data_debit.debit_m3_s,label="Debit",c="tab:orange")
    ax2.set_ylabel("debit_m3_s")
    ax2.legend()

fig.set_dpi(150.0)
fig.autofmt_xdate()
title = f"Paramètres de la bouée POEM en 2020 et 2021 comparés au débit de la Têt"
fig.suptitle(title)
fig.savefig("fig/"+fm.to_img_fname(title))