import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

fname = "/home/karnaphorion/Downloads/query.csv"

data = pd.read_csv(fname,sep=";")

data["Time"] = pd.to_datetime(data["Time"])

data = data[["Time","mag"]]
 
#cuts = [0,2.5,5,7.5]

cuts = list(np.arange(0,10,1))

groups = data.groupby(pd.cut(data.mag,cuts))#.count()

for group in groups:
    print(group)


df = pd.DataFrame()

cnt = groups.head().resample("MS",on="Time")["mag"].count()

df.index = cnt.index
#df.reset_index(inplace=True)

for i,group in enumerate(groups):
    cnt = group[1].resample("MS",on="Time")["mag"].count()
    tmp = pd.DataFrame()
    tmp[i+1] = cnt
    if not tmp.empty:
        df = df.merge(tmp,how="outer",on="Time")
        #df.reset_index(inplace=True)
    
df = df.fillna(0)

keys = list(df.keys())

#plt.bar(df.index,df[keys])

width = 30

ngrp = list(groups.groups.keys())

fig, ax = plt.subplots()

offset = np.zeros(len(df))

for key in keys:
    ax.bar(df.index,df[key],width=width,label=ngrp[key-1],bottom=offset)
    offset += df[key].values
    
ax.legend()

plt.show()
