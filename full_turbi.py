import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
import load_copernicus as coper
from size import A4_paysage, A4_portait

#%% ===========================================================================
# LOAD DATA
# =============================================================================
cut = ['2020-09-06T21:24', '2020-12-07T10:57']

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_POEM_HF = tl.timecut(data_POEM_HF, *cut)

data_SOLA_HF = ctd.ctds(ctd.fname_SOLA_ctd_2021)
data_SOLA_HF = tl.timecut(data_SOLA_HF, *cut)

data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=cut[0],end=cut[1]).data

data_chem = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)
data_chem = tl.timecut(data_chem, *cut)

data_pctd = pctd.manual_ctd_2m(pctd.fname_CHIFRE_POEM_ctd_2017_2021)
data_pctd = tl.timecut(data_pctd,*cut)

data_SPM_TUR_CHL = coper.get_SPM_CHL_TUR()
data_SPM_TUR_CHL = tl.timecut(data_SPM_TUR_CHL, *cut,col="time")

tl.warn()

# %% =============================================================================
# GRAPH
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(*A4_paysage)

ax.plot(data_POEM_HF.mdates,data_POEM_HF.turbidity_ntu,label="POEM HF",c=tl.color(0))
ax.plot(data_SOLA_HF.mdates,data_SOLA_HF.turbidity_ntu,label="SOLA HF",c=tl.color(1),alpha=0.5)

ax.plot(data_chem.mdates,data_chem.MES_mg_L,label="chem BF",c=tl.color(2),marker="x",zorder=10)
ax.plot(data_pctd.mdates,data_pctd.turbidity_ntu,label="pctd BF",c=tl.color(3),marker="x",zorder=11)

ax.legend(loc=1)
ax.set_ylim(-2,10)

ax2 = ax.twinx()
ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="Têt $(L/s)$",c=tl.color(4),alpha=0.5)
ax2.legend(loc=2)

ax2 = ax.twinx()
ax2.plot(data_SPM_TUR_CHL.time,data_SPM_TUR_CHL.tur,label="TUR sat",c=tl.color(5),alpha=0.5,marker="x")
ax2.legend(loc=4)

ax.set_ylabel(tl.col2name("turbidity_ntu"))
ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
#ax.xaxis.set_major_locator(mdates.MonthLocator())

fig.autofmt_xdate()
fig.tight_layout(pad=0)