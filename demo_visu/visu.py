import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import datetime
import tools as tl
import file_manager as fm
import os

# 0    No quality control (QC) was performed.
# 1    QC was performed: good data
# 2    QC was performed: probably good data
# 3    QC was performed: probably bad data
# 4    QC was performed: bad data
# 5    The value was changed as a result of QC
# 7    Nominal value
# 8    Interpolated value
# 9   The value is missing

# =============================================================================
# SOME USEFUL THINGS
# =============================================================================
cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm

def qc2c(qc):
    d_qc2c = {
        9 : "tab:grey",
        4 : "tab:red",
        3 : "tab:orange",
        1 : "tab:green"
        }
    return d_qc2c[qc]

# =============================================================================
# LOAD DATA
# =============================================================================
fname = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/validator/data_qc.csv"
data = pd.read_csv(fname)
data.utc_datetime = pd.to_datetime(data.utc_datetime)

keys = [e for e in list(data.keys()) if (not "datetime" in e) and (not "qc" in e)]

data.loc[:,("mdates")] = [mdates.date2num(e) for e in data.utc_datetime]


start = datetime.datetime.now()
# =============================================================================
# GRAPH method 1
# =============================================================================

fig, ax = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    col_qc = f"qc_{key}"
    qcs = list(data[col_qc].unique())
    for qc in qcs:
        ax[i].plot(data.mdates,data[key].where(data[col_qc]==qc),c=qc2c(qc),label=f"qc = {qc}")
    
    ax[i].legend()
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax[i].set_ylabel(f"{tl.col2shortname(key)}")
    ax[i].grid(True)
    ax[i].xaxis.set_major_locator(mdates.MonthLocator())
    ax[i].legend(loc=1)
        
fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(hspace=0)

fname = fm.build_path(os.getcwd(),"qc_POEM",ext=".svg")
fig.savefig(fname,bbox_inches='tight')

end = datetime.datetime.now()
print(end-start)


# start = datetime.datetime.now()
# =============================================================================
# GRAPH method 2
# =============================================================================
    
# fig, ax = plt.subplots(len(keys),1,sharex=True)
# fig.set_size_inches(*A4_paysage)

# for i,key in enumerate(keys):
#     col_qc = f"qc_{key}"
#     qcs = list(data[col_qc].unique())
#     for qc in qcs:
#         #tmp = data[["utc_datetime",key]]
#         tmp = data[data[col_qc]==qc]
#         ax[i].plot(tmp.utc_datetime,tmp[key],c=qc2c(qc))
        

# fig.autofmt_xdate()
# fig.tight_layout(pad=0)

# end = datetime.datetime.now()
# print(end-start)


# tl.warn()

#
# %%%
#

l = data[qc_keys].count()/100

df = pd.DataFrame()
df["donnée bonne"] = (data[qc_keys] == 1).sum()/l
df["probablement donnée mauvaise"] = (data[qc_keys] == 3).sum()/l
df["donnée mauvaise"] = (data[qc_keys] == 4).sum()/l
df["donnée manquante"] = (data[qc_keys] == 9).sum()/l

df.index = [tl.col2name(e.split("_",1)[1]) for e in df.index.values]
df = df.round(2)
print(df.to_markdown())