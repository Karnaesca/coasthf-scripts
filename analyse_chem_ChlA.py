import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import read_chemistry
import read_profil_ctd
import read_ctd
import read_debit
import file_manager as fm
from datetime import timedelta

# function
def med_1h(data,param,datetime):
    dt_min = datetime-timedelta(minutes=30)
    dt_max = datetime+timedelta(minutes=30)
    res = data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)].median()
    tmp =  data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)]
    print(tmp)
    return res

# o2_ml_L ChlA_ug_L MES_mg_L
data_chem = read_chemistry.chem_3m(read_chemistry.fname_CHIFRE_POEM_chem_2017_2021)
# fluorescence_rfu turbidity_ntu
data_profil_ctd  = read_profil_ctd.manual_ctd_2m(read_profil_ctd.fname_CHIFRE_POEM_ctd_2017_2021)
# fluorescence_rfu turbidity_ntu oxygen_mgl
data_ctd0 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2020)
data_ctd1 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2021)
data_ctd = pd.concat([data_ctd0,data_ctd1],sort=False)
# debit
data_debit0 = read_debit.debit(read_debit.fname_debit_2020_Tet)
data_debit1 = read_debit.debit(read_debit.fname_debit_2021_Tet)
data_debit = pd.concat([data_debit0,data_debit1],sort=False)

# cut profil and chemistry
data_profil_ctd = data_profil_ctd[(data_profil_ctd.datetime >= data_ctd.datetime.min()) & (data_profil_ctd.datetime <= data_ctd.datetime.max())]
data_chem = data_chem[(data_chem.datetime >= data_ctd.datetime.min()) & (data_chem.datetime <= data_ctd.datetime.max())]

# sample data_ctd with datetime from data_chem
df_ctd_med1h = pd.DataFrame()
df_ctd_med1h["datetime"] = data_chem["datetime"]
df_ctd_med1h["mdates"] = data_chem["mdates"]
df_ctd_med1h["fluorescence_rfu"] = [med_1h(data_ctd,"fluorescence_rfu",dt) for dt in df_ctd_med1h["datetime"]]


#%% select and polyfit
# selected some chlA values

df_selected = pd.DataFrame(True,index=df_ctd_med1h.index,columns=["ChlA"])
#df_selected[df_ctd_med1h.fluorescence_rfu>2]=False
#df_selected[df_ctd_med1h.fluorescence_rfu<0.01]=False
#df_selected[data_chem.ChlA_ug_L>3]=False

# polyfit Chla
df_tmp = pd.DataFrame()
df_tmp["x"] = data_chem[df_selected.ChlA].ChlA_ug_L
df_tmp["y"] = df_ctd_med1h[df_selected.ChlA].fluorescence_rfu
df_tmp.dropna(inplace=True)

a,b = np.polyfit(df_tmp.x,df_tmp.y,deg=1)

x_fit = np.array([df_tmp.x.min(),df_tmp.x.max()])
y_fit = a*x_fit+b


# =============================================================================
# polyfit profil ctd and chem
# =============================================================================
tmp1 = data_chem[["datetime","ChlA_ug_L"]]
tmp1.datetime = pd.to_datetime(tmp1.datetime.dt.strftime("%Y-%m-%d"))
tmp2 = data_profil_ctd[["datetime","fluorescence_rfu"]]
tmp2.datetime = pd.to_datetime(tmp2.datetime.dt.strftime("%Y-%m-%d"))

df_join = pd.merge(tmp1,tmp2,on="datetime")

c,d = np.polyfit(df_join.fluorescence_rfu,df_join.ChlA_ug_L,deg=1)

x_fit1 = np.array([df_join.fluorescence_rfu.min(),df_join.fluorescence_rfu.max()])
y_fit1 = c*x_fit1+d


CHIFRE_ctd_fitted = df_join.copy()
CHIFRE_ctd_fitted.fluorescence_rfu = CHIFRE_ctd_fitted.fluorescence_rfu*c+d



# =============================================================================
# %% graph chla
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(32*(25/64),24*(25/64))

ax.plot(data_ctd.mdates,data_ctd.fluorescence_rfu,label="ctd HF")
#ax.plot(df_ctd_med1h.mdates,df_ctd_med1h.fluorescence_rfu,label="resample ctd BF")
ax.plot(data_profil_ctd.mdates,data_profil_ctd.fluorescence_rfu,label="profil ctd CHIFRE BF",marker="x")
#ax.plot(data_profil_ctd.mdates,CHIFRE_ctd_fitted.fluorescence_rfu,label="profil ctd CHIFRE BF fitted",marker="x")

#ax.semilogy()

for j,val in enumerate(df_ctd_med1h.fluorescence_rfu.values):
    tmp_y = df_ctd_med1h.fluorescence_rfu.values[j]
    tmp_x = df_ctd_med1h.mdates.values[j]
    ax.text(x=tmp_x,y=tmp_y,s=val)  
ax.set_ylabel("fluorescence_rfu")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.set_xticks(data_chem.mdates)
ax.legend()
ax.grid(True)

ax2 = ax.twinx()
ax2.plot(data_chem.mdates,data_chem.ChlA_ug_L,label="chem BF",marker="x",c="tab:red")
ax2.set_ylabel("ChlA_ug_L")
ax2.legend()

#ax3 = ax2.twinx()
#ax3.plot(data_debit.mdates,data_debit.debit_m3_s,label="debit_m3_s Têt",c="tab:green")

fig.autofmt_xdate()
title = "fluo HF and ChlA chem"
fig.suptitle(title)
#fig.savefig("fig/"+fm.to_img_fname(title))

# =============================================================================
#%% HF vs BF ChlA
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])

ax.scatter(data_chem[df_selected.ChlA].ChlA_ug_L,df_ctd_med1h[df_selected.ChlA].fluorescence_rfu,marker="x") # hf vs bf
ax.plot(x_fit,y_fit,label="fit")
ax.set_ylabel("HF fluorescence_rfu")
ax.set_xlabel("BF ChlA_ug_L")
ax.grid(True)
for j,date in enumerate(df_ctd_med1h[df_selected.ChlA]["datetime"].values):
    tmp_date = pd.to_datetime(date).strftime("%Y-%m-%d")
    tmp_y = df_ctd_med1h[df_selected.ChlA].fluorescence_rfu.values[j]
    tmp_x = data_chem[df_selected.ChlA].ChlA_ug_L.values[j]
    ax.text(x=tmp_x,y=tmp_y,s=tmp_date)   

title = "fluo HF vs ChlA chem"
fig.suptitle(title)
#fig.savefig("fig/"+fm.to_img_fname(title))

# =============================================================================
#%% BF CHIFRE chem vs BF CHIFRE ctd
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])

ax.scatter(df_join.fluorescence_rfu,df_join.ChlA_ug_L,marker="x")
ax.plot(x_fit1,y_fit1,label="fit")
ax.set_xlabel("HF fluorescence_rfu")
ax.set_ylabel("BF ChlA_ug_L")
ax.grid(True)
for j,date in enumerate(df_join.datetime.values):
    tmp_date = pd.to_datetime(date).strftime("%Y-%m-%d")
    tmp_x = df_join.fluorescence_rfu.values[j]
    tmp_y = df_join.ChlA_ug_L.values[j]
    ax.text(x=tmp_x,y=tmp_y,s=tmp_date)   

title = "fluo HF vs ChlA chem"
fig.suptitle(title)
#fig.savefig("fig/"+fm.to_img_fname(title))