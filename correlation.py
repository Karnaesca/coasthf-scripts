import pandas as pd
import read_ctd as ctd
import matplotlib.pyplot as plt
import numpy as np

data_ctd_2020 = ctd.ctd(ctd.fname_POEM_ctd_2020)

#%%

data_ctd_2020.set_index("datetime",inplace=True)

keys = list(data_ctd_2020.keys())
#keys.remove("datetime")
keys.remove("mdates")

dkeys = {}
for i,e in enumerate(keys):
    dkeys[i]=e

matcorr = data_ctd_2020[keys].corr()
plt.matshow(matcorr)
plt.colorbar()
plt.xticks(np.arange(len(keys)),labels=keys)
plt.yticks(np.arange(len(keys)),labels=keys)


#%% 
freq = 1/(5*60)
#interval = 7*24*3600 # a week
interval = 3600*24
sample_size = int(freq*interval)

data_size = len(data_ctd_2020)
nb_section = int(data_size/sample_size)

nb_param = len(keys)

res = pd.DataFrame(index=keys,columns=keys)

store_corr = [0]*nb_param
for i in range(nb_param):
    store_corr[i] = [0]*nb_param
    
for i in range(nb_param):
    for j in range(nb_param):
        store_corr[i][j]=[]
        
store_med = [0]*nb_param
for i in range(nb_param):
    store_med[i] = []
    
# Calcul la correlation et median par section
for i in range(nb_section):
    section = data_ctd_2020[keys].iloc[int(i*sample_size):int((i+1)*sample_size)]
    corr = section.corr()
    med = section.median()
    for i in range(nb_param):
        store_med[i].append(med.iloc[i])
        for j in range(nb_param):
            store_corr[i][j].append(corr.iloc[i][j])

# Normalisation
for i in range(nb_param):
    store_med[i] = np.asarray(store_med[i])
    tmp_min = np.nanmin(store_med[i])
    tmp_max = np.nanmax(store_med[i])
    store_med[i] = (store_med[i]-tmp_min)/(tmp_max-tmp_min)*2-1


#%% plot mess
x = np.arange(nb_section)

for i in range(0,nb_param):
    plt.plot(x,store_med[i],label=f"{dkeys[i]}",zorder=-10+i,alpha=0.5)
    for j in range(i+1,nb_param):
        name = f"{dkeys[i]} vs {dkeys[j]}"
        plt.scatter(x,store_corr[i][j],label=name,alpha=0.5)
        plt.legend(bbox_to_anchor=(1,1), loc="upper left")
        #plt.tight_layout()
    
#%% plot hist
x = np.arange(nb_section)
fig,ax = plt.subplots(nrows=2,ncols=5)
ax = ax.flatten()
k = 0
for i in range(0,nb_param):
    for j in range(i+1,nb_param):
        name = f"{dkeys[i]} vs {dkeys[j]}"
        ax[k].hist(store_corr[i][j],label=name)
        ax[k].set_xlim((-1,1))
        ax[k].set_ylim((0,70))
        ax[k].set_title(name)
        k+=1
    






