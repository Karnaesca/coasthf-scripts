import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import read_chemistry
import read_profil_ctd
import read_ctd
import read_debit
import file_manager as fm
from datetime import timedelta
from App_v2 import Data, App

# function
def med_1h(data,param,datetime):
    dt_min = datetime-timedelta(minutes=30)
    dt_max = datetime+timedelta(minutes=30)
    res = data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)].median()
    tmp =  data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)]
    print(tmp)
    return res

# o2_ml_L ChlA_ug_L MES_mg_L
data_chem = read_chemistry.chem_3m(read_chemistry.fname_chem)
# fluorescence_rfu turbidity_ntu
data_profil_ctd  = read_profil_ctd.manual_ctd_2m(read_profil_ctd.fname_profil_ctd)
# fluorescence_rfu turbidity_ntu oxygen_mgl
data_ctd0 = read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
data_ctd1 = read_ctd.ctd(read_ctd.fname_ctd_2021_med5min)
data_ctd = pd.concat([data_ctd0,data_ctd1],sort=False)
# debit
data_debit0 = read_debit.debit(read_debit.fname_debit_2020_Tet)
data_debit1 = read_debit.debit(read_debit.fname_debit_2021_Tet)
data_debit = pd.concat([data_debit0,data_debit1],sort=False)


# cut profil and chemistry
data_profil_ctd = data_profil_ctd[(data_profil_ctd.datetime >= data_ctd.datetime.min()) & (data_profil_ctd.datetime <= data_ctd.datetime.max())]
data_chem = data_chem[(data_chem.datetime >= data_ctd.datetime.min()) & (data_chem.datetime <= data_ctd.datetime.max())]

# conversion
dO2 = 1.42763 #kg·m-3, mg/mL
data_chem["oxygen_mgl"] = data_chem["o2_ml_L"]*dO2

# sample data_ctd with datetime from data_chem
df_ctd_med1h = pd.DataFrame()
df_ctd_med1h["datetime"] = data_chem["datetime"]
df_ctd_med1h["mdates"] = data_chem["mdates"]
df_ctd_med1h["fluorescence_rfu"] = [med_1h(data_ctd,"fluorescence_rfu",dt) for dt in df_ctd_med1h["datetime"]]

#%% selected some chlA values

df_selected = pd.DataFrame(True,index=df_ctd_med1h.index,columns=["ChlA"])
#df_selected[df_ctd_med1h.fluorescence_rfu>=1]=False
#df_selected[df_ctd_med1h.fluorescence_rfu<0.01]=False

#%% polyfit Chla


# =============================================================================
#%% graph oxygen
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(32*(25/64),24*(25/64))

ax.plot(data_ctd.mdates,data_ctd.oxygen_mgl,label="ctd HF")
ax.set_ylabel("oxygen_mgl")
ax.grid(True)
ax.plot(data_chem.mdates,data_chem.oxygen_mgl,label="chem BF",marker="x")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.legend()
    
fig.autofmt_xdate()
title = "graph oxygen chemistry and ctd HF 2020 2021"
fig.suptitle(title)
fig.savefig(fm.to_img_fname(title))

# =============================================================================
# %% graph chla
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(32*(25/64),24*(25/64))

ax.plot(data_ctd.mdates,data_ctd.fluorescence_rfu,label="ctd HF")
ax.plot(df_ctd_med1h.mdates,df_ctd_med1h.fluorescence_rfu,label="resample ctd BF")
ax.scatter(df_ctd_med1h.mdates,df_ctd_med1h.fluorescence_rfu,label="resample ctd BF",cmap=plt.cm.get_cmap("bwr").reversed(),c=df_selected.ChlA,zorder=20)
for j,val in enumerate(df_ctd_med1h.fluorescence_rfu.values):
    tmp_y = df_ctd_med1h.fluorescence_rfu.values[j]
    tmp_x = df_ctd_med1h.mdates.values[j]
    ax.text(x=tmp_x,y=tmp_y,s=val)  
ax.set_ylabel("fluorescence_rfu")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.set_xticks(data_chem.mdates)
ax.legend()
ax.grid(True)

ax2 = ax.twinx()
ax2.plot(data_chem.mdates,data_chem.ChlA_ug_L,label="chem BF",marker="x",c="tab:red")
ax2.set_ylabel("ChlA_ug_L")
ax2.legend()

ax3 = ax2.twinx()
ax3.plot(data_debit.mdates,data_debit.debit_m3_s,label="debit_m3_s Têt",c="tab:green")

fig.autofmt_xdate()
title = "fluo HF and ChlA chem"
fig.suptitle(title)
#fig.savefig(fm.to_img_fname(title))

# =============================================================================
#%% HF vs BF ChlA
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])

ax.scatter(data_chem[df_selected.ChlA].ChlA_ug_L,df_ctd_med1h[df_selected.ChlA].fluorescence_rfu,marker="x") # hf vs bf
ax.set_ylabel("HF fluorescence_rfu")
ax.set_xlabel("BF ChlA_ug_L")
ax.grid(True)
for j,date in enumerate(df_ctd_med1h[df_selected.ChlA]["datetime"].values):
    tmp_date = pd.to_datetime(date).strftime("%Y-%m-%d")
    tmp_y = df_ctd_med1h[df_selected.ChlA].fluorescence_rfu.values[j]
    tmp_x = data_chem[df_selected.ChlA].ChlA_ug_L.values[j]
    ax.text(x=tmp_x,y=tmp_y,s=tmp_date)   

title = "fluo HF vs ChlA chem"
fig.suptitle(title)


