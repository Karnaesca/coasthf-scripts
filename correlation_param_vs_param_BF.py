import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm

# figure size
cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

# load the data
if 'data_POEM_BF' not in locals():
    data_POEM_BF = cmp.cmp(cmp.fname_data_POEM_1d_med)

# GRAPH DATA PREP

# keys the keys we want from the df
keys = list(data_POEM_BF.keys())
keys = [key for key in keys if ("datetime" not in key) and ("mdates" not in key)]

nb_keys = len(keys)

# produce a dict index to key
dkeys = {}
for i,e in enumerate(keys):
    dkeys[i]=e

# loop of every unique combination of keys
select = []
for i in range(0,nb_keys):
    for j in range(i+1,nb_keys):
        select.append([i,j])        

nselect = len(select)

# calculate shape
ncols = 6
nrows = int(np.ceil(nselect/ncols))

# GRAPH

fig, axs = plt.subplots(nrows,ncols)
fig.set_size_inches(*SIZE)

cmap = "viridis"
norm = Normalize(vmin=data_POEM_BF.mdates.iloc[0],vmax=data_POEM_BF.mdates.iloc[-1])

index = 0
for x in range(ncols):
    for y in range(nrows):
        if index < nselect:
            ax = axs[x][y]
            col = dkeys[select[index][0]]
            row = dkeys[select[index][1]]
            ax.scatter(data_POEM_BF[col],data_POEM_BF[row],marker=".",c=data_POEM_BF.mdates,alpha=0.5)
            ax.set_xlabel(col)
            ax.set_ylabel(row)
        index += 1
        
fig.tight_layout(pad=0)
fname = fm.build_path(os.getcwd(),"_build","raw","correlation_POEM_BF_raw")
fig.savefig(fname,bbox_inches='tight')
        

        