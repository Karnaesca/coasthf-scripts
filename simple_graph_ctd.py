# imports
import read_ctd as ctd
import read_met as met
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import file_manager as fm
import read_composite as cmp

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_portait

# 2020 et 2021
# data_ctd0 = ctd.ctd(ctd.fname_POEM_ctd_2020)
# data_ctd1 = ctd.ctd(ctd.fname_POEM_ctd_2021)
# data_ctd_POEM = pd.concat((data_ctd0,data_ctd1))
# data_met0 = met.met(met.fname_POEM_met_2020)
# data_met1 = met.met(met.fname_POEM_met_2021)
# data_met_POEM = pd.concat((data_met0,data_met1))
# data_POEM_2020 = pd.merge(data_ctd0,data_met0,how="outer",on="datetime")
# data_POEM_2021 = pd.merge(data_ctd1,data_met1,how="outer",on="datetime")
# data_POEM = pd.merge(data_ctd_POEM,data_met_POEM,how="outer",on="datetime")

print("Load data :")
data_POEM = cmp.cmp(cmp.fname_data_POEM_1d_med)
data_POEM_2020 = data_POEM[(data_POEM.datetime >= "2020") & (data_POEM.datetime < "2021")]
data_POEM_2021 = data_POEM[(data_POEM.datetime >= "2021") & (data_POEM.datetime < "2022")]

#%%

s_trad="""
oxygen_mgl:Oxygène $(mg/L)$
atmosphericpressure_mbar:Pression Atmo. $(mbar)$
airtemperature_degc:Température Air $(°C)$
windspeed_kn:Vitesse Vent $(kn)$
windirection_deg:Direction Vent $(°)$
temperature_degc:Température Eau $(°C)$
salinity:Salinité $(ratio)$
fluorescence_rfu:Fluoréscence $(RFU)$
turbidity_ntu:Turbidité $(NTU)$
"""
t_trad = np.array([e.split(":") for e in s_trad.strip().split("\n")])
d_trad = {k:v for k,v in t_trad}

def trad(val):
    sres = f"{d_trad[val]}"
    return sres

def graph(data,fname):
    datetime_col_name="datetime"
    keys = list(data.keys())
    keys.remove(datetime_col_name)
    keys = [key for key in keys if "mdates" not in key]
    data.loc[:,("mdates")] = [mdates.date2num(e) for e in data.datetime]
        
    fig, axes = plt.subplots(len(keys),1,sharex=True)
    fig.set_size_inches(*SIZE)
    
    for i,key in enumerate(keys):
        print(key)
        axes[i].plot(data.mdates,data[key])
        axes[i].set_ylabel(trad(key))
        axes[i].grid(True)
        axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
        axes[i].xaxis.set_major_locator(mdates.MonthLocator())
        #axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
        #axes[i].legend()
    
    fig.set_dpi(150.0)
    fig.autofmt_xdate()
    fig.tight_layout()
    title=f"Données brutes océanographique POEM 2020"
    title=""
    print(title)
    fig.suptitle(title)
    fig.savefig(fname,bbox_inches='tight')
    plt.close()
    
    
if __name__ == '__main__':
    print("build figure 1")
    fname = fm.build_path("_build","raw","simple_graph_POEM",ext=".png")
    graph(data_POEM,fname)
    
    print("build figure 2")
    fname = fm.build_path("_build","raw","simple_graph_POEM_2020",ext=".png")
    graph(data_POEM_2020,fname)
    
    print("build figure 3")
    fname = fm.build_path("_build","raw","simple_graph_POEM_2021",ext=".png")
    graph(data_POEM_2021,fname)