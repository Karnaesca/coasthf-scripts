# load official libs
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import numpy as np

# load custom libs
import read_ctd
import read_debit
import read_chemistry
import file_manager as fm
import load_hydroportail as lh

# load data
data_POEM_2021 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2021)
data_SOLA_2021 = read_ctd.ctd(read_ctd.fname_SOLA_ctd_2021)
data_debit_2021 = read_debit.debit(read_debit.fname_debit_2021_Tet)
data_chem = read_chemistry.chem_3m(read_chemistry.fname_CHIFRE_POEM_chem_2017_2021)

# cut data
data_chem_2021 = data_chem[(data_chem.datetime>="2021") & (data_chem.datetime<="2022")]

datetime_col_name = "datetime"
keys = list(data_POEM_2021.keys())
keys.remove(datetime_col_name)
keys.remove("mdates")

#%% graph all
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(29.7*(25/64),21*(25/64))
#fig.tight_layout(rect=[0, 0.03, 1, 0.95])

for i,key in enumerate(keys):
    print(f"graph {key} :")
    axes[i].plot(data_POEM_2021.mdates,data_POEM_2021[key],label="data POEM",alpha=1,zorder=0)
    axes[i].plot(data_SOLA_2021.mdates,data_SOLA_2021[key],label="data SOLA",alpha=1,zorder=1,c="tab:orange")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].legend()
    
    ax2 = axes[i].twinx()
    ax2.plot(data_debit_2021.mdates,data_debit_2021.debit_m3_s,label="debit Têt",alpha=0.5,zorder=2,c="tab:green")
    ax2.set_ylabel("debit_m3_s")
    ax2.legend()

fig.set_dpi(150.0)
fig.autofmt_xdate()
title = "compare ctd POEM SOLA 2021"
fig.suptitle(title)
#fig.savefig("fig/"+fm.to_img_fname(title))

#%% graph SOLA POEM chem O2
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))

ax.plot(data_POEM_2021.mdates,data_POEM_2021.oxygen_mgl,label="POEM oxygen mgl")
ax.plot(data_SOLA_2021.mdates,data_SOLA_2021.oxygen_mgl,label="SOLA oxygen mgl")
ax.plot(data_chem_2021.mdates,data_chem_2021.o2_ml_L/0.7,label="POEM chem oxygen mgl")
ax.legend()
ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.set_ylabel("oxygen mg l")

ax2 = ax.twinx()
ax2.plot(data_debit_2021.mdates,data_debit_2021.debit_m3_s,label="debit Têt",alpha=0.5,zorder=2,c="tab:red")
ax2.set_ylabel("debit_m3_s")
ax2.legend()

fig.set_dpi(150.0)
fig.autofmt_xdate()
title = "SOLA POEM chem O2"
fig.suptitle(title)
#fig.savefig("fig/"+fm.to_img_fname(title))