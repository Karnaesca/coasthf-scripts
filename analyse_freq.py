import read_ctd as ctd
import grapher
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
from six.moves import input as raw_input
import sys
import file_manager as fm
import tools as tl

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)


#%%
data = data_POEM_HF[["datetime","salinity"]]
data.index = data.datetime
data.salinity = data.salinity.interpolate(method="time")

# =============================================================================
# GRAPH
# =============================================================================

rmed = data.salinity.rolling(5,center=True).median()

fig, ax = plt.subplots()

ax.plot(data.index,data.salinity)
ax.plot(data.index,data.salinity.where((np.absolute(data.salinity-rmed)<1)))
#ax.plot(data.index,rmed)
#ax.plot(data.index,np.absolute(data.salinity-rmed))
ax.set_ylabel("salinity")
ax.grid(True)
ax.legend()
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()

# %%=============================================================================
# FREQ THINGY
# =============================================================================



dr = pd.date_range("2020-01-01","2021-12-31",periods=12)

cuts = [[dr[i],dr[i+1]] for i in range(len(dr)-1)]

for cut in cuts:
    
    title = f'{cut[0].strftime("%Y-%m-%d")}'

    chunk = tl.timecut(data, *cut)
    
    rmed = chunk.salinity.rolling(5*24*12,center=True).median()
    
    #chunk.salinity = chunk.salinity - rmed
    
    chunk.dropna(inplace=True)
    
    sample = chunk.salinity.values
    
    # fig, ax = plt.subplots()
    # ax.plot(chunk.index,chunk.salinity)
    
    n = len(sample)
    d = 300 #interval in secs between sample
    
    sp = np.fft.rfft(sample)
    freq = np.fft.rfftfreq(n=n,d=d)
    
    
    fig, ax = plt.subplots(2)
    
    ax[0].plot(freq, sp.real,label="spectrum")
    ax[0].set_xlabel("frequency (Hz)")
    ax[0].set_xlim((1/(3600*24*30*6),1/300/4/4))
    ax[0].set_ylim((2000,-2000))
    ax[0].set_ylabel("energy")
    ax[1].plot(chunk.index,chunk.salinity,label="raw")
    #ax[1].plot(chunk.index,rmed)
    ax[1].set_xlabel("t")
    ax[1].set_ylabel("salinty")
    ax[0].grid(True)
    ax[1].grid(True)
    ax[0].legend()
    ax[1].legend()
    
    fig.suptitle(title)
    fname = fm.build_path(fm.root,"freq",title)
    print(fname)
    fig.savefig(fname)
    plt.close()
    
