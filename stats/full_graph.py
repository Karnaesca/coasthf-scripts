import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait

#%% ===========================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = cmp.cmp(cmp.fname_POEM_ctd_2020_2021_good)
data_POEM_HF = tl.addmdates(data_POEM_HF)

#%% ===========================================================================
# GRAPH CTD
# =============================================================================

datetime_col_name="datetime"
keys = [key for key in list(data_POEM_HF.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
    
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    # HF data
    axes[i].plot(data_POEM_HF.mdates,data_POEM_HF[key],label="POEM HF",c=tl.color(0))
    
    axes[i].set_ylabel(tl.col2name(key))
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())
    #axes[i].xaxis.set_minor_locator(mdates.DayLocator())
    axes[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(hspace=0)

# fname = fm.build_path(os.getcwd(),"_build","raw","full_graph_POEM")
# fig.savefig(fname,bbox_inches='tight')