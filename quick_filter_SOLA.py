import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# =============================================================================
# LOAD DATA
# =============================================================================
data_SOLA_HF = ctd.ctd(ctd.fname_SOLA_ctd_2021)
datetime_col_name="datetime"
keys = [key for key in list(data_SOLA_HF.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
start, end = pd.to_datetime(['2021-08-20T10:16', '2021-12-01T10:45'])
data_SOLA_HF = tl.timecut(data_SOLA_HF, start, end)

data_POEM_HF = cmp.cmp(cmp.fname_POEM_ctd_2020_2021_good)
data_POEM_HF = tl.addmdates(data_POEM_HF)
data_POEM_HF = tl.timecut(data_POEM_HF, start, end)

data_hydro_T = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data
data_hydro_B = hydro.HydroSeries(hydro.station_Baillaury_Banyuls,start=start,end=end).data

# %%=============================================================================
# FILTERING META
# =============================================================================
df_select_SOLA = pd.DataFrame(True, index=data_SOLA_HF.index, columns=data_SOLA_HF.columns)
df_select_POEM = pd.DataFrame(True, index=data_POEM_HF.index, columns=data_POEM_HF.columns)

# %%=============================================================================
# FILTERING SALINITY SOLA
# =============================================================================
cuts = [
        ['2021-07-23T06:49', '2021-07-26T08:01']
        ]
cuts = [pd.to_datetime(cut) for cut in cuts]

t = data_SOLA_HF.datetime
salinity = data_SOLA_HF.salinity

window_mean = 288
window_std = int(window_mean*2)
f = 3

mean = salinity.rolling(window_mean,center=True,min_periods=1).mean()
std = salinity.rolling(window_std,center=True,min_periods=1).std()

threshold = 1
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (salinity <= over) & (salinity >= under)

nb_rejected = (~select).sum() - salinity.isnull().sum()
print(f"Nb rejected : {nb_rejected}")

select[(t>cuts[0][0]) & (t<cuts[0][1])] = False

df_select_SOLA.salinity = select

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(t,salinity)
ax.plot(t,mean)
ax.plot(t,over)
ax.plot(t,under)
ax.scatter(t,salinity.where(~select),marker="x",c="y")
ax.set_ylabel("")
ax.legend()

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# %%=============================================================================
# FILTERING SALINITY POEM
# =============================================================================
cuts = [
        ['2021-07-24T23:28', '2021-08-03T18:32']
        ]
cuts = [pd.to_datetime(cut) for cut in cuts]

t = data_POEM_HF.datetime
salinity = data_POEM_HF.salinity

window_mean = 288
window_std = int(window_mean*2)
f = 3

mean = salinity.rolling(window_mean,center=True,min_periods=1).mean()
std = salinity.rolling(window_std,center=True,min_periods=1).std()

threshold = 0.5
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (salinity <= over) & (salinity >= under)

nb_rejected = (~select).sum() - salinity.isnull().sum()
print(f"Nb rejected : {nb_rejected}")

select[(t>cuts[0][0]) & (t<cuts[0][1])] = False

df_select_POEM.salinity = select


fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(t,salinity)
ax.plot(t,mean)
ax.plot(t,over)
ax.plot(t,under)
ax.scatter(t,salinity.where(~select),marker="x",c="y")
ax.set_ylabel("")
ax.legend()

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# %%=============================================================================
# FILTERING FLUO SOLA
# =============================================================================
cuts = [
        ['2021-07-08T01:56', '2021-08-11T05:12']
        ]
cuts = [pd.to_datetime(cut) for cut in cuts]

t = data_SOLA_HF.datetime
fluorescence = data_SOLA_HF.fluorescence_rfu

window_mean = 288
window_std = int(window_mean*2)
f = 3
mean = fluorescence.rolling(window_mean,center=True,min_periods=1).mean()
std = fluorescence.rolling(window_std,center=True,min_periods=1).std()

threshold = 0.5
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (fluorescence <= over) & (fluorescence >= under)

nb_rejected = (~select).sum() - fluorescence.isnull().sum()
print(f"Nb rejected : {nb_rejected}")

select[(t>cuts[0][0]) & (t<cuts[0][1])] = False

df_select_SOLA.fluorescence_rfu = select

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(t,fluorescence)
ax.plot(t,mean)
ax.plot(t,over)
ax.plot(t,under)
ax.scatter(t,fluorescence.where(~select),marker="x",c="y")
ax.set_ylabel("")
ax.legend()

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# %%=============================================================================
# FILTERING TURBI SOLA
# =============================================================================
t = data_SOLA_HF.datetime
turbidity = data_SOLA_HF.turbidity_ntu

window_mean = 288
window_std = int(window_mean*2)
f = 3
mean = turbidity.rolling(window_mean,center=True,min_periods=1).mean()
std = turbidity.rolling(window_std,center=True,min_periods=1).std()

threshold = 1
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (turbidity <= over) & (turbidity >= under)

nb_rejected = (~select).sum() - turbidity.isnull().sum()
print(f"Nb rejected : {nb_rejected}")

select[(t>cuts[0][0]) & (t<cuts[0][1])] = False

df_select_SOLA.turbidity_ntu = select

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(t,turbidity)
ax.plot(t,mean)
ax.plot(t,over)
ax.plot(t,under)
ax.scatter(t,turbidity.where(~select),marker="x",c="y")
ax.set_ylabel("")
ax.legend()

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))

fig.autofmt_xdate()
fig.tight_layout(pad=0)

#%% =============================================================================
# INTERPOLATE
# =============================================================================
keys = [key for key in keys if (not "oxy" in key)]

data_POEM_HF_tmp = data_POEM_HF[keys].where(df_select_POEM[keys])
data_POEM_HF_tmp["datetime"] = data_POEM_HF.datetime
data_POEM_HF_tmp["mdates"] = data_POEM_HF.mdates
data_POEM_HF_tmp.index = data_POEM_HF_tmp["datetime"]

data_SOLA_HF_tmp = data_SOLA_HF[keys].where(df_select_SOLA[keys])
data_SOLA_HF_tmp["datetime"] = data_SOLA_HF.datetime
data_SOLA_HF_tmp["mdates"] = data_SOLA_HF.mdates
data_SOLA_HF_tmp.index = data_SOLA_HF_tmp["datetime"]

print("before interp")
print(data_POEM_HF_tmp[keys].isnull().sum())
print(data_SOLA_HF_tmp[keys].isnull().sum())

data_POEM_HF_tmp[keys] = data_POEM_HF_tmp[keys].interpolate(method='time')
data_SOLA_HF_tmp[keys] = data_SOLA_HF_tmp[keys].interpolate(method='time')

print("after interp")
print(data_POEM_HF_tmp[keys].isnull().sum())
print(data_SOLA_HF_tmp[keys].isnull().sum())



# %% =============================================================================
# FULL GRAPH
# =============================================================================    
fig, ax = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    ax[i].plot(data_SOLA_HF.mdates,data_SOLA_HF[key],label="SOLA HF",c=tl.color(1))
    
    ax[i].plot(data_POEM_HF.mdates,data_POEM_HF[key],label="POEM HF corrected",c=tl.color(0),zorder=-10,alpha=0.5)
    
    ax2 = ax[i].twinx()
    ax2.plot(data_hydro_T.mdates,data_hydro_T.volume_l_s,label="Têt $(L/s)$",c=tl.color(2),alpha=0.5)
    ax2.plot(data_hydro_B.mdates,data_hydro_B.volume_l_s,label="Baillaury", c=tl.color(3),alpha=0.5)
    
    ax[i].set_ylabel(tl.col2name(key))
    ax[i].grid(True)
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax[i].xaxis.set_major_locator(mdates.MonthLocator())
    ax[i].xaxis.set_minor_locator(mdates.DayLocator())
    ax[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# %% =============================================================================
# FULL GRAPH CLEAN
# =============================================================================    
fig, ax = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    ax[i].plot(data_SOLA_HF_tmp.mdates,data_SOLA_HF_tmp[key],label="SOLA HF",c=tl.color(1))
    
    ax[i].plot(data_POEM_HF_tmp.mdates,data_POEM_HF_tmp[key],label="POEM HF",c=tl.color(0),zorder=-10,alpha=0.5)
    
    ax2 = ax[i].twinx()
    ax2.plot(data_hydro_T.mdates,data_hydro_T.volume_l_s,label="Têt $(L/s)$",c=tl.color(2),alpha=0.5)
    ax2.plot(data_hydro_B.mdates,data_hydro_B.volume_l_s,label="Baillaury", c=tl.color(3),alpha=0.5)
    
    ax[i].set_ylabel(tl.col2name(key))
    ax[i].grid(True)
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax[i].xaxis.set_major_locator(mdates.MonthLocator())
    ax[i].xaxis.set_minor_locator(mdates.DayLocator())
    ax[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)

# %% =============================================================================
# SAVE PART DATA
# =============================================================================

root = os.getcwd()

fname_poem = fm.build_path(root,"data","part","POEM_HF_clean_part",ext=".csv")
fname_sola = fm.build_path(root,"data","part","SOLA_HF_clean_part",ext=".csv")

cols = ["datetime"] + keys

data_POEM_HF_tmp = data_POEM_HF_tmp[cols]
data_SOLA_HF_tmp = data_SOLA_HF_tmp[cols]

data_POEM_HF_tmp.to_csv(fname_poem,index=False)
data_SOLA_HF_tmp.to_csv(fname_sola,index=False)


