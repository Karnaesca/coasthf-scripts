import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize
from matplotlib.widgets import  Button, SpanSelector
from matplotlib.colors import ListedColormap
from time import sleep
import shutil

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm

data = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

#%%

def chaos(y):
    y1 = y.interpolate()
    dy1 = (y1-y1.shift())**2
    y2 = dy1.rolling(5000,center=True,min_periods=1).mean()
    return y2


cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_portait

datetime_col_name="datetime"
keys = list(data.keys())
keys.remove(datetime_col_name)
keys = [key for key in keys if "mdates" not in key]
    
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*SIZE)

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data.mdates,data[key])
    ax2 = axes[i].twinx()
    ax2.plot(data.mdates,chaos(data[key]),c="tab:red")
    # axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())

fig.autofmt_xdate()
fig.tight_layout(pad=0)
