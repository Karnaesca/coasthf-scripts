import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# =============================================================================
# LOAD DATA
# =============================================================================
data_POEM = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

# %% =============================================================================
# COMPUTE THINGS
# =============================================================================

datetime_col_name="datetime"
keys = [key for key in list(data_POEM.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]

#temperature_degc salinity fluorescence_rfu turbidity_ntu oxygen_mgl
data_POEM_slopes = (data_POEM[keys]-data_POEM[keys].shift()).abs()

# =============================================================================
# GRAPH
# =============================================================================
# hist compare 2020 2021

#plot hist value
for key in keys:
    fig, ax = plt.subplots()
    fig.set_size_inches(*A4_paysage)
    
    ax.hist(data_POEM[key],bins=100)
    ax.set_xlabel(key)
    ax.semilogy()
   

# plot hist slope 
for key in keys:
    fig, ax = plt.subplots()
    fig.set_size_inches(*A4_paysage)
    
    ax.hist(data_POEM_slopes[key],bins=100)
    ax.set_xlabel(f"slope {key}")
    ax.semilogy()
   
# =============================================================================
# WEIRD STUFF
# =============================================================================

