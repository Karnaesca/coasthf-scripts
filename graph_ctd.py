import read_ctd
import read_profil_ctd
import grapher
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta
from matplotlib.colors import Normalize

# function
def med_1h(data,param,datetime):
    dt_min = datetime-timedelta(minutes=30)
    dt_max = datetime+timedelta(minutes=30)
    res = data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)].median()
    tmp =  data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)]
    print(tmp)
    return res

def med_1d(data,param,datetime):
    dt_min = datetime-timedelta(days=1)
    dt_max = datetime+timedelta(days=1)
    res = data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)].median()
    tmp =  data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)]
    print(tmp)
    return res

# load data
print("load data")
#data_ctd = read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
data_ctd = read_ctd.ctd(read_ctd.fname_POEM_ctd_2020)
#data_manual_ctd = read_profil_ctd.manual_ctd_2m(read_profil_ctd.fname_profil_ctd)
data_manual_ctd = read_profil_ctd.manual_ctd_2m(read_profil_ctd.fname_CHIFRE_POEM_ctd_2017_2021)

# config
print("config")
datetime_col_name = "datetime"
name = "ctd_manual"

# add mdate field
print("add mdate field")
data_ctd["mdates"] = [mdates.date2num(e) for e in data_ctd.datetime]
data_manual_ctd["mdates"] = [mdates.date2num(e) for e in data_manual_ctd.datetime]

# build the key list
print("build the key list")
keys = list(data_ctd.keys())
keys.remove(datetime_col_name)
keys.remove("mdates")
keys.remove("oxygen_mgl")

# filters
print("filters")
for key in keys:
    data_ctd[key] = data_ctd[key][data_ctd[key]>=0]
    

    #data_manual_ctd[key][data_manual_ctd[key]>=0]
    
# cut
print("cut")
dtmin = data_ctd["datetime"].min()
dtmax = data_ctd["datetime"].max()
data_manual_ctd = data_manual_ctd[(data_manual_ctd.datetime>=dtmin) & (data_manual_ctd.datetime<=dtmax)]

# vector for median to compare
print("vector for median to compare")
df_med_ctd = pd.DataFrame()
df_med_ctd["datetime"] = data_manual_ctd["datetime"]
df_med_ctd["mdates"] = data_manual_ctd["mdates"]
for key in keys:
    print(f"{key}")
    df_med_ctd[key]=[med_1h(data_ctd,key,dt) for dt in df_med_ctd.datetime]
    #df_med_ctd[key]=[med_1d(data_ctd,key,dt) for dt in df_med_ctd.datetime]


# calc diff
df_comp = (df_med_ctd[keys]-data_manual_ctd[keys]).abs()/data_manual_ctd[keys]

print(df_comp)

# display filter
df_filter = df_comp[df_comp>0.05]
df_filter = df_filter/df_filter
data_manual_ctd_filtered = data_manual_ctd*df_filter

#%% filter and polyfit

df_selected = pd.DataFrame(True,index=df_med_ctd.index,columns=df_med_ctd.columns)

# filter bad HF/BF for 2020
#df_selected.turbidity_ntu.iloc[-1] = False
#df_selected.turbidity_ntu.iloc[1] = False

# filter bad HF/BF for 2021
#df_selected.fluorescence_rfu.iloc[1]=False
#df_selected.fluorescence_rfu.iloc[4]=False
#df_selected.turbidity_ntu.iloc[4]=False


# polyfit
d_fit={}
for key in keys:
    tmp_x = data_manual_ctd[df_selected][key]
    tmp_y = df_med_ctd[df_selected][key]
    df_tmp = pd.DataFrame()
    df_tmp["x"]=tmp_x
    df_tmp["y"]=tmp_y
    df_tmp.dropna(inplace=True)
    a,b = np.polyfit(df_tmp.x,df_tmp.y,deg=1)
    d_fit[key]={}
    d_fit[key]["a"]=a
    d_fit[key]["b"]=b
    df_fit=pd.DataFrame()
    
print(d_fit)
        
#%% graph with only wrong value manual
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(29.7*(25/64),21*(25/64))
#cm = plt.cm.get_cmap('plasma')

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd.mdates,data_ctd[key],label="buoy data")
    #sc = axes[i].scatter(data_manual_ctd.mdates,data_manual_ctd[key],label="manual ctd",c=df_comp[key], marker="x",cmap=cm)
    axes[i].scatter(data_manual_ctd.mdates,data_manual_ctd_filtered[key],label="diff>=0.05 ctd",c="r", marker="x")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
    axes[i].legend()
    #axes[i].set_colorbar(sc)

str_date = data_ctd.datetime[0].strftime("%Y%m%d_%H%M%S")

fig.set_dpi(150.0)
plt.gcf().autofmt_xdate()
#plt.colorbar(sc)
#plt.savefig(f"{str_date}_{name}.png")
plt.show()

#%% graph 2 with all manual values
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])
#cm = plt.cm.get_cmap('plasma')

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd.mdates,data_ctd[key],label="buoy data",alpha=0.5,zorder=-10)
#    axes[i].scatter(data_manual_ctd.mdates,data_manual_ctd[key],label="data manual",marker="x")
#    axes[i].scatter(df_med_ctd.mdates,df_med_ctd[key],label="data buoy med 1h",marker="x")
    axes[i].plot(data_manual_ctd.mdates,data_manual_ctd[key],label="data manual",marker="x")
    axes[i].plot(df_med_ctd.mdates,df_med_ctd[key],label="data buoy med 1h",marker="x")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    #axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
    axes[i].set_xticks(data_manual_ctd.mdates)
    axes[i].legend()
    #axes[i].set_colorbar(sc)

str_date = data_ctd.datetime[0].strftime("%Y")

fig.set_dpi(300.0)
plt.gcf().autofmt_xdate()
#plt.colorbar(sc)
#plt.savefig(f"{str_date}_RAW_HF_BF.png")
plt.show()

#%% graph manual data vs buoy data
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(29.7*(25/64),21*(25/64))
#cm = plt.cm.get_cmap('plasma')

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_manual_ctd.mdates,data_manual_ctd[key],label="data manual")
    axes[i].plot(df_med_ctd.mdates,df_med_ctd[key],label="data buoy med 1h")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
    axes[i].legend()
    axes[i].set_title(f"Comparaison {key} mesure ctd 2m et mesure bouée median 1h (2020)")

str_date = data_ctd.datetime[0].strftime("%Y%m%d_%H%M%S")

fig.set_dpi(150.0)
plt.gcf().autofmt_xdate()
#plt.colorbar(sc)
#plt.savefig(f"{str_date}_HF_BF_along.png")
plt.show()

#%% HF vs BF with bad
fig, axes = plt.subplots(len(keys),1)
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.set_cmap("RdYlBu")
norm = Normalize(0,1)

for i,key in enumerate(keys):
    print(key)
    axes[i].scatter(data_manual_ctd[key],df_med_ctd[key],c=df_selected[key],norm=norm,marker="x") # hf vs bf
    axes[i].set_ylabel(f"HF {key}")
    axes[i].set_xlabel(f"BF {key}")
    axes[i].grid(True)
    for j,date in enumerate(data_manual_ctd["datetime"].values):
        tmp_date = pd.to_datetime(date).strftime("%Y/%m/%d")
        tmp_x = data_manual_ctd[key].values[j]
        tmp_y = df_med_ctd[key].values[j]
        axes[i].text(x=tmp_x,y=tmp_y,s=tmp_date)
    x_fit = np.array([data_manual_ctd[key].min(), data_manual_ctd[key].max()])
    y_fit = d_fit[key]["a"]*x_fit+d_fit[key]["b"]
    axes[i].plot(x_fit,y_fit)
   

str_date = data_ctd.datetime[0].strftime("%Y")

fig.set_dpi(300.0)
#plt.savefig(f"{str_date}_HF_BF_vs.png")
plt.show()

#%% HF vs BF without bad
fig, axes = plt.subplots(len(keys),1)
fig.set_size_inches(29.7*(25/64),21*(25/64))
fig.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.set_cmap("RdYlBu")
norm = Normalize(0,1)

for i,key in enumerate(keys):
    print(key)
    axes[i].scatter(data_manual_ctd[df_selected][key],df_med_ctd[df_selected][key],c=df_selected[key],norm=norm,marker="x") # hf vs bf
    axes[i].set_ylabel(f"HF {key}")
    axes[i].set_xlabel(f"BF {key}")
    axes[i].grid(True)
    for j,date in enumerate(data_manual_ctd["datetime"].values):
        tmp_date = pd.to_datetime(date).strftime("%Y/%m/%d")
        tmp_x = data_manual_ctd[key].values[j]
        tmp_y = df_med_ctd[key].values[j]
        axes[i].text(x=tmp_x,y=tmp_y,s=tmp_date)
    x_fit = np.array([data_manual_ctd[key].min(), data_manual_ctd[key].max()])
    y_fit = d_fit[key]["a"]*x_fit+d_fit[key]["b"]
    axes[i].plot(x_fit,y_fit)
   

str_date = data_ctd.datetime[0].strftime("%Y")

fig.set_dpi(300.0)
#plt.savefig(f"{str_date}_HF_BF_vs.png")
plt.show()