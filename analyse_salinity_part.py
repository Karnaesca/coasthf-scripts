import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

from meteostat import Stations
from meteostat import Hourly

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2021)
data_SOLA_HF = ctd.ctds(ctd.fname_SOLA_ctd_2021)

timeframe = ['2021-08-20T15:54', '2021-12-01T03:16']