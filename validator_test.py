import read_ctd
import grapher
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#datetime temperature_degc salinity fluorescence_rfu turbidity_ntu oxygen_mgl

#config
data_ctd = read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
datetime_name = "datetime"
data_ctd["mdates"] = [mdates.date2num(e) for e in data_ctd.datetime]

keys = list(data_ctd.keys())
keys.remove(datetime_name)
keys.remove("mdates")

slope = (data_ctd[keys]-data_ctd[keys].shift()).abs()
rmean = slope.rolling(10).mean()

# =============================================================================
# for key in keys:
#     tmp_min = data_ctd[key].min()
#     tmp_max = data_ctd[key].max()
#     tmp_range = tmp_max-tmp_min
#     print(f"{key} : min={tmp_min}, max={tmp_max}, range={tmp_range}, *0.1 {tmp_range*0.1}")
# =============================================================================

# =============================================================================
# init filter
# =============================================================================
df_good = pd.DataFrame().reindex_like(data_ctd[keys])
df_good[keys] = True

# =============================================================================
# filter 1 nan
# =============================================================================
test = pd.DataFrame().reindex_like(df_good[keys])
test = ~data_ctd[keys].isnull()

# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 1 - NaN - amount of rejected values (:\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test

# =============================================================================
# filter 2 slope
# =============================================================================

test = pd.DataFrame().reindex_like(df_good[keys])
d_max_slope={
        "temperature_degc" : 0.3,
        "salinity" : 4.6,
        "fluorescence_rfu" : 4,
        "turbidity_ntu" : 10,
        "oxygen_mgl" : 0.4
        }
for key in keys:
    tmp_test = (slope[key]<=d_max_slope[key])
    test[key] = (tmp_test & (tmp_test.shift()))
    
# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 2 - slope - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test

# =============================================================================
# filter 3 data from rmean
# =============================================================================

test = pd.DataFrame().reindex_like(data_ctd[keys])
d_max_mean={
        "temperature_degc" : 0.3,
        "salinity" : 2.2,
        "fluorescence_rfu" : 1,
        "turbidity_ntu" : 5,
        "oxygen_mgl" : 0.2
        }
for key in keys:
    test[key] = (rmean[key]<=d_max_mean[key])
    
# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 3 - mean slope - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test

# =============================================================================
# end filters
# =============================================================================

# calc graph
good_data = data_ctd[keys][df_good[keys]]
bad_data = data_ctd[keys][~df_good[keys]]

# graph
# =============================================================================
# graph raw + slope
# =============================================================================
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],data_ctd[key],label="RAW")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    axes2 = axes[i].twinx()
    axes2.plot(data_ctd["mdates"],slope[key],label="slope",c="tab:orange",alpha=0.5)
    axes[i].set_ylabel("slope")
    axes2.legend()
    
plt.gcf().autofmt_xdate()
plt.title("raw data + slope")
plt.savefig("raw_data_and_slope.png")

# =============================================================================
# graph raw + mean rolling slope
# =============================================================================
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],data_ctd[key],label="RAW")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    axes2 = axes[i].twinx()
    axes2.plot(data_ctd["mdates"],rmean[key],label="rolling mean slope",c="tab:orange",alpha=0.5)
    axes[i].set_ylabel("rolling mean slope")
    axes2.legend()
    
plt.gcf().autofmt_xdate()
plt.title("raw data + rolling mean slope on 10 values")
plt.savefig("raw_data_and_rmean.png")

# =============================================================================
# graph good and bad data
# =============================================================================
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],good_data[key],label="good")
    axes[i].plot(data_ctd["mdates"],bad_data[key],label="bad")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
plt.gcf().autofmt_xdate()
plt.title("good and bad")
plt.savefig("good_bad.png")

# =============================================================================
# graph only good data
# =============================================================================

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],good_data[key],label="good")
    #axes[i].plot(data_ctd["mdates"],bad_data[key],label="bad")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
plt.gcf().autofmt_xdate()
plt.title("only_good")
plt.savefig("only_good.png")

plt.show()