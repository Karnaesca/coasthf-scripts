import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

#%% ===========================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

# %%=============================================================================
# PROCESS
# =============================================================================

datetime_col_name="datetime"
keys = [key for key in list(data_POEM_HF.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]


x1 = data_POEM_HF[keys]

x0 = data_POEM_HF[keys].shift(1)
x2 = data_POEM_HF[keys].shift(-1)

sp = ((x2 - x0).abs())/2 

# =============================================================================
# GRAPH
# =============================================================================

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    axes[i].plot(data_POEM_HF.mdates,sp[key],label="pente",c=tl.color(0))
    axes[i].legend(loc=2)
    
    ax2 = axes[i].twinx()
    ax2.plot(data_POEM_HF.mdates,data_POEM_HF[key],label="raw",c=tl.color(1),zorder=-20,alpha=0.5)
    ax2.legend(loc=1)
    ax2.set_ylabel(f"raw {tl.col2shortname(key)}")
    
    axes[i].set_ylabel(f"pente {tl.col2shortname(key)}")
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())
    
    per = np.nanpercentile(sp[key],99.99)
    #axes[i].plot([data_POEM_HF.mdates.iloc[0],data_POEM_HF.mdates.iloc[-1]],[per,per],c="r",linestyle='dotted')

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.subplots_adjust(hspace=0)

fname = fm.build_path(os.getcwd(),"_build","test","plot_pente_POEM",ext=".svg")
fig.savefig(fname,bbox_inches='tight')

#
# 
#

fig, axes = plt.subplots(1,len(keys),sharey=True)
fig.set_size_inches(29.7*cm,10*cm)

for i,key in enumerate(keys):
    axes[i].hist(sp[key],bins=50)
    axes[i].semilogy()
    axes[i].grid(True)
    if i==2 :
        axes[i].set_xlabel("valeur de pente")
    if i==0 :
        axes[i].set_ylabel("occurences")
    axes[i].set_title(tl.col2name(key))
    per = np.nanpercentile(sp[key],99.99)
    #axes[i].plot([per,per],[0,10**5],c="r",linestyle='dotted')

fig.tight_layout(pad=0)
fig.subplots_adjust(wspace=0)

fname = fm.build_path(os.getcwd(),"_build","test","hist_pente_POEM",ext=".svg")
fig.savefig(fname,bbox_inches='tight')