import logging
from datetime import datetime

fnamelog = f'/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/logs/{__file__.split("/")[-1]}.log'
logging.basicConfig(format='%(message)s',filename = fnamelog, level=logging.INFO)
print = logging.info
header = f"""
# =============================================================================
# RUNNING SCRIPT : {__file__.split("/")[-1]} - {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
# =============================================================================
"""
print(header)