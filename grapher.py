import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def graph(data,name,save=False,show=False,datetime_col_name="datetime"):
    
    data["mdates"] = [mdates.date2num(e) for e in data.datetime]
    # keys = data.keys()[1:-1]  # remove dateime and mdates columns
    keys = list(data.keys())
    keys.remove(datetime_col_name)
    keys.remove("mdates")
    
    #for key in keys:
    #    data[key] = data[key].where(data[key]>0)
            
    fig, axes = plt.subplots(len(keys),1,sharex=True)
    fig.set_size_inches(21*(25/64),10*(25/64))
    
    for i,key in enumerate(keys):
        print(key)
        axes[i].plot(data.mdates,data[key])
        axes[i].set_ylabel(key)
        axes[i].grid(True)
        axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
        axes[i].xaxis.set_minor_locator(mdates.DayLocator())
        axes[i].legend()
    
    #str_date = data["datetime"][0].strftime("%Y%m%d_%H%M%S")
    str_date="err"
    
    #fig.set_dpi(150.0)
    plt.gcf().autofmt_xdate()
    if save: 
        plt.savefig(f"{str_date}_{name}.png")
    if show:
        plt.show()