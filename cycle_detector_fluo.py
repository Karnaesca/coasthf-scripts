import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

# %%=============================================================================
# CUT
# =============================================================================
cuts = [
    ["2020-01-25","2020-02-05"],
    ["2020-03-20","2020-04-05"],
    ["2020-04-15","2020-04-27"],
    ["2020-04-26","2020-05-10"],
    ["2020-09-24","2020-10-07"],
    ["2020-11-11","2020-11-24"],
    ["2021-02-27","2021-03-10"],
    ["2021-03-25","2021-04-10"],
    ["2021-05-25","2021-06-07"],
    ["2021-07-20","2021-08-05"]
    ]

#%% =============================================================================
# 
# =============================================================================

for cut in cuts[:4]:

    data = tl.timecut(data_POEM_HF, *cut)[["datetime","fluorescence_rfu"]]
    data.set_index("datetime",inplace=True)
    
    df = pd.DataFrame()
    
    for i,e in enumerate(data.resample("1D")):
        ts, sample = e
        sample.reset_index(drop=True,inplace=True)
        norm_sample = (sample-sample.min())/(sample.max()-sample.min())
        df[i] = norm_sample
        
    
    corr = df.corr()
    
    fig, ax = plt.subplots()
    m1 = ax.matshow(corr)
    fig.colorbar(m1)

    