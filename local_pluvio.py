#https://pypi.org/project/meteostat/
#https://dev.meteostat.net/python/#example

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

from meteostat import Stations
from meteostat import Hourly

# loc
coord_poem = {"lat" : 42.704167, "lon" : 3.066667}
# time frame
timeframe = pd.to_datetime(["2020-01-01","2021-12-31"])

stations = Stations()
stations = stations.nearby(*list(coord_poem.values()))
stations = stations.fetch(3)

data0 = Hourly(stations.iloc[0].wmo,*timeframe).fetch()
data1 = Hourly(stations.iloc[1].wmo,*timeframe).fetch()
data2 = Hourly(stations.iloc[2].wmo,*timeframe).fetch()

fig, ax = plt.subplots()
ax.plot(data0.index,data0.prcp)
ax.plot(data1.index,data1.prcp)
ax.plot(data2.index,data2.prcp)



