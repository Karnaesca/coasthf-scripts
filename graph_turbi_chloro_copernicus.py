import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import load_copernicus as coper
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
start = data_POEM_HF.datetime.iloc[0].strftime("%Y-%m-%d")
end = data_POEM_HF.datetime.iloc[-1].strftime("%Y-%m-%d")
data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

data_SPM_TUR_CHL = coper.get_SPM_CHL_TUR()

tl.warn()

# =============================================================================
# GRAPH
# =============================================================================
#%%

data_SPM_TUR_CHL.dropna(inplace=True)

fig, ax = plt.subplots(2)
fig.set_size_inches(*A4_paysage)

ax[0].plot(data_POEM_HF.datetime,data_POEM_HF.fluorescence_rfu,c=tl.color(0),label="fluo POEM")
ax[0].set_ylabel("fluo POEM")
ax[0].legend(loc=0)
ax[0].grid(True)
ax2 = ax[0].twinx()
ax2.plot(data_SPM_TUR_CHL.time,data_SPM_TUR_CHL.chl,c=tl.color(1),marker="x",label="CHL sat")
ax2.set_ylabel("CHL sat")
ax2.legend(loc=3)
ax2 = ax[0].twinx()
ax2.plot(data_hydro.datetime,data_hydro.volume_l_s,c=tl.color(2),label="debit Têt",alpha=0.4)
ax2.set_ylabel("Debit Têt")
ax2.legend(loc=2)

ax[1].plot(data_POEM_HF.datetime,data_POEM_HF.turbidity_ntu,c=tl.color(0),label="turbidity POEM")
ax[1].set_ylabel("turbidity POEM")
ax[1].legend(loc=0)
ax[1].grid(True)
ax2 = ax[1].twinx()
ax2.plot(data_SPM_TUR_CHL.time,data_SPM_TUR_CHL.tur,c=tl.color(1),marker="x",label="TUR sat")
ax2.set_ylabel("TUR sat")
ax2.legend(loc=3)
ax2 = ax[1].twinx()
ax2.plot(data_hydro.datetime,data_hydro.volume_l_s,c=tl.color(2),label="debit Têt",alpha=0.4)
ax2.set_ylabel("Debit Têt")
ax2.legend(loc=2)

fig.autofmt_xdate()
fig.tight_layout(pad=0)

fname = fm.build_path(os.getcwd(),"_build","raw","fluo_turbi_CHL_TUR")
fig.savefig(fname,bbox_inches='tight')
