import read_chemistry as chem
import read_ctd as ctd
import read_met as met
import read_composite as cmp
import tools as tl
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.dates as mdates
import numpy as np
from scipy.stats import shapiro


# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_POEM_HF_met = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021)

#data_POEM_HF = tl.timecut(data_POEM_HF, "2020-01-01", "2020-01-31")

# %%=============================================================================
# HISTO RAW
# =============================================================================

fig, ax = plt.subplots()
ax.plot(data_POEM_HF.datetime,data_POEM_HF.temperature_degc)

fig, ax = plt.subplots()
ax.hist(data_POEM_HF.temperature_degc,bins=100)

fig, ax = plt.subplots()
ax.boxplot(data_POEM_HF.temperature_degc.dropna())

# %%=============================================================================
# Test
# =============================================================================

temp = data_POEM_HF.temperature_degc
tempair = data_POEM_HF_met.airtemperature_degc
dt = data_POEM_HF.datetime

median = temp.rolling(12*24*30,center=True,min_periods=1).median()

temp2 = temp - median
std = temp2.std()

fig, ax = plt.subplots()
ax.plot(dt,temp)
ax.plot(dt,tempair)

fig, ax = plt.subplots()
ax.plot(dt,temp)
ax.plot(dt,median)

fig, ax = plt.subplots()
ax.plot(dt,temp2)
ax.plot(dt,np.zeros(len(dt))-3*std)
ax.plot(dt,np.zeros(len(dt))+3*std)

fig, ax = plt.subplots()
ax.hist(temp2,bins=100)

fig, ax = plt.subplots()
ax.boxplot(temp2.dropna())

