#https://www.meteociel.fr/cartes_obs/archives/14-12-2021/pression_eur2-14.png
#https://www.meteociel.fr/cartes_obs/radar/2021/t2109152000_so.png #for south west

import requests
import os
import tools as tl

current = os.getcwd()
folder = "rain_southwest"

for day in range(15,20):
    for hour in range(0,24):
        for minute in range(0,60,15):
            d = str(day).zfill(2)
            H = str(hour).zfill(2)
            M = str(minute).zfill(2)
            url = f"https://www.meteociel.fr/cartes_obs/radar/2021/t2109{d}{H}{M}_so.png"
            r  = requests.get(url)
            name = f'202109{d}{H}{M}_so.png'
            fullpath = os.path.join(current,folder,name)

            if not os.path.exists(os.path.join(current,folder)):
                os.mkdir(os.path.join(current,folder))
                print(f"mkdir {os.path.join(current,folder)}")

            if r.status_code == 200 :
                with open(fullpath,"ab") as f :
                        f.write(r.content) 

tl.warn()