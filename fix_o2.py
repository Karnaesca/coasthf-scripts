import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl


# =============================================================================
# LOAD DATA
# =============================================================================
keys = ["datetime","oxygen_mgl","mdates"]
poem_o2 = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)[keys]
sola_o2 = ctd.ctd(ctd.fname_SOLA_ctd_2021)[keys]

keys = ["datetime","o2_ml_L","mdates"]
poem_chem = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)[keys]

#%%
# =============================================================================
# CUT DATA
# =============================================================================
date0 = poem_o2.datetime.iloc[0]
date1 = poem_o2.datetime.iloc[1]
daten = poem_o2.datetime.iloc[-1]
freq = date1-date0

offset = pd.Timedelta(days=60)

sola_o2 = tl.timecut(sola_o2, date0, daten)
poem_chem = tl.timecut(poem_chem, date0-offset, daten+offset)

# =============================================================================
# CONVERT DATA
# =============================================================================
# conversion
dO2 = 1.42763 #kg·m-3, mg/mL
poem_chem["oxygen_mgl"] = poem_chem["o2_ml_L"]*dO2

#%%
# =============================================================================
# INTERPOLATE DATA
# =============================================================================
dt_index = pd.DatetimeIndex(poem_chem.datetime).floor("D")
poem_chem_interp = pd.DataFrame(index=dt_index)
poem_chem_interp["oxygen_mgl"] = poem_chem["oxygen_mgl"].values
poem_chem_interp = poem_chem_interp.resample("5min").interpolate(method="linear")

dates = pd.date_range(date0,daten,freq="5min")
tmp = pd.DataFrame()
tmp["datetime"] = dates
poem_chem_interp = tmp.merge(poem_chem_interp,on="datetime",how="left")
poem_chem_interp["mdates"] = [mdates.date2num(e) for e in poem_chem_interp.datetime]

dates = pd.date_range(date0,daten,freq="5min")
tmp = pd.DataFrame()
tmp["datetime"] = dates
poem_o2 = tmp.merge(poem_o2,on="datetime",how="left")
poem_o2["mdates"] = [mdates.date2num(e) for e in poem_chem_interp.datetime]

#%%
# =============================================================================
# CALCULATE OFFSET
# =============================================================================
b = 12*24*60
t_offset = poem_o2.oxygen_mgl.interpolate(method="ffill")-poem_chem_interp.oxygen_mgl
dfoffset = pd.DataFrame()
dfoffset["datetime"] = dates
dfoffset["offset"] = t_offset

# =============================================================================
# CALIBRATION
# =============================================================================

bins=[
      ['2019-12-31T06:50', '2020-01-16T18:05'],
      ['2020-01-23T13:04', '2020-06-03T10:18'],
      ["2020-06-20","2020-08-15"],
      ["2020-10-24","2020-11-23"],
      ["2021-02-03","2021-04-22"],
      ["2021-06-08","2021-12-31"]
      ]

calibs = []
for d0,d1 in bins:
    sample = tl.timecut(dfoffset, d0, d1)
    m = sample.mean().values[0]
    calibs.append(m)
    
poem_o2_calib = poem_o2.copy()
#poem_o2_calib.oxygen_mgl = np.nan

select = poem_o2.copy()
select.oxygen_mgl = False

displayoffset = pd.DataFrame()
displayoffset["datetime"] = dates
displayoffset["offset"] = np.zeros(len(dates))
displayoffset["mdates"] = [mdates.date2num(e) for e in displayoffset.datetime]

i = 0
for d0,d1 in bins:
    cut = (poem_o2.datetime>=pd.to_datetime(d0)) & (poem_o2.datetime<=pd.to_datetime(d1))
    select.oxygen_mgl[cut] = True
    poem_o2_calib.oxygen_mgl[cut] = poem_o2.oxygen_mgl[cut]-calibs[i]
    displayoffset.offset[cut] = calibs[i]
    i += 1

# =============================================================================
# SAVE CALIB
# =============================================================================
root = os.getcwd()
fname = fm.build_path(root,"data","recalibrated","POEM_o2_recalibrated",ext=".csv")
poem_o2_calib.oxygen_mgl = poem_o2_calib.oxygen_mgl.round(1)
poem_o2_calib[["datetime","oxygen_mgl"]].to_csv(fname,index=False)

# =============================================================================
# SAVE SELECT
# =============================================================================
root = os.getcwd()
fname = fm.build_path(root,"_build","manual_filter","POEM_o2_fixed",ext=".csv")
select[["oxygen_mgl"]].to_csv(fname,index=False)

#%%
# =============================================================================
# GRAPH
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_paysage

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

ax.plot(poem_o2.mdates,poem_o2.oxygen_mgl,label="oxygen_mgl HF poem")
#ax.plot(sola_o2.mdates,sola_o2.oxygen_mgl,label="oxygen_mgl sola")
ax.plot(poem_chem_interp.mdates,poem_chem_interp.oxygen_mgl,label="oxygen_mgl BF chem.")

#ax.plot(poem_chem_interp.mdates,t_offset,label="oxygen_mgl sola")

ax.plot(poem_o2_calib.mdates,poem_o2_calib.oxygen_mgl,label="oxygen_mgl poem HF calibrated")

ax.set_ylabel("oxygen mg/l")

ax2 = ax.twinx()

ax2.plot(dfoffset.datetime,dfoffset.offset,label="raw offset",c="tab:purple")
ax2.plot(displayoffset.mdates,-displayoffset.offset,label="offset applied",c="tab:red")
ax2.legend()

ax2.set_ylabel("offset")

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.xaxis.set_major_locator(mdates.MonthLocator())
#ax.xaxis.set_minor_locator(mdates.DayLocator())
ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)

fname = fm.build_path(os.getcwd(),"_build","calibration","calib_O2_POEM")
fig.savefig(fname,bbox_inches='tight')
