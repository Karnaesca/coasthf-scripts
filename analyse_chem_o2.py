import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import read_chemistry
import read_profil_ctd
import read_ctd
import file_manager as fm
from datetime import timedelta

import read_composite as cmp

import tools as tl

# function
def med_1h(data,param,datetime):
    dt_min = datetime-timedelta(minutes=30)
    dt_max = datetime+timedelta(minutes=30)
    res = data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)].median()
    tmp =  data[param][(data.datetime<=dt_max) & (data.datetime>=dt_min)]
    print(tmp)
    return res

# o2_ml_L ChlA_ug_L MES_mg_L
data_chem = read_chemistry.chem_3m(read_chemistry.fname_CHIFRE_POEM_chem_2017_2021)
# fluorescence_rfu turbidity_ntu
data_profil_ctd  = read_profil_ctd.manual_ctd_2m(read_profil_ctd.fname_CHIFRE_POEM_ctd_2017_2021)
# fluorescence_rfu turbidity_ntu oxygen_mgl
data_ctd0 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2020)
data_ctd1 = read_ctd.ctd(read_ctd.fname_POEM_ctd_2021)
data_ctd = pd.concat([data_ctd0,data_ctd1],sort=False)

SOLA = cmp.cmp(cmp.fname_data_SOLA_1d_med)
SOLA = tl.timecut(SOLA,"2020-01-01","2021-12-31")

# cut profil and chemistry
data_profil_ctd = data_profil_ctd[(data_profil_ctd.datetime >= data_ctd.datetime.min()) & (data_profil_ctd.datetime <= data_ctd.datetime.max())]
data_chem = data_chem[(data_chem.datetime >= data_ctd.datetime.min()) & (data_chem.datetime <= data_ctd.datetime.max())]

# conversion
dO2 = 1.42763 #kg·m-3, mg/mL
data_chem["oxygen_mgl"] = data_chem["o2_ml_L"]*dO2

# sample data_ctd with datetime from data_chem
df_ctd_med1h = pd.DataFrame()
df_ctd_med1h["datetime"] = data_chem["datetime"]
df_ctd_med1h["mdates"] = data_chem["mdates"]
df_ctd_med1h["fluorescence_rfu"] = [med_1h(data_ctd,"fluorescence_rfu",dt) for dt in df_ctd_med1h["datetime"]]
df_ctd_med1h["oxygen_mgl"] = [med_1h(data_ctd,"oxygen_mgl",dt) for dt in df_ctd_med1h["datetime"]]





# =============================================================================
# graph oxygen
# =============================================================================
fig, ax = plt.subplots()
fig.set_size_inches(32*(25/64),24*(25/64))

ax.plot(SOLA.mdates,SOLA.oxygen_mgl,label="")
ax.plot(data_ctd.mdates,data_ctd.oxygen_mgl,label="ctd HF")
ax.scatter(df_ctd_med1h.mdates,df_ctd_med1h.oxygen_mgl,label="downsampled ctd HF",c="tab:red",marker="x",zorder=10)
ax.set_ylabel("oxygen_mgl")
ax.grid(True)
ax.plot(data_chem.mdates,data_chem.oxygen_mgl,label="chem BF",marker="x")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.legend()
    
fig.autofmt_xdate()
title = "graph oxygen chemistry and ctd HF 2020 2021"
fig.suptitle(title)
#fig.savefig(fm.to_img_fname(title))



