import read_ctd
import read_profil_ctd
import grapher
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta

import read_profil_ctd
import read_ctd

data_ctd = read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
data_manual_ctd = read_profil_ctd.manual_ctd(read_profil_ctd.fname_profil_ctd)

dtmin = data_ctd["datetime"].min()
dtmax = data_ctd["datetime"].max()
data_manual_ctd = data_manual_ctd[(data_manual_ctd.datetime>=dtmin) & (data_manual_ctd.datetime<=dtmax)]

data_manual_ctd.set_index("depth_m",inplace=True)
data_manual_ctd.groupby("datetime")["temperature_degc"].plot(legend=True)