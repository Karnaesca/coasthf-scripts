import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import read_mar as mar
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait

#%% ===========================================================================
# LOAD DATA
# =============================================================================

data_POEM_met_HF = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021)

start = data_POEM_met_HF.datetime.iloc[0].strftime("%Y-%m-%d")
end = data_POEM_met_HF.datetime.iloc[-1].strftime("%Y-%m-%d")

data_SOLA_met_HF = met.mets(met.fname_SOLA_met_2021)

data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

# data_mar_PV = mar.mars(mar.fname_maree_PV_2020,mar.fname_maree_PV_2021)

# data_mar_PN = mar.mars(mar.fname_maree_PN_2020,mar.fname_maree_PN_2021)

#%% ===========================================================================
# 
# =============================================================================

data_POEM_met_HF_med1d = data_POEM_met_HF.resample("1D", on="datetime").mean()
data_POEM_met_HF_med1d.reset_index(inplace=True)
data_POEM_met_HF_med1d = tl.addmdates(data_POEM_met_HF_med1d)


data_SOLA_met_HF_med1d = data_SOLA_met_HF.resample("1D", on="datetime").mean()
data_SOLA_met_HF_med1d.reset_index(inplace=True)
data_SOLA_met_HF_med1d = tl.addmdates(data_SOLA_met_HF_med1d)

#%% ===========================================================================
# GRAPH TIME MET
# =============================================================================

datetime_col_name="datetime"
keys = [key for key in list(data_POEM_met_HF.keys()) if ("mdates" not in key) and (datetime_col_name not in key)]
    
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    # HF data
    axes[i].plot(data_POEM_met_HF.mdates,data_POEM_met_HF[key],label="POEM HF",c=tl.color(0))
    axes[i].plot(data_POEM_met_HF_med1d.mdates,data_POEM_met_HF_med1d[key],label="POEM HF mean1d",c=tl.color(1))
    axes[i].plot(data_SOLA_met_HF.mdates,data_SOLA_met_HF[key],label="SOLA HF",c=tl.color(2),alpha=0.5)
    axes[i].plot(data_SOLA_met_HF_med1d.mdates,data_SOLA_met_HF_med1d[key],label="SOLA HF mean1d",c=tl.color(3),alpha=0.5)
    # debit Têt
    ax2 = axes[i].twinx()
    ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="Têt $(L/s)$",c=tl.color(4),alpha=0.5)

    axes[i].set_ylabel(tl.col2name(key))
    axes[i].grid(True)
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes[i].xaxis.set_major_locator(mdates.MonthLocator())
    axes[i].legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)