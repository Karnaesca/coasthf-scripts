import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

# =============================================================================
# LOAD RAW DATA
# =============================================================================

data_POEM = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

keys = [key for key in list(data_POEM.keys()) if ("mdates" not in key)]

# =============================================================================
# LOAD FIXED DATA
# =============================================================================

# 'fname_POEM_o2_recalibrated',
# 'fname_POEM_turbi_fix',
# 'fname_POEM_fluo_fix'

oxy = cmp.cmp(cmp.fname_POEM_o2_recalibrated)
tur = cmp.cmp(cmp.fname_POEM_turbi_fix)
flu = cmp.cmp(cmp.fname_POEM_fluo_fix)

# =============================================================================
# LOAD FILTER
# =============================================================================

# 'fname_good',
# 'fname_passed'

good = cmp.cmp(cmp.fname_good)
passed = cmp.cmp(cmp.fname_passed)

# =============================================================================
# QC
# =============================================================================

# 0    No quality control (QC) was performed.
# 1    QC was performed: good data
# 2    QC was performed: probably good data
# 3    QC was performed: probably bad data
# 4    QC was performed: bad data
# 5    The value was changed as a result of QC
# 7    Nominal value
# 8    Interpolated value
# 9   The value is missing

filter2qc = {
    0 : 9, # nan
    1 : 4, # static
    2 : 4, # slope
    3 : 4, # rolling
    4 : 3, # manual
    5 : 1  # good
    }

modif_cols = { col : f"qc_{col}" for col in keys if not col == "datetime" }

qc = passed.replace(filter2qc)
qc.rename(columns=modif_cols,inplace=True)

# =============================================================================
# BUILD FILE
# =============================================================================

rename = {
    "datetime" : "utc_datetime"
    }

data_qc = data_POEM[keys].merge(qc,on="datetime",how="outer")
#data_qc.reset_index(inplace=True)
data_qc.rename(columns=rename,inplace=True)
data_qc.utc_datetime = data_qc.utc_datetime.dt.strftime("%Y-%m-%dT%H:%M:%SZ")

# replace modif values

data_qc.oxygen_mgl = oxy.oxygen_mgl
data_qc.fluorescence_rfu = flu.fluorescence_rfu
data_qc.turbidity_ntu = tur.turbidity_ntu

root = os.getcwd()
fname = fm.build_path(root,"data","validator","data_qc",ext=".csv")
data_qc.to_csv(fname,index=False)

tl.warn()

# =============================================================================
# BUILD FILE WITH ONLY GOOD
# =============================================================================

keys = [key for key in keys if (not "datetime" in key)]
columns = ["datetime"]+keys
data_good = pd.DataFrame(index = data_qc.index, columns = columns)
data_good["datetime"] = data_POEM.datetime
for key in keys :
        data_good[key] = data_qc[key].where(data_qc[f"qc_{key}"] == 1)
        
root = os.getcwd()
fname = fm.build_path(root,"data","validator", "POEM_ctd_2020_2021_good", ext=".csv")
data_good.to_csv(fname, index=False)