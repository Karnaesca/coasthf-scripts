import read_chemistry as chem
import read_ctd as ctd
import read_composite as cmp
import tools as tl
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.dates as mdates
import numpy as np

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

data_POEM_BF = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)
timeframe = data_POEM_HF.datetime.iloc[[0,-1]].values
data_POEM_BF = tl.timecut(data_POEM_BF, *timeframe)

data_POEM_clean = cmp.cmp("/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/demo_visu/data_qc.csv",dtcol="utc_datetime")
data_POEM_clean.rename(columns={"utc_datetime":"datetime"},inplace=True)
data_POEM_clean.datetime = data_POEM_clean.datetime.dt.tz_convert(None)

keys = list(data_POEM_HF.keys())
keys.remove("datetime")
keys.remove("mdates")

# =============================================================================
# SOME FUNCTIONS
# =============================================================================

qc2color = {
    9 : "tab:grey",
    1 : "tab:green",
    2 : "tab:yellow",
    3 : "tab:orange",
    4  :"tab:red"
    }

def qc2colors(ilist):
    return [qc2color[e] for e in ilist]

cmap = matplotlib.cm.plasma

# %%=============================================================================
# PROCESS 1
# =============================================================================

# intervals = pd.DataFrame()
# intervals["start"] = data_POEM_BF.datetime.shift()
# intervals["end"] = data_POEM_BF.datetime
# intervals.dropna(inplace=True)

# fig, ax = plt.subplots(len(keys),sharex=True)

# for i in range(len(intervals)):
#     tmp_timeframe = intervals.iloc[i].values
#     tmp_data = tl.timecut(data_POEM_HF, *tmp_timeframe)
#     #tmp_data.datetime = tmp_data.datetime - tmp_data.datetime.iloc[0]
#     tmp_data.mdates = tmp_data.mdates - tmp_data.mdates.iloc[0]
#     tmp_data[keys] = tmp_data[keys] - tmp_data[keys].iloc[0]
    
#     date = pd.to_datetime(tmp_timeframe[0]).strftime("%Y-%m-%d")
    
#     for j,key in enumerate(keys):
#         if j == 0:
#             ax[j].scatter(tmp_data.mdates,tmp_data[key], label=date, c = cmap(float(i/len(intervals))),marker=".", s=0.5,alpha=0.5)
#         else :
#             ax[j].scatter(tmp_data.mdates,tmp_data[key], c = cmap(float(i/len(intervals))), marker=".", s=0.5,alpha=0.5)
#         ax[j].set_ylabel(key)
#         ax[j].set_xlabel("nb de jours")
#         ax[j].grid(True)
        
# fig.legend()
# fig.autofmt_xdate()
# fig.tight_layout(pad=0)
# fig.subplots_adjust(wspace=0, hspace=0)

# %%=============================================================================
# PROCESS 2
# =============================================================================

# intervals = pd.DataFrame()
# intervals["start"] = data_POEM_BF.datetime.shift()
# intervals["end"] = data_POEM_BF.datetime
# intervals.dropna(inplace=True)

# l = len(keys)*len(intervals)
# index = 0

# fig, ax = plt.subplots(l,sharex=True)

# for i in range(len(intervals)):
#     tmp_timeframe = intervals.iloc[i].values
#     tmp_data = tl.timecut(data_POEM_clean, *tmp_timeframe)
#     #tmp_data.datetime = tmp_data.datetime - tmp_data.datetime.iloc[0]
#     tmp_data.mdates = tmp_data.mdates - tmp_data.mdates.iloc[0]
#     tmp_data[keys] = tmp_data[keys] - tmp_data[keys].iloc[0]
    
#     date = pd.to_datetime(tmp_timeframe[0]).strftime("%Y-%m-%d")
    
#     for j,key in enumerate(keys):
#         qc = f"qc_{key}"
#         if j == 0:
#             ax[index].scatter(tmp_data.mdates,tmp_data[key], label=date, c = qc2colors(tmp_data[qc]),marker=".", s=0.5,alpha=0.5)
#         else :
#             ax[index].scatter(tmp_data.mdates,tmp_data[key], c = qc2colors(tmp_data[qc]), marker=".", s=0.5,alpha=0.5)
#         #ax[index].set_ylabel(key)
#         ax[index].set_xlabel("nb de jours")
#         #ax[index].grid(True)
#         index += 1
        
# #fig.legend()
# fig.autofmt_xdate()
# fig.tight_layout(pad=0)
# fig.subplots_adjust(wspace=0, hspace=0)

# %%=============================================================================
# PROCESS 3
# =============================================================================

# cuts = data_POEM_BF.datetime

# fig, ax = plt.subplots(len(keys),sharex=True)

# for i,key in enumerate(keys):
#     qc = f"qc_{key}"
#     if i == 0:
#         ax[i].scatter(data_POEM_clean.mdates,data_POEM_clean[key], label=date, c = qc2colors(data_POEM_clean[qc]),marker=".", s=0.5,alpha=0.5)
#     else :
#         ax[i].scatter(data_POEM_clean.mdates,data_POEM_clean[key], c = qc2colors(data_POEM_clean[qc]), marker=".", s=0.5,alpha=0.5)
#     ax[i].set_ylabel(key)
#     ax[i].set_xlabel("nb de jours")
#     ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
#     ax[i].set_xticks(cuts)
#     ax[i].grid(True)
        
# fig.legend()
# fig.autofmt_xdate()
# fig.tight_layout(pad=0)
# fig.subplots_adjust(wspace=0, hspace=0)

# %%=============================================================================
# PROCESS 4 - SOMME DES MAUVAISE VALEURS
# =============================================================================

# intervals = pd.DataFrame()
# intervals["start"] = data_POEM_BF.datetime.shift()
# intervals["end"] = data_POEM_BF.datetime
# intervals["dt"] = intervals["end"] - intervals["start"]
# intervals["dt"] = [ e.days for e in intervals.dt]
# intervals.dropna(inplace=True)
# intervals =  intervales[intervales.dt>26]

# dt = data_POEM_clean.mdates - data_POEM_clean.mdates.iloc[0]
# df = pd.DataFrame(0,index=data_POEM_clean.index,columns=keys)
# df["dt"] = dt

# for i in range(len(intervals)):
#     tmp_timeframe = intervals.iloc[i].values
#     tmp_data = tl.timecut(data_POEM_clean, *tmp_timeframe)
#     tmp_data.mdates = tmp_data.mdates - tmp_data.mdates.iloc[0]
#     tmp_data[keys] = tmp_data[keys] - tmp_data[keys].iloc[0]
#     date = pd.to_datetime(tmp_timeframe[0]).strftime("%Y-%m-%d")
    
#     for j,key in enumerate(keys):
#         qc = f"qc_{key}"
#         bad = (~(tmp_data[qc]==1))
#         pivot = len(bad)-1
#         df.loc[:pivot,(key)] =  df.loc[:pivot,(key)] + bad.astype(int).values
#         df.loc[pivot:,(key)] = np.NaN
        
# fig, ax = plt.subplots()
# ax.plot(df.dt,df[keys])

# %%=============================================================================
# PROCESS 5 - SOMME DES MAUVAISE VALEURS
# =============================================================================

intervals = pd.DataFrame()
intervals["start"] = data_POEM_BF.datetime.shift()
intervals["end"] = data_POEM_BF.datetime
intervals.dropna(inplace=True)

qc_keys = list(data_POEM_clean.keys())
qc_keys = [k for k in qc_keys if "qc" in k]

qc_codes = np.array([1,2,3,4,9])

store = {}

bins = np.arange(0,140,1)

for i in range(len(intervals)):
    tmp_timeframe = intervals.iloc[i].values
    tmp_data = tl.timecut(data_POEM_clean, *tmp_timeframe)
    tmp_data.reset_index(drop=True,inplace=True)
    tmp_data.mdates = tmp_data.mdates - tmp_data.mdates.iloc[0]
    
    for k in qc_keys:
        for qc_code in qc_codes:
            fil = (tmp_data[k]==qc_code).values
            if (k,qc_code) in store.keys():
                store[(k,qc_code)] = np.append(store[(k,qc_code)],tmp_data[fil].mdates.values)
            else :
                store[(k,qc_code)] = tmp_data[fil].mdates.values
            
for k in qc_keys:
    fig, ax = plt.subplots()
    offset = np.zeros(shape=bins.shape[0]-1)
    for qc_code in qc_codes:
        if not store[(k,qc_code)].shape[0] == 0 :
            # not empty
            tmp = ax.hist(store[(k,qc_code)],bins=bins,bottom=offset,color=qc2color[qc_code])
            offset += tmp[0]
        
    fig.suptitle(k)
            