import read_ctd
import grapher
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os

data_ctd=read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)

#plt.plot(data_ctd.index,data_ctd.temperature_degc)

data = data_ctd.temperature_degc.dropna().values
N=len(data)
Y=np.abs(np.fft.rfft(data,N)/N)
plt.plot(Y)