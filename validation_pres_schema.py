import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0,4*np.pi,4*8+1)

y = np.sin(x)

plt.plot(x,y)
plt.xlabel("x")
plt.ylabel("y")
