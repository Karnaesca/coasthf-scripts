import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm
from matplotlib.patches import Rectangle

import pybaselines 


# =============================================================================
# LOAD DATA
# =============================================================================

data = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)[["datetime","fluorescence_rfu"]]

#%% =============================================================================
# SELECT THE ZONE TO FIX
# =============================================================================

# to detrend
cuts = [None]*3
cuts[0] = ['2020-07-25T08:49', '2020-10-06T08:04']
cuts[1] = ['2020-11-02T11:40', '2020-11-24T13:24']
cuts[2] = ['2021-07-14T01:26', '2021-08-03T12:17']

# =============================================================================
# APPLY FIX
# =============================================================================

data_fixed = data.copy()

data_fixed.dropna(inplace=True)

for cut in cuts:
    chunk = data_fixed[(data_fixed.datetime >= cut[0]) & (data_fixed.datetime <= cut[1])]
    baseline = pybaselines.polynomial.modpoly(chunk.fluorescence_rfu, chunk.index, poly_order=2)[0]
    fix_part = chunk.fluorescence_rfu - baseline
    fix_part = fix_part.round(decimals=1)
    fix_part.where(fix_part>0,0,inplace=True)
    data_fixed.fluorescence_rfu[(data_fixed.datetime >= cut[0]) & (data_fixed.datetime <= cut[1]) ] = fix_part
    
data_nan = data[data.fluorescence_rfu.isnull()]
data_res = pd.concat([data_nan,data_fixed])
data_res.sort_values(by="datetime", inplace=True)

# =============================================================================
# SAVE MODIF
# =============================================================================
root = os.getcwd()
fname = fm.build_path(root,"data","recalibrated","POEM_fluo_fix",ext=".csv")
data_res.to_csv(fname,index=False)

# =============================================================================
# GRAPH
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(data.datetime,data.fluorescence_rfu,label="raw")
#ax.plot(data_fixed.datetime,data_fixed.fluorescence_rfu,label="fixed")
# ax.plot(data_res.datetime,data_res.fluorescence_rfu,label="more",alpha=0.5)
ax.set_ylabel(tl.col2name("fluorescence_rfu"))

ax.grid(True)
# ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
# ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)