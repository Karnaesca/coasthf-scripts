import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait

# CHEM
# o2_ml_L ChlA_ug_L MES_mg_L

# PCTD
# fluorescence_rfu turbidity_ntu

# CTD
# fluorescence_rfu turbidity_ntu oxygen_mgl

# =============================================================================
# LOAD DATA
# =============================================================================
data_POEM = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
data_SOLA = ctd.ctds(ctd.fname_SOLA_ctd_2021)

start = "2020-01-01"
end = "2021-12-31"

data_chem = chem.chem_3m(chem.fname_CHIFRE_POEM_chem_2017_2021)
data_chem = tl.timecut(data_chem, start, end)

data_pctd = pctd.manual_ctd_2m(pctd.fname_CHIFRE_POEM_ctd_2017_2021)
data_pctd = tl.timecut(data_pctd, start, end)

data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

#%% ===========================================================================
# GRAPH
# =============================================================================
   
fig, ax = plt.subplots()
fig.set_size_inches(*A4_paysage)

# HF
ax.plot(data_POEM.mdates,data_POEM.turbidity_ntu,label="turbidity_ntu POEM HF",c=tl.color(0))
ax.plot(data_SOLA.mdates,data_SOLA.turbidity_ntu,label="turbidity_ntu SOLA HF",c=tl.color(1),alpha=0.5)
ax.plot(data_pctd.mdates,data_pctd.turbidity_ntu,label="turbidity_ntu POEM BF",c=tl.color(2),marker="x")
ax.set_ylabel("turbidity_ntu")

# BF MES
ax2 = ax.twinx()
ax2.plot(data_chem.mdates,data_chem.MES_mg_L,label="MES_mg_L",c=tl.color(3),alpha=0.5,marker="x")
ax.set_ylabel("MES_mg_L")
ax2.legend()

# debit l/s
ax2 = ax.twinx()
ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="volume_l_s",c=tl.color(4),alpha=0.5)
ax.set_ylabel("volume_l_s")
ax2.legend()


ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.xaxis.set_minor_locator(mdates.DayLocator())
ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)

fname = fm.build_path(os.getcwd(),"_build","raw","full_graph_POEM")
fig.savefig(fname,bbox_inches='tight')