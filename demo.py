import pandas as pd

date = ["2020-01-01","2020-01-02","2020-01-03"]
heure = ["08","09","10"]
hs = [2,3,4]
ho = [1,2,3]

data = pd.DataFrame()

data["date"] = date
data["heure"] = heure
data["hs"] = hs
data["ho"] = ho

####

data["datetime"] = pd.to_datetime(data.date+" "+data.heure.astype(str),format="%Y-%m-%d %H")

moyenne_2D = data.resample("2D",on="datetime").mean()