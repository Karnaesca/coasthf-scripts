import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm
from matplotlib.patches import Rectangle

# =============================================================================
# SOME FUNCTIONS
# =============================================================================

def rect(md,fluo,n=0):
    r = Rectangle((md.min(),fluo.min()), md.max()-md.min(), fluo.max()-fluo.min(),facecolor=tl.color(n),zorder=20,alpha=0.2,label="zoom")
    return r

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
start = data_POEM_HF.datetime.iloc[0].strftime("%Y-%m-%d")
end = data_POEM_HF.datetime.iloc[-1].strftime("%Y-%m-%d")
data_hydro = hydro.HydroSeries(hydro.station_Tet_Perpignan,start=start,end=end).data

# %%=============================================================================
# CUT
# =============================================================================
cuts = [
    ["2020-01-25","2020-02-05"],
    ["2020-03-20","2020-04-05"],
    ["2020-04-15","2020-04-27"],
    ["2020-04-26","2020-05-10"],
    ["2020-09-24","2020-10-07"],
    ["2020-11-11","2020-11-24"],
    ["2021-02-27","2021-03-10"],
    ["2021-03-25","2021-04-10"],
    ["2021-05-25","2021-06-07"],
    ["2021-07-20","2021-08-05"]
    ]

cuts = cuts[-2:]

# =============================================================================
# GRAPH BIG
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(data_POEM_HF.mdates,data_POEM_HF.fluorescence_rfu,label=tl.col2name("fluorescence_rfu"))
# ax2 = ax.twinx()
# ax2.plot(data_hydro.mdates,data_hydro.volume_l_s,label="debit Têt moyen journalier (l/s)",zorder=-20,c="tab:orange")
# ax2.set_ylabel("débit (l/s)")
ax.set_ylabel(tl.col2name("fluorescence_rfu"))
ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.legend()

for i,cut in enumerate(cuts):
    data_POEM_HF_cut = tl.timecut(data_POEM_HF, *cut)
    md = data_POEM_HF_cut.mdates
    fluo = data_POEM_HF.fluorescence_rfu
    ax.add_patch(rect(md,fluo,i+1))

fig.autofmt_xdate()
fig.tight_layout(pad=0)
#fname = fm.build_path(os.getcwd(),"_build","raw","full_fluo")
#fig.savefig(fname,bbox_inches='tight')

# %%=============================================================================
# ALL SMALL GRAPHS
# =============================================================================
for i,cut in enumerate(cuts):

    fig, ax = plt.subplots()
    fig.set_size_inches(21*cm,7*cm)
    
    data_POEM_HF_cut = tl.timecut(data_POEM_HF, *cut)
    
    ax.plot(data_POEM_HF_cut.mdates,data_POEM_HF_cut.fluorescence_rfu,label=tl.col2name("fluorescence_rfu"))
    ax.set_ylabel(tl.col2name("fluorescence_rfu"))
    ax.grid(True)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    ax.xaxis.set_major_locator(mdates.DayLocator())
    ax.xaxis.set_minor_locator(mdates.HourLocator([6,12,18]))
    ax.legend()
    ax.add_patch(rect(data_POEM_HF_cut.mdates,data_POEM_HF_cut.fluorescence_rfu,i+1))
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    #fname = fm.build_path(os.getcwd(),"_build","raw",f"full_fluo_part_{i}")
    #fig.savefig(fname,bbox_inches='tight')
    