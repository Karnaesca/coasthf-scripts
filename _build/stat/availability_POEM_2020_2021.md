|                          |   available_data |   total_data |   percent_available_data |
|:-------------------------|-----------------:|-------------:|-------------------------:|
| atmosphericpressure_mbar |           196619 |       210528 |                  93.3933 |
| airtemperature_degc      |           197338 |       210528 |                  93.7348 |
| windspeed_kn             |           197338 |       210528 |                  93.7348 |
| windirection_deg         |           197338 |       210528 |                  93.7348 |
| temperature_degc         |           171988 |       210528 |                  81.6936 |
| salinity                 |           171838 |       210528 |                  81.6224 |
| fluorescence_rfu         |           170324 |       210528 |                  80.9033 |
| turbidity_ntu            |           155503 |       210528 |                  73.8633 |
| oxygen_mgl               |           155865 |       210528 |                  74.0353 |