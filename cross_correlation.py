import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm

# =============================================================================
# LOAD DATA
# =============================================================================
station = hydro.station_Tet_Perpignan
start = "2020-01-01"
end = "2020-01-01"
data_POEM = cmp.cmp(cmp.fname_data_POEM_1d_med)
hydro_Tet = hydro.HydroSeries(station=station,start=start,end=end).data

keys = list(data_POEM.keys())
keys = [key for key in keys if ("datetime" not in key) and ("mdates" not in key)]

# =============================================================================
# CUT DATA
# =============================================================================
data_POEM = data_POEM[data_POEM.datetime<"2020-03-01"]
hydro_Tet = hydro_Tet[hydro_Tet.datetime<"2020-03-01"]

# =============================================================================
# Normalization
# =============================================================================



turbidity = data_POEM.turbidity_ntu.values
salinity = data_POEM.salinity.values
debit = hydro_Tet.volume_l_s.values
time = data_POEM.mdates
time2 = hydro_Tet.mdates

xcorr = np.correlate(normalize(turbidity),normalize(debit),"same")

start = data_POEM.datetime.iloc[0]
periods = len(xcorr)
freq = "1D"
datetime = pd.date_range(start=start,periods=periods,freq=freq)
t = np.arange(-(periods/2),periods/2,1)

# =============================================================================
# Normal plot
# =============================================================================

cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm
SIZE = A4_portait

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

ax.plot(time,normalize(turbidity),label="turbidity_ntu")
ax.plot(time,normalize(salinity),label="salinity")
ax.plot(time2,normalize(debit),label="debit")

ax.set_ylabel("")
ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.xaxis.set_minor_locator(mdates.DayLocator())
ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)
# fname = fm.build_path(os.getcwd(),"_build","raw","correlation_POEM_BF_raw")

# fig.savefig(fname,bbox_inches='tight')

# =============================================================================
# Cross corrlation
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(*SIZE)

ax.plot(t,xcorr)
ax.set_ylabel("")
ax.grid(True)
#ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
#ax.xaxis.set_major_locator(mdates.MonthLocator())
#ax.xaxis.set_minor_locator(mdates.DayLocator())
#fig.autofmt_xdate()
fig.tight_layout(pad=0)

# fname = fm.build_path(os.getcwd(),"_build","raw","correlation_POEM_BF_raw")
# fig.savefig(fname,bbox_inches='tight')