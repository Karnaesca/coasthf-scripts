import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#Programme CHIFFRE,,,,,,,,,
#Bouée POEME,,,,,,,,,
#,,,,,,,,,
#Id Sortie,Date ,Commentaire,Etat de la mer,Heure (UTC),Profondeur,Dissous,Particulaire,,
#,,,,,,O2 dissous (ml/L),Chlorophylle a (µg/L),Phéopigment (µg/L),MES (mg/L)
#POEM20170915,15/09/2017,,"Faible houle, ciel couvert & pluie",08:09,3m,5.037,0.296,0.051,0.875
#,,,,,24m,5.053,0.945,0.104,1.655

fname_CHIFRE_POEM_chem_2017_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/CHIFRE_2017_2021/CHIFRE_POEM_chem_2017_2021.csv"

def chem(fname):
    print(f"Read Chemistry\n{fname}")
    data = pd.read_table(fname,skiprows=5,
                            decimal='.',
                            sep = ",",
                            usecols=[1,4,5,6,7,8,9],
                            names=["date","time","depth_m","o2_ml_L","ChlA_ug_L","pigment_ug_L","MES_mg_L"]
                            )
    
    #fix the data
    data[["date","time"]] = data[["date","time"]].ffill()
    data.depth_m = data.depth_m.str.extract(r'(\d+)m').astype(float)
    
    #parse date
    data.loc[:,"datetime"] = pd.to_datetime(data.date+"\t"+data.time,format="%d/%m/%Y\t%H:%M")
    data.drop(columns=["date","time"],inplace=True)
    data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

def chem_3m(fname):
    data = chem(fname)
    data = data[data.depth_m==3]
    data.drop(columns=["depth_m"],inplace=True)
    return data
