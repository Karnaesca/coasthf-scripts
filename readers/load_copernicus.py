#https://resources.marine.copernicus.eu/product-detail/OCEANCOLOUR_MED_BGC_HR_L4_NRT_009_211/DATA-ACCESS
#https://help.marine.copernicus.eu/en/articles/5182598-how-to-consume-the-opendap-api-and-cas-sso-using-python
#https://nrt.cmems-du.eu/thredds/dodsC/cmems_obs_oc_med_bgc_tur-spm-chl_nrt_l4-hr-mosaic_P1D-m.html

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.colors import Normalize
from matplotlib.widgets import RectangleSelector
import matplotlib
import matplotlib.dates as mdates
import xarray as xr
from pydap.client import open_url
from pydap.cas.get_cookies import setup_session
import os
import tools as tl
import file_manager as fm
from size import A4_paysage, A4_portait
from matplotlib.patches import Rectangle
import read_geojson

username = "tvala"
password = "OEBZdZCpImwgiaIQsP0A"

dataset_cmems_obs_oc_med_bgc_tur_spm_chl_nrt_l4_hr_mosaic_P1D_m = "cmems_obs_oc_med_bgc_tur-spm-chl_nrt_l4-hr-mosaic_P1D-m"
dataset_med_cmcc_ssh_rean_d = "med-cmcc-ssh-rean-d"

class SatDataSet:
    def __init__(self,dataset = "cmems_obs_oc_med_bgc_tur-spm-chl_nrt_l4-hr-mosaic_P1D-m"):
        self.username = "tvala"
        self.password = "OEBZdZCpImwgiaIQsP0A"
        self.dataset = dataset
        self.root = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab"

        data_store = copernicusmarine_datastore(self.dataset, self.username, self.password)
        self.ds = xr.open_dataset(data_store)

    def getparams(self):
        params = list(self.ds.keys())
        return params
    
    def getgeobounds(self):
        return [float(self.ds["lat"][0]),float(self.ds["lon"][-1]),float(self.ds["lat"][-1]),float(self.ds["lon"][0])]
    
    def gettimebounds(self):
        return [pd.to_datetime(self.ds["time"][0].values),pd.to_datetime(self.ds["time"][-1].values)]
    
    def isparam(self,param):
        return param in self.getparams()
    
    def iszone_inbounds(self,zone):
        # n,   e,   s,   w
        # lat, lon, lat, lon
        return self.islat_inbounds(zone[0]) and self.islon_inbounds(zone[1]) and self.islat_inbounds(zone[2]) and self.islon_inbounds(zone[3])
    
    def istimeframe_inbounds(self,timeframe):
        return self.ist_inbounds(timeframe[0]) and self.ist_inbounds(timeframe[1])
    
    def islat_inbounds(self,lat):
        bounds = [float(self.ds["lat"][0].values),float(self.ds["lat"][-1].values)]
        bounds.sort()
        if lat>=bounds[0] and lat<=bounds[1] :
            return True
        else :
            return False
        
    def islon_inbounds(self,lon):
        bounds = [float(self.ds["lon"][0].values),float(self.ds["lon"][-1].values)]
        bounds.sort()
        if lon>=bounds[0] and lon<=bounds[1] :
            return True
        else :
            return False
        
    def ist_inbounds(self,t):
        bounds = [pd.to_datetime(self.ds["time"][0].values),pd.to_datetime(self.ds["time"][-1].values)]
        bounds.sort()
        if t>=bounds[0] and t<=bounds[1] :
            return True
        else :
            return False
        
    def lat2i(self,lat):
        lat_min = float(self.ds["lat"][0])
        lat_max = float(self.ds["lat"][-1])
        nb = len(self.ds["lat"])
        lat_res = (lat_max-lat_min)/nb
        return int(round((lat-lat_min)/lat_res))
    
    def i2lat(self,i):
        lat_min = float(self.ds["lat"][0])
        lat_max = float(self.ds["lat"][-1])
        nb = len(self.ds["lat"])
        lat_res = (lat_max-lat_min)/nb
        lat = i*lat_res+lat_min
        return lat

    def lon2i(self,lon):
        lon_min = float(self.ds["lon"][0])
        lon_max = float(self.ds["lon"][-1])
        nb = len(self.ds["lon"])
        lon_res = (lon_max-lon_min)/nb
        return int(round((lon-lon_min)/lon_res))
    
    def i2lon(self,i):
        lon_min = float(self.ds["lon"][0])
        lon_max = float(self.ds["lon"][-1])
        nb = len(self.ds["lon"])
        lon_res = (lon_max-lon_min)/nb
        lon = i*lon_res+lon_min
        return lon

    def t2i(self,t):
        t_min = pd.to_datetime(self.ds["time"][0].values)
        t_max = pd.to_datetime(self.ds["time"][-1].values)
        nb = len(self.ds["time"])
        t_res = (t_max-t_min)/nb
        return int(round((t-t_min)/t_res))

    def getchunk(self,param,n_deg,e_deg,s_deg,w_deg,t):
        slug = f"{param}_{n_deg}_{e_deg}_{s_deg}_{w_deg}_{t}"
        # print(f"SatDataSet.getchunk() : {slug}")
        ilat = [self.lat2i(n_deg),self.lat2i(s_deg)]
        ilon = [self.lon2i(w_deg),self.lon2i(e_deg)]
        it = self.t2i(t)
        
        ilat.sort()
        ilon.sort()
        return self.ds[param][it,ilat[0]:ilat[1],ilon[0]:ilon[1]]
    
    def getchunks(self,param,n_deg,e_deg,s_deg,w_deg,start,end):
        slug = f"{param}_{n_deg}_{e_deg}_{s_deg}_{w_deg}_{start}_{end}"
        print(param,n_deg,e_deg,s_deg,w_deg,start,end)
        # print(f"SatDataSet.getchunk() : {slug}")
        ilat = [self.lat2i(n_deg),self.lat2i(s_deg)]
        ilon = [self.lon2i(w_deg),self.lon2i(e_deg)]
        it = [self.t2i(start),self.t2i(end)]
        
        ilat.sort()
        ilon.sort()
        print(param, ilat, ilon, it)
        return self.ds[param][it[0]:it[1],ilat[0]:ilat[1],ilon[0]:ilon[1]]
        
    def setcache_series(self,slug,series):
        fname = fm.build_path(self.root,"data","cached",slug,ext=".csv")
        series.to_csv(fname,index=False)
        
    def getcache_series(self,slug):
        fname = fm.build_path(self.root,"data","cached",slug,ext=".csv")
        df = pd.read_csv(fname)
        df["datetime"] = pd.to_datetime(df.datetime)
        return df
    
    def iscached(self,slug):
        fname = fm.build_path(self.root,"data","cached",slug,ext=".csv")
        return os.path.exists(fname)
            
    def getseries(self,params,lat,lon,start=0,end=0):
        slug = f"{'_'.join(params)}_{lat}_{lon}_{start}_{end}"
        print(f"SatDataSet.getseries() : {slug}")
        if self.iscached(slug) :
            return self.getcache_series(slug)
        else :
            data = self.ds.sel(lat=lat,lon=lon,method="nearest")
            
            df = pd.DataFrame()
            df["datetime"] = pd.to_datetime(data["time"].values)
            
            for param in params :
                df[param] = data[param].values.astype(float)

            df.dropna(inplace=True)
            df = tl.addmdates(df)
            
            self.setcache_series(slug, df)
            return df
            

def copernicusmarine_datastore(dataset, username, password):
    cas_url = 'https://cmems-cas.cls.fr/cas/login'
    session = setup_session(cas_url, username, password)
    session.cookies.set("CASTGC", session.cookies.get_dict()['CASTGC'])
    database = ['my', 'nrt']
    url = f'https://{database[0]}.cmems-du.eu/thredds/dodsC/{dataset}'
    try:
        data_store = xr.backends.PydapDataStore(open_url(url, session=session)) # needs PyDAP >= v3.3.0 see https://github.com/pydap/pydap/pull/223/commits 
    except:
        url = f'https://{database[1]}.cmems-du.eu/thredds/dodsC/{dataset}'
        data_store = xr.backends.PydapDataStore(open_url(url, session=session)) # needs PyDAP >= v3.3.0 see https://github.com/pydap/pydap/pull/223/commits
    return data_store


# retro compatibility
def get_SPM_CHL_TUR():
    params = ["SPM","CHL","TUR"]
    
    sds = SatDataSet()
    
    lat,lon = 42.704167,3.066667
    
    return sds.getseries(params, lat, lon)
        

   
#%%
if __name__ == '__main__':
    # 43.671118 , 5.163132
    # 42.463854 , 2.923778
    # coord_poem = {"lat" : 42.704167, "lon" : 3.066667}
    
    df_loc = read_geojson.from_Point_FeatureCollection(read_geojson.fname_estuaries_loc)
    zones = read_geojson.build_zones(df_loc)
    names = df_loc.name.values
    
    # zones = [
    #     [43.637,4.457,42.969,5.155],
    #     [43.63,3.875,42.92,4.697],
    #     [43.606,3.159,42.932,4.01],
    #     [43.194,2.925,42.753,3.691],
    #     [42.771,2.957,42.47,3.642]
    #     ]
    
    satds = SatDataSet()
    
    print(satds.getparams())
    
    # list available dates
    dates = pd.date_range("2021-07-01","2021-10-01")
    ndates = []
    for date in dates:
        chunk = satds.getchunk("TUR", 42.703, 3.067, 42.705, 3.065, date)
        if np.sum(~np.isnan(chunk.values.flatten())) > 0 :
            print(date)
            ndates.append(date)

    meta = []
    for zone in zones :
        print(zone)
        print(ndates[0])
        tdata = satds.getchunk("TUR", *zone, ndates[0]).values.flatten()
        for i,date in enumerate(ndates[1:]):
            print(date)
            tmp = satds.getchunk("TUR", *zone, date).values.flatten()
            tdata = np.column_stack((tdata,tmp))
        meta.append(tdata)
     
    # %%graph 
    zone = [43.671118 ,5.163132, 42.463854, 2.923778]
    date = pd.to_datetime("2021-07-05 00:00:00")
    data = satds.getchunk("TUR", *zone, date)
    norm = Normalize(np.nanmin(np.log10(data)),np.nanpercentile(np.log10(data),99))
    
    fig, axe = plt.subplots()
    axe.matshow(np.log10(data),norm=norm)
    
    for i, area in enumerate(zones) :
        print(zone)
        zlon = np.array(zone)[[1,3]]
        zlat = np.array(zone)[[0,2]]
        
        zlon0 = np.min(zlon)
        zlon1 = np.max(zlon)
        zlat0 = np.min(zlat)
        zlat1 = np.max(zlat)
        
        zx0 = satds.lon2i(zlon0)
        zx1 = satds.lon2i(zlon1)
        zy1 = satds.lat2i(zlat0)
        zy0 = satds.lat2i(zlat1)    
        
        zx = zx0
        zy = zy0
        
        zw = zx1-zx0
        zh = zy1-zy0
        
        alon = np.array(area)[[1,3]]
        alat = np.array(area)[[0,2]]
        
        alon0 = np.min(alon)
        alon1 = np.max(alon)
        alat0 = np.min(alat)
        alat1 = np.max(alat)
        
        ax0 = satds.lon2i(alon0)
        ax1 = satds.lon2i(alon1)
        ay1 = satds.lat2i(alat0)
        ay0 = satds.lat2i(alat1)    
        
        ax = ax0
        ay = ay0
        
        aw = ax1-ax0
        ah = ay1-ay0
        
        x = ax-zx
        y = ay-zy
        w = aw
        h = ah
        
        print(x,y,w,h)
        
        axe.add_patch(Rectangle((x, y), w, h,fill=False,label=f"{names[i]}",edgecolor=tl.color(i)))
    
    fig.legend()
    fig.tight_layout(pad=0)
    fig.subplots_adjust(wspace=0, hspace=0)
    
    #%%
    nbzones = len(zones)
    fig, ax = plt.subplots(sharex=True,nrows=nbzones)
    
    for i,tdata in enumerate(meta):
        vpers = np.array([10,25,50,75,90])
        tper = np.nanpercentile(tdata,vpers[0],axis=0)
        for vper in vpers[1:]:
            tper = np.column_stack((tper,np.nanpercentile(tdata,vper,axis=0)))
        
        ax[i].fill_between(ndates,tper[:,0],tper[:,4],alpha=0.25,label="percentile 10, 90")
        ax[i].fill_between(ndates,tper[:,1],tper[:,3],alpha=0.5,label="percentile 25, 75")
        ax[i].plot(ndates,tper[:,2],c="black",label="median")
        ax[i].set_ylabel(f"TUR {names[i]}")
        ax[i].grid(True,c="white")
        if i == 0 :
            ax[i].legend()
        
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    fig.subplots_adjust(wspace=0, hspace=0)
        
    tl.warn()
        
    
    
