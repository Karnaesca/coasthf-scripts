import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#utctimestamp_yyyy-mm-dd_hh:mm:ss,atmosphericpressure_mbar,airtemperature_degc,windspeed_kn,windirection_deg
#2020-01-01 00:00:00,1032.7,7.0,9.9,269.5

fname_POEM_met_2020 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/POEM_subsurface_2020_2021/oobobsbuo_poem_met0med5m_2020.csv"
fname_POEM_met_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/POEM_subsurface_2020_2021/oobobsbuo_poem_met0med5m_2021.csv"
fname_SOLA_met_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/SOLA_subsurface_2021_2022/oobobsbuo_sola_met0med5m_2021.csv"
fname_SOLA_met_2022 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/SOLA_subsurface_2021_2022/oobobsbuo_sola_met0med5m_2022.csv"

def met(fname,add_mdates=True):
    dateparse = lambda X: datetime.datetime.strptime(X,'%Y-%m-%d %H:%M:%S')
    print(f"Read MET\n{fname}")
    data = pd.read_table(fname,header=0,
                        decimal='.',
                        sep = ",",
                        usecols=[0,1,2,3,4],
                        names=["﻿utctimestamp","atmosphericpressure_mbar","airtemperature_degc","windspeed_kn","windirection_deg"],
                        parse_dates={"datetime": ["﻿utctimestamp"]},
                        date_parser=dateparse
                        #delim_whitespace = True,
                        #skip_blank_lines=True
                        )
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

def mets(*fnames,add_mdates=True):
    data = met(fnames[0],False)
    for fname in fnames[1:]:
        data = pd.concat((data,met(fname,False)))
    data.reset_index(drop=True,inplace=True)
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data
    