import time
import json
import requests
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime
import os
from size import A4_paysage, A4_portait, cm

def get_params(platform,verbose=False):
    url = f"https://data.coriolis-cotier.org/api/platforms/{platform}/details"
    r = requests.get(url)
    if verbose:
        print(f"Connection:\n{url}\nstatus code : {r.status_code}")
    if r.status_code == 200 :
        d_data = json.loads(r.text)
        vals = [e["parameterCode"] for e in list(d_data["parametersList"].values())]
        return vals
    
def get_params_POEM():
    platform="EXIN0003"
    return get_params(platform)

def get_params_SOLA():
    platform="EXIN0006"
    return get_params(platform)
    
def get_info_param(param,verbose=False):
    url = f"https://data.coriolis-cotier.org/api/params/parametre?code={str(param)}"
    r = requests.get(url)
    if verbose:
        print(f"Connection:\n{url}\nstatus code : {r.status_code}")
    if r.status_code == 200 :
        d_data = json.loads(r.text)
        return d_data    

def totimestamp(str_date):
    timestamp = time.mktime(time.strptime(str_date, '%Y%m%d'))
    return timestamp

def get_data(param,start,end,verbose=False,platform="EXIN0003"):
    url = f"https://api-coriolis.ifremer.fr/legacy/timeseries?downsampling=LTTB&format=highcharts&platform={platform}&parameter={param}&measuretype=15&start={start}&end={end}&samplingparameter=0&qc=0&qc=1&qc=2&qc=3&qc=4&qc=5&qc=6&qc=7&qc=8&qc=9"
    
    r = requests.get(url)
    
    if verbose:
        print(f"Connection:\n{url}\nstatus code : {r.status_code}")
    if r.status_code == 200 :
        d_data = json.loads(r.text)
        t_data = d_data["result"][0]["data"]
        data = np.asarray(t_data)
        data[:,0] = data[:,0]/1000 #the timestamp is somehow off by 1000, so ...
    return data

def graph(x,y,label,fname,show=False,verbose=False):
    dt = [datetime.fromtimestamp(e) for e in x]
    t = [mdates.date2num(e) for e in dt]
    
    if verbose:
        print(f"[{label}] amount data : {len(y)}")
    
    fig, axes = plt.subplots(1,1)
    fig.set_size_inches(29.7*(25/64),21*(25/64))
    axes.plot(t,y,label=f"{label} ({unit})")
    axes.grid(True)
    axes.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    axes.xaxis.set_minor_locator(mdates.MonthLocator())
    axes.legend()
    fig.set_dpi(150.0)
    plt.gcf().autofmt_xdate()
    #plt.savefig(f"POEM_{label}_{min(x)}_{max(x)}.png")
    #plt.savefig(fname)
    if show :
        plt.show()
        
        

        
if __name__ == '__main__':
    # start = int(totimestamp("20170517"))
    # end = int(totimestamp("20220308"))
    # #params = get_params_POEM()
    # params = interest
    
    # folder = "dump/"
    # current = os.getcwd()
    # path = os.path.join(current,folder)
    # if not os.path.exists(path):
    #     os.mkdir(path)
    
    # fig, axes = plt.subplots(len(params),1,sharex=True)
    # fig.set_size_inches(29.7*(25/64),21*(25/64))
    
    # for i,param in enumerate(params):
    #     d_info = get_info_param(param)
    #     label = d_info["label"]
    #     unit = d_info["unit"]
    #     print(f"{label} - {unit}")
    #     data = get_data(param,start,end)
    #     x = data[:,0]
    #     y = data[:,1]
    #     dt = [datetime.fromtimestamp(e) for e in x]
    #     t = [mdates.date2num(e) for e in dt]
    #     #data = np.stack((x, y), axis=-1)
    #     header = f"timestamp, {label} ({unit})"
    #     fname = f"POEM_{label}_{start}_{end}.txt"
    #     fullpath = os.path.join(path,fname)
    #     #np.savetxt(fullpath,X=data,header=header,delimiter=",",newline="\n",fmt="%f")
        
    #     #graph(x, y, label,fullpath)
        
    #     axes[i].plot(t,y,label=f"{label} ({unit})")
    #     #axes[i].set_ylabel(label)
    #     axes[i].grid(True)
    #     axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    #     axes[i].xaxis.set_minor_locator(mdates.MonthLocator())
    #     axes[i].legend()
        
    # fig.set_dpi(150.0)
    # plt.gcf().autofmt_xdate()
    #fname = f"POEM_{start}_{end}.png"
    #fullpath = os.path.join(path,fname)
    #plt.savefig(fullpath)
    
# =============================================================================
#     LOAD THE THINGS I NEED
# =============================================================================
    
    start = int(totimestamp("20170517"))
    end = int(totimestamp("20220308"))
    params = [30,61,63,681]
    
    for param in params:
        d_info = get_info_param(param)
        label = d_info["label"]
        unit = d_info["unit"]
        print(f"{label} - {unit}")
        data = get_data(param,start,end)
        
        
        x = data[:,0]
        y = data[:,1]
        dt = [datetime.fromtimestamp(e) for e in x]
        t = [mdates.date2num(e) for e in dt]

        fig, ax = plt.subplots()
        fig.set_size_inches(21*cm,7*cm)
        
        ax.plot(t,y,label=f"{label} ({unit})")
        ax.grid(True)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        ax.legend()

        
        
    
    
    
    