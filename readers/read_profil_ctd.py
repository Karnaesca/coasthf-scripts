import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#Id Sortie,Date ,Heure UTC ,CTD,,,,
#,,,T°c,Fluorescence,Turbidité,Profondeur,Salinité
#POEM20170915,15/09/2017,08:09:27,19.3033,0.2060,3.519,0.75,37.8061

fname_CHIFRE_POEM_ctd_2017_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/CHIFRE_2017_2021/CHIFRE_POEM_ctd_2017_2021.csv"

def profil_ctd(fname):
    print(f"Read Profil CTD\n{fname}")
    data = pd.read_table(fname,skiprows=5,
                        decimal='.',
                        sep = ",",
                        usecols=[1,2,3,4,5,6,7],
                        names=["date","hours","temperature_degc","fluorescence_rfu","turbidity_ntu","depth_m","salinity"]
                        )    
    return data

def manual_ctd_2m(fname):
    data = profil_ctd(fname)
    data.dropna(how="all",inplace=True)
    data["datetime"] = pd.to_datetime(data.date+"\t"+data.hours,format="%d/%m/%Y\t%H:%M:%S")
    data.drop(columns=["date","hours"],inplace=True)
    data["mdates"] = [mdates.date2num(e) for e in data.datetime]
    data = data[data["depth_m"]==2]
    
    # on peut cut avec le temps :o
    #data[data.datetime >= "2022-01-01"]
    return data

def manual_ctd(fname):
    data = profil_ctd(fname_profil_ctd)
    data.dropna(how="all",inplace=True)
    data["datetime"] = pd.to_datetime(data.date+"\t"+data.hours,format="%d/%m/%Y\t%H:%M:%S")
    data.drop(columns=["date","hours"],inplace=True)
    
    # on peut cut avec le temps :o
    #data[data.datetime >= "2022-01-01"]
    return data

if __name__ == '__main__':
    data = profil_ctd(fname_profil_ctd)
    data.dropna(how="all",inplace=True)
    data["datetime"] = pd.to_datetime(data.date+"\t"+data.hours,format="%d/%m/%Y\t%H:%M:%S")
    data.drop(columns=["date","hours"],inplace=True)