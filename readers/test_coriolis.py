import load_coriolis_cotier as clib
import matplotlib.pyplot as plt

start = int(clib.totimestamp("20170517"))
end = int(clib.totimestamp("20220308"))

fig=False

params = clib.get_params_POEM()
#params = clib.get_params_SOLA()

print(f"Amount of parameters available : {len(params)}")

for param in params:
    d_info = clib.get_info_param(param)
    print(f"\t{param}\t{d_info['label']} ({d_info['unit']})")
    
    data = clib.get_data(param,start,end)
    
    t_qc = data[:,2]
    
    if fig:
        plt.Figure()
        plt.hist(t_qc,bins=10,range=(0,9),label=f"{d_info['label']} ({d_info['unit']})",rwidth=1)
        plt.title(f"Histogram of QC codes {d_info['label']} ({d_info['unit']})")
        plt.legend()
        plt.savefig(f"{param}_hist_qc.png")
        plt.show()