import pandas as pd
import os

import read_ctd as ctd
import read_met as met
import read_mar as mar
import file_manager as fm
import tools as tl

# This script downsample SOLA and POEM data when we don't need as much HF data. 
# It does that by calculating the median by bins of 6h and 1d and saves the
# result as files. The files can be loaded via the read_composite script

# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.date_range.html
# https://numpy.org/doc/stable/reference/generated/numpy.array_split.html
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.resample.html?highlight=resample#pandas.DataFrame.resample

force = False

root = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/"

# POEM
# downsample 1 median value per day
fname = fm.build_path(root, "data","resampled","data_POEM_1d_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    if "data_ctd_POEM" not in globals() or force:
        data_ctd_POEM = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021,add_mdates=False)
    if "data_met_POEM" not in globals() or force:
        data_met_POEM = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021,add_mdates=False)
    if "data_POEM" not in globals() or force:
        data_POEM = pd.merge(data_ctd_POEM,data_met_POEM,how="outer",on="datetime")
    
    data_POEM_1d_med = data_POEM.groupby(data_POEM.datetime.dt.strftime("%Y-%m-%d")).median()
    data_POEM_1d_med.index.names = ["index"]
    data_POEM_1d_med.loc[:,"datetime"] = pd.to_datetime(data_POEM_1d_med.index)
    data_POEM_1d_med.loc[:,"mdates"] = [mdates.date2num(e) for e in data_POEM_1d_med.datetime]
    data_POEM_1d_med.sort_values(by="datetime",inplace=True)
    data_POEM_1d_med.reset_index(inplace=True,drop=True)
    data_POEM_1d_med.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")
    
# downsample 1 median value per 6 hours
fname = fm.build_path(root, "data","resampled","data_POEM_6h_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    if "data_ctd_POEM" not in globals() or force:
        data_ctd_POEM = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021,add_mdates=False)
    if "data_met_POEM" not in globals() or force:
        data_met_POEM = met.mets(met.fname_POEM_met_2020,met.fname_POEM_met_2021,add_mdates=False)
    if "data_POEM" not in globals() or force:
        data_POEM = pd.merge(data_ctd_POEM,data_met_POEM,how="outer",on="datetime")
    
    data_POEM_6h_med = data_POEM.resample("6H",on="datetime").median()
    data_POEM_6h_med.index.names = ["index"]
    data_POEM_6h_med.loc[:,"datetime"] = pd.to_datetime(data_POEM_6h_med.index)
    data_POEM_6h_med.loc[:,"mdates"] = [mdates.date2num(e) for e in data_POEM_6h_med.datetime]
    data_POEM_6h_med.sort_values(by="datetime",inplace=True)
    data_POEM_6h_med.reset_index(inplace=True,drop=True)
    data_POEM_6h_med.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")   
 
# SOLA
# downsample 1 median value per day
fname = fm.build_path(root, "data","resampled","data_SOLA_1d_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    if "data_ctd_SOLA" not in globals() or force:
        data_ctd_SOLA = ctd.ctds(ctd.fname_SOLA_ctd_2021,ctd.fname_SOLA_ctd_2022,add_mdates=False)
    if "data_met_SOLA" not in globals() or force:
        data_met_SOLA = met.mets(met.fname_SOLA_met_2021,met.fname_SOLA_met_2022,add_mdates=False)
    if "data_SOLA" not in globals() or force:
        data_SOLA = pd.merge(data_ctd_SOLA,data_met_SOLA,how="outer",on="datetime")
    
    data_SOLA_1d_med = data_SOLA.groupby(data_SOLA.datetime.dt.strftime("%Y-%m-%d")).median()
    data_SOLA_1d_med.index.names = ["index"]
    data_SOLA_1d_med.loc[:,"datetime"] = pd.to_datetime(data_SOLA_1d_med.index)
    data_SOLA_1d_med.loc[:,"mdates"] = [mdates.date2num(e) for e in data_SOLA_1d_med.datetime]
    data_SOLA_1d_med.sort_values(by="datetime",inplace=True)
    data_SOLA_1d_med.reset_index(inplace=True,drop=True)
    data_SOLA_1d_med.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")
    
# downsample 1 median value per 6 hours
fname = fm.build_path(root, "data","resampled","data_SOLA_6h_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    if "data_ctd_SOLA" not in globals() or force:
        data_ctd_SOLA = ctd.ctds(ctd.fname_SOLA_ctd_2021,ctd.fname_SOLA_ctd_2022,add_mdates=False)
    if "data_met_SOLA" not in globals() or force:
        data_met_SOLA = met.mets(met.fname_SOLA_met_2021,met.fname_SOLA_met_2022,add_mdates=False)
    if "data_SOLA" not in globals() or force:
        data_SOLA = pd.merge(data_ctd_SOLA,data_met_SOLA,how="outer",on="datetime")
    
    data_SOLA_6h_med = data_SOLA.resample("6H",on="datetime").median()
    data_SOLA_6h_med.index.names = ["index"]
    data_SOLA_6h_med.loc[:,"datetime"] = pd.to_datetime(data_SOLA_6h_med.index)
    data_SOLA_6h_med.loc[:,"mdates"] = [mdates.date2num(e) for e in data_SOLA_6h_med.datetime]
    data_SOLA_6h_med.sort_values(by="datetime",inplace=True)
    data_SOLA_6h_med.reset_index(inplace=True,drop=True)
    data_SOLA_6h_med.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")
    
# downsample 1 median value per 1 day for tide PN
fname = fm.build_path(root, "data","resampled","data_tide_PN_3h_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    data_tide_PN = mar.mars(mar.fname_maree_PN_2020,mar.fname_maree_PN_2021)
    data_tide_PN_med3h = data_tide_PN.resample("3H",on="datetime").median()
    data_tide_PN_med3h.reset_index(inplace=True)
    data_tide_PN_med3h = tl.addmdates(data_tide_PN_med3h)
    data_tide_PN_med3h.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")
    
# downsample 1 median value per 1 day for tide PV
fname = fm.build_path(root, "data","resampled","data_tide_PV_3h_med",ext=".csv")
if (not os.path.exists(fname)) or force :
    data_tide_PV = mar.mars(mar.fname_maree_PV_2020,mar.fname_maree_PV_2021)
    data_tide_PV_med3h = data_tide_PV.resample("3H",on="datetime").median()
    data_tide_PV_med3h.reset_index(inplace=True)
    data_tide_PV_med3h = tl.addmdates(data_tide_PV_med3h)
    data_tide_PV_med3h.to_csv(fname,index=False)
else :
    print(f"Not resampling - already exist :\n{fname}")   


