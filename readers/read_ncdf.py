import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd
from datetime import datetime

fname = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/archives/cmems_obs_oc_med_bgc_tur-spm-chl_nrt_l4-hr-mosaic_P1D-m_1649347561447.nc"

ds = nc.Dataset(fname)

lats = ds["lat"]
lons = ds["lon"]

time = ds["time"]

TURS = ds["TUR"]

base = pd.Timestamp("2020").timestamp()

for i in range(len(TURS)):
    offset = float(time[i])
    ts = base + offset
    fig, ax = plt.subplots()
    ax.matshow(TURS[i])
    ax.set_title(datetime.fromtimestamp(ts).strftime("%Y-%m-%d"))
    fig.savefig(f"pict{i}.png")
    plt.close()



