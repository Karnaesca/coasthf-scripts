import json
import requests
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime
import pandas as pd
import file_manager as fm
import os
from tools import *

d_stations = {
    "station_Rhone" : "V7200005",
    "station_Lez_Montpellier" : "Y3210010",
    "station_Herault_Agde" : "Y2370020",
    "station_Orb_Beziers" : "Y2580010",
    "station_Aude_Coursan" : "Y1612040",
    "station_Agly_Riveslates" : "Y0674060",
    "station_Tet_Perpignan" : "Y0474030",
    "station_Tech_Argeles" : "Y0284060",
    "station_Baillaury_Banyuls" : "Y0105220"
    }


# for retro compatiblity
for name,station in list(d_stations.items()):
    globals()[name] = station

root = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/"

def code2name(code):
    d_stations_names = dict(zip(list(d_stations.values()),list(d_stations.keys())))
    return d_stations_names[code]

class HydroSeries:
    def __init__(self,station = "Y0474030",
        start="01/01/2020",
        end="01/01/2022",
        variableType = "daily_variable",
        dailyVariable = "QmnJ",
        statusdata = "pre_validated_and_validated",
        step = 1,
        event = 1,
        useUserTimezone = 0):
        """
        Parameters
        ----------
        station : TYPE, optional
            DESCRIPTION. The default is "Y0474030".
        start : TYPE, optional
            DESCRIPTION. The default is "01/01/2020".
        end : TYPE, optional
            DESCRIPTION. The default is "01/01/2022".
        variableType : TYPE, optional
            DESCRIPTION. The default is "daily_variable".
        dailyVariable : TYPE, optional
            DESCRIPTION. The default is "QmnJ".
        statusdata : TYPE, optional
            DESCRIPTION. The default is "pre_validated_and_validated".
            possible values : most_valid, validated, re_validated_and_validated, raw
        step : TYPE, optional
            DESCRIPTION. The default is 1.
        event : TYPE, optional
            DESCRIPTION. The default is 1.
        useUserTimezone : TYPE, optional
            DESCRIPTION. The default is 0.

        Returns
        -------
        Instance of HydroSeries class

        """
        
        start = pd.to_datetime(start).strftime("%d/%m/%Y")
        end = pd.to_datetime(end).strftime("%d/%m/%Y")
        
        # check if data already exist on disk
        slug = f"{station}_{start}_{end}_{dailyVariable}_{statusdata}_{step}"
        path = fm.build_path(root,"data","cached",slug,ext=".cache")
        
        if os.path.exists(path) :
            # the data is already available on disk, load the data from disk
            print(f"The data {path} is already available on disk.\nLoad data from disk :\n{path}")
            with open(path,"r") as f:
                text = f.read()
                self.load_from_text(text)
            
        else :
            print(f"The data {fm.to_fname(slug)} doesn't seems to be on disk.")
            url = f"https://www.hydro.eaufrance.fr/sitehydro/ajax/{station}/series?"
            url += f"hydro_series%5BstartAt%5D={start}"
            url += f"&hydro_series%5BendAt%5D={end}"
            url += f"&hydro_series%5BvariableType%5D={variableType}"
            url += f"&hydro_series%5BdailyVariable%5D={dailyVariable}"
            url += f"&hydro_series%5BstatusData%5D={statusdata}"
            url += f"&hydro_series%5Bstep%5D={str(step)}"
            url += f"&hydro_series%5Bevent%5D={str(event)}"
            url += f"&useUserTimezone={str(useUserTimezone)}"
            
            print(f"Load HydroSeries :\n{url}\n")
    
            r = requests.get(url)
            self.status_code = r.status_code

            if r.status_code == 200:
                self.load_from_text(r.text)
                # save freshy downloaded data on disk, for later
                with open(path,"w") as f:
                    f.write(r.text)
               

    def load_from_text(self,text):
        self.raw = json.loads(text)
        self.title = self.raw["series"]["title"]
        self.station_code = self.raw["series"]["code"]
        self.unit = self.raw["unitQ"]
        self.station_name = code2name(self.station_code)
        self.name = f"volume {self.unit}/s {self.station_name}"
        self.col_name = f"volume_{self.unit}_s"
        #print(self.raw["series"]["data"])
        if self.raw["series"]["data"] == [] :
            self.data = pd.DataFrame(columns=["datetime","mdates","volume_l_s"])
        else :
            self.data = pd.json_normalize(self.raw["series"]["data"])
            # datetime
            self.data["datetime"]=pd.to_datetime(self.data["t"],format="%Y-%m-%dT%H:%M:%SZ")
            self.data.drop(columns=["t","md","s","q","m","c"],inplace=True)
            self.data["mdates"] = [mdates.date2num(e) for e in self.data.datetime]
            self.data.rename(columns={"v":self.col_name},inplace=True)
        

if __name__ == '__main__' :
    start="01/01/2020"
    end="01/01/2022"     
    station=station_Tet_Perpignan      
    hydro = HydroSeries(station=station,start=start,end=end).data
    
    len(hydro)
    
    # plt.plot(hydro1.df_hydro_series.volume_l_s)