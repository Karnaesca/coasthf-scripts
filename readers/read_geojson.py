import pandas as pd
import json

fname_estuaries_loc = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/esutaries_loc/estuaries.geojson"

def from_Point_FeatureCollection(fname):
    with open(fname,"r") as f:
        dump = f.read()
    dump = json.loads(dump)
    df = pd.DataFrame()
    df["name"] = [feature["properties"]["name"] for feature in dump["features"]]
    df["lon"] = [feature["geometry"]["coordinates"][0] for feature in dump["features"]] 
    df["lat"] = [feature["geometry"]["coordinates"][1] for feature in dump["features"]]
    return df

def build_zones(df_loc_point,offset=0.05):
    res = []
    for i in range(len(df_loc_point)):
        lat = df_loc_point.lat.iloc[i]
        lon = df_loc_point.lon.iloc[i]
        tmp = [round(lat-offset,4), round(lon+offset,4), round(lat+offset,4), round(lon-offset,4)]
        res.append(tmp)
    return res

if __name__ == '__main__' :
    df = from_Point_FeatureCollection(fname_estuaries_loc)
    print(build_zones(df))
    
    