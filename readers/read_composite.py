import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta
import os
import sys

import load_hydroportail as hydro
import read_chemistry as chem
import read_profil_ctd as pctd
import read_ctd as ctd
import read_met as met
import file_manager as fm
import tools as tl

# https://www.geeksforgeeks.org/python-program-to-create-dynamically-named-variables-from-user-input/
# https://www.geeksforgeeks.org/python-list-files-in-a-directory/
# https://theprogrammingexpert.com/python-check-if-variable-exists/
# https://stackoverflow.com/questions/1429814/how-to-programmatically-set-a-global-module-variable

# resample data if not available
import resample

# make fnames for all the files form the resampled directory

roots = [
        "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/resampled",
        "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/recalibrated",
        "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/validator",
        "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/part"
        ]

l_vars = []

def build_fnames(root):
    module = sys.modules[__name__]
    #root = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/resampled"
    fnames = os.listdir(root)
    for fname in fnames:
        base = os.path.basename(fname)
        name,ext = os.path.splitext(base)
        if ext == ".csv":
            vname = f"fname_{name}"
            path = os.path.join(root, fname)
            #print(f"Available var : {vname}")
            #globals()[vname] = path
            global l_vars
            l_vars.append(vname)
            setattr(module,vname,path)
            
def cmp(fname,dtcol="datetime"):
    data = pd.read_csv(fname)
    data[dtcol] = pd.to_datetime(data[dtcol])
    data = tl.addmdates(data,dtcol=dtcol)
    return data

def list_vars():
    global l_vars
    return l_vars

for root in roots:
    #print(f"\nFrom : {root}")
    build_fnames(root)




