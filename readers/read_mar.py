import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# Station : PORT-VENDRES
# Longitude : 3.10745
# Latitude : 42.519922
# Organisme fournisseur de données : Shom, CR Languedoc-Roussillon, CG Pyrénées-Orientales
# Fuseau horaire : UTC
# Référence verticale : zero_hydrographique
# Unité : m
# Source 1 : Données brutes temps réel
# Source 2 : Données brutes temps différé
# Source 3 : Données validées temps différé
# Source 4 : Données horaires validées
# Source 5 : Données horaires brutes
# Source 6 : Pleines et basses mers
# Date;Valeur;Source
#01/01/2020 00:00:00;0.465;4

fname_maree_PV_2020 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/maregraphie/75_2020.txt"
fname_maree_PV_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/maregraphie/75_2021.txt"
fname_maree_PN_2020 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/maregraphie/803_2020.txt"
fname_maree_PN_2021 = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/maregraphie/803_2021.txt"

def mar(fname,add_mdates=True):
    dateparse = lambda X: datetime.datetime.strptime(X,'%d/%m/%Y %H:%M:%S')
    print(f"Read MAR\n{fname}")
    data = pd.read_table(fname,header=14,
                        decimal='.',
                        sep = ";",
                        usecols=[0,1,2],
                        names=["dates","h_mer_m","source"],
                        parse_dates={"datetime": ["dates"]},
                        date_parser=dateparse
                        )
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

def mars(*fnames,add_mdates=True):
    data = mar(fnames[0],False)
    for fname in fnames[1:]:
        data = pd.concat((data,mar(fname,False)))
    data.reset_index(drop=True,inplace=True)
    if add_mdates :
        data.loc[:,"mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

if __name__ == '__main__':
    data = mars(fname_maree_PV_2020,fname_maree_PV_2021)
    
    rmean = data.h_mer_m.rolling(100).mean()
    
    fig, ax = plt.subplots()
    ax.plot(data.mdates,data.h_mer_m)
    ax.plot(data.mdates,rmean)
