import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#"Date (TU)","Valeur (en m³/s)","Statut","Qualification","Méthode","Continuité"
#2021-01-01T00:00:00.000Z,"6.28","12",20,8,0
fname_debit_2020_Tet = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/debit_Tet/debit_Tet_2020.csv"
fname_debit_2021_Tet = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/debit_Tet/debit_Tet_2021.csv"

def debit(fname):
    print(f"Read debit:\n{fname}")
    data = pd.read_table(fname,skiprows=1,
                        decimal='.',
                        sep = ",",
                        usecols=[0,1],
                        names=["date","debit_m3_s"]
                        )
    data["datetime"]=pd.to_datetime(data["date"],format="%Y-%m-%dT%H:%M:%S.%fZ")
    data.drop(columns=["date"],inplace=True)
    data["mdates"] = [mdates.date2num(e) for e in data.datetime]
    return data

if __name__ == '__main__':
    data_debit = debit(fname_debit_2020_Tet)
    plt.plot(data_debit.mdates,data_debit.debit_m3_s)

    