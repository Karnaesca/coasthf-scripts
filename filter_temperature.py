import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys

import load_hydroportail as hydro
import load_copernicus as coper
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm

from meteostat import Stations
from meteostat import Hourly

# =============================================================================
# LOAD DATA
# =============================================================================

data_POEM_HF = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)

#%% =============================================================================
# COMPUTE STD
# =============================================================================
t = data_POEM_HF.datetime
temperature_degc = data_POEM_HF.temperature_degc

l = len(temperature_degc)
window_mean = 288
window_std = int(window_mean*2)
f = 5

mean = temperature_degc.rolling(window_mean,center=True,min_periods=1).mean()
std = temperature_degc.rolling(window_std,center=True,min_periods=1).std()

threshold = 1
threshold = threshold/f
std = std.where(std>threshold,threshold)

over = mean+std*f
under = mean-std*f

select = (temperature_degc <= over) & (temperature_degc >= under)

nb_rejected = (~select).sum() - temperature_degc.isnull().sum()

print(f"Nb rejected : {nb_rejected}")


if __name__ == '__main__' :
# =============================================================================
# GRAPH STD
# =============================================================================

    fig, ax = plt.subplots()
    fig.set_size_inches(21*cm,7*cm)
    
    ax.plot(t,temperature_degc)
    ax.plot(t,mean,c="tab:orange",zorder=10)
    # ax.plot(t,over)
    # ax.plot(t,under)
    ax.fill_between(t,under,over,color="tab:purple",alpha=0.3,zorder=-10)
    ax.scatter(t,temperature_degc.where(~select),marker="x",c="tab:red",zorder=20)
    ax.set_ylabel("")
    ax.legend()
    
    ax.grid(True)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
    
    # =============================================================================
    # GRAPH CLEAN
    # =============================================================================
    
    fig, ax = plt.subplots()
    fig.set_size_inches(21*cm,7*cm)
    
    ax.plot(t,temperature_degc.where(select))
    ax.plot(t,temperature_degc,alpha=0.3,zorder=-10)
    ax.set_ylabel("")
    ax.legend()
    
    ax.grid(True)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    fig.autofmt_xdate()
    fig.tight_layout(pad=0)
