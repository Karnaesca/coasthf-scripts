import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import datetime

# 0    No quality control (QC) was performed.
# 1    QC was performed: good data
# 2    QC was performed: probably good data
# 3    QC was performed: probably bad data
# 4    QC was performed: bad data
# 5    The value was changed as a result of QC
# 7    Nominal value
# 8    Interpolated value
# 9   The value is missing

# =============================================================================
# SOME USEFUL THINGS
# =============================================================================
cm = (25/64)
A4_portait = np.array([21,29.7])*cm
A4_paysage = np.array([29.7,21])*cm

def qc2c(qc):
    d_qc2c = {
        9 : "tab:grey",
        4 : "tab:red",
        3 : "tab:orange",
        1 : "tab:green"
        }
    return d_qc2c[qc]

# =============================================================================
# LOAD DATA
# =============================================================================
fname = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/data/validator/data_qc.csv"
data = pd.read_csv(fname)
data.utc_datetime = pd.to_datetime(data.utc_datetime)

keys = [e for e in list(data.keys()) if (not "datetime" in e) and (not "qc" in e)]

data.loc[:,("mdates")] = [mdates.date2num(e) for e in data.utc_datetime]


start = datetime.datetime.now()
# =============================================================================
# GRAPH method 1
# =============================================================================

fig, ax = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(*A4_paysage)

for i,key in enumerate(keys):
    col_qc = f"qc_{key}"
    qcs = list(data[col_qc].unique())
    for qc in qcs:
        ax[i].plot(data.mdates,data[key].where(data[col_qc]==qc),c=qc2c(qc),label=f"qc = {qc}")
    
    ax[i].set_ylabel(key)
    ax[i].grid(True)
    ax[i].legend()
    ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
        
fig.autofmt_xdate()
fig.tight_layout(pad=0)

end = datetime.datetime.now()
print(end-start)


# start = datetime.datetime.now()
# =============================================================================
# GRAPH method 2
# =============================================================================
    
# fig, ax = plt.subplots(len(keys),1,sharex=True)
# fig.set_size_inches(*A4_paysage)

# for i,key in enumerate(keys):
#     col_qc = f"qc_{key}"
#     qcs = list(data[col_qc].unique())
#     for qc in qcs:
#         #tmp = data[["utc_datetime",key]]
#         tmp = data[data[col_qc]==qc]
#         ax[i].plot(tmp.utc_datetime,tmp[key],c=qc2c(qc))
        

# fig.autofmt_xdate()
# fig.tight_layout(pad=0)

# end = datetime.datetime.now()
# print(end-start)


# tl.warn()