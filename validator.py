import read_ctd as ctd
import grapher
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
from six.moves import input as raw_input
import sys
import file_manager as fm
import tools as tl

#datetime temperature_degc salinity fluorescence_rfu turbidity_ntu oxygen_mgl

#config
data_ctd = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
datetime_name = "datetime"
data_ctd["mdates"] = [mdates.date2num(e) for e in data_ctd.datetime]

keys = list(data_ctd.keys())
keys.remove(datetime_name)
keys.remove("mdates")

#cut
#data_ctd = data_ctd[(data_ctd["datetime"]>"2020-01-10") & (data_ctd["datetime"]<"2020-01-18")]
#data_ctd = data_ctd[(data_ctd["datetime"]>"2020-11-17")]

#

#init
df_good=pd.DataFrame()
df_passed=pd.DataFrame()
df_good["datetime"] = data_ctd["datetime"]
df_passed["datetime"] = data_ctd["datetime"]
df_good[keys]=True # on crée et passe toute les colonnes dans keys à 0
df_passed[keys]=0 # on crée et passe toute les colonnes dans keys à 0

# =============================================================================
# BEGIN FILTERING
# =============================================================================

# %%=============================================================================
# filter 1 filter NaN
# =============================================================================
test = pd.DataFrame().reindex_like(df_good[keys])
test = ~data_ctd[keys].isnull()

# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 1 - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test[keys]
# incremente valeur du filtre passer en cas de succes
df_passed[keys] = df_passed[keys] + df_good[keys]

# %%=============================================================================
# filter 2 static filter
# =============================================================================
test = pd.DataFrame().reindex_like(df_good[keys])
test = (data_ctd[keys]>=0)
test["temperature_degc"] = test["temperature_degc"] & (data_ctd["temperature_degc"]<=30)
test["salinity"] = test["salinity"] & (data_ctd["salinity"]<=40)
test["fluorescence_rfu"] = test["fluorescence_rfu"] & (data_ctd["fluorescence_rfu"]<=10)
test["turbidity_ntu"] = test["turbidity_ntu"] & (data_ctd["turbidity_ntu"]<=100)
test["oxygen_mgl"] = test["oxygen_mgl"] & (data_ctd["oxygen_mgl"]<=100)

# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 2 - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test[keys]
# incremente valeur du filtre passer en cas de succes
df_passed[keys] = df_passed[keys] + df_good[keys]

# %%=============================================================================
# filter 3 filtering with slope
# =============================================================================
#datetime temperature_degc salinity fluorescence_rfu turbidity_ntu oxygen_mgl
slope = (data_ctd[keys]-data_ctd[keys].shift()).abs()
slope["datetime"]=data_ctd["datetime"]

test = pd.DataFrame().reindex_like(df_good[keys])
d_max_slope={
        "temperature_degc" : 1,
        "salinity" : 7.5,
        "fluorescence_rfu" : 10,
        "turbidity_ntu" : 25,
        "oxygen_mgl" : 1
        }
for key in keys:
    tmp_test = (slope[key]<=d_max_slope[key])
    test[key] = (tmp_test & (tmp_test.shift()))
    
# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 3 - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test[keys]
# incremente valeur du filtre passer en cas de succes
df_passed[keys] = df_passed[keys] + df_good[keys]

# %%=============================================================================
# filter 4 filtering with rolling std and rolling mean
# =============================================================================

import filter_temperature
import filter_salinity
import filter_fluo
import filter_turbi
import filter_oxy

test = pd.DataFrame().reindex_like(df_good[keys])
test.temperature_degc = filter_temperature.select
test.salinity = filter_salinity.select
test.fluorescence_rfu = filter_fluo.select
test.turbidity_ntu = filter_turbi.select
test.oxygen_mgl = filter_oxy.select

# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 4 - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test[keys]
# incremente valeur du filtre passer en cas de succes
df_passed[keys] = df_passed[keys] + df_good[keys]

# %%=============================================================================
# filter 5 manual
# =============================================================================
fname_manual = "/home/karnaphorion/2021/Univ/Stage/CoastHF/pylab/_build/manual_filter/manual_filter.csv" 
test = pd.read_csv(fname_manual)

# compte de le nombre de valeurs rejeté
sum_filtered = (~test & df_good[keys]).sum(axis=0)
print(f"filter 5 - amount of rejected values :\n{sum_filtered}")
# applique le resultat du filter à la mémoire good or bad
df_good[keys] = df_good[keys] & test[keys]
# incremente valeur du filtre passer en cas de succes
df_passed[keys] = df_passed[keys] + df_good[keys]

# =============================================================================
# END FILTERING
# =============================================================================

# %%=============================================================================
# SAVE DATA
# =============================================================================

#folder = "filter_save"
#current = os.getcwd()
#fullfolder = os.path.join(current,folder)
#if not os.path.exists(fullfolder):
#    os.mkdir(fullfolder)
#    print(f"mkdir {fullfolder}")
#
##if raw_input("File")    
#
#fgood = os.path.join(fullfolder,"df_good.csv")
#fpassed = os.path.join(fullfolder, "df_passed.csv")
#
#df_good.to_csv(fgood)
#df_passed.to_csv(fpassed)

root = os.getcwd()
fn_passed = fm.build_path(root,"data","validator","passed",ext=".csv")
df_passed.to_csv(fn_passed,index=False)

fn_good = fm.build_path(root, "data","validator","good",ext=".csv")
df_good.to_csv(fn_good,index=False)



# %%=============================================================================
# BEGIN GRAPH
# =============================================================================

#tl.warn()

# calc graph
threshold = 5
good_data = data_ctd[keys][df_passed[keys]==threshold]
bad_data = data_ctd[keys][~(df_passed[keys]==threshold)]

# =============================================================================
#%% graph raw + filter passed
# =============================================================================

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],data_ctd[key],label="RAW")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    axes2 = axes[i].twinx()
    axes2.plot(data_ctd["mdates"],df_passed[key],label="filter passed",c="tab:orange",alpha=0.5)
    axes2.set_ylabel("passed")
    axes2.legend()
    
plt.gcf().autofmt_xdate()
plt.title("raw data + slope")
#plt.savefig("raw_data_and_slope.png")


# =============================================================================
#%% graph raw + slope
# =============================================================================
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],data_ctd[key],label="RAW")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
    axes2 = axes[i].twinx()
    axes2.plot(data_ctd["mdates"],slope[key],label="slope",c="tab:orange",alpha=0.5)
    axes2.set_ylabel("slope")
    axes2.legend()
    
plt.gcf().autofmt_xdate()
plt.title("raw data + slope")
#plt.savefig("raw_data_and_slope.png")

# =============================================================================
#%% graph good and bad data
# =============================================================================
fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],good_data[key],label="good")
    axes[i].plot(data_ctd["mdates"],bad_data[key],label="bad")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
plt.gcf().autofmt_xdate()
plt.title("good and bad")
#plt.savefig("good_bad.png")

# =============================================================================
#%% graph only good data
# =============================================================================

fig, axes = plt.subplots(len(keys),1,sharex=True)
fig.set_size_inches(32*(25/64),24*(25/64))

for i,key in enumerate(keys):
    print(key)
    axes[i].plot(data_ctd["mdates"],good_data[key],label="good")
    #axes[i].plot(data_ctd["mdates"],bad_data[key],label="bad")
    axes[i].set_ylabel(key)
    axes[i].grid(True)
    axes[i].legend()
    axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    
plt.gcf().autofmt_xdate()
plt.title("only_good")
#plt.savefig("only_good.png")

plt.show()

# =============================================================================
# END GRAPH
# =============================================================================

#keys = ["temperature_degc"]
#
#fig, axes = plt.subplots(len(keys),1,sharex=True)
#fig.set_size_inches(32*(25/64),24*(25/64))
#
#for i,key in enumerate(keys):
#    print(key)
#    axes.plot(data_ctd["mdates"],good_data[key],label="good")
#    axes.plot(data_ctd["mdates"],bad_data[key],label="bad")
#    axes.set_ylabel(key)
#    axes.grid(True)
#    axes.legend()
#    axes.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
#    
#    axes2 = axes.twinx()
#    axes2.plot(data_ctd["mdates"],slope[key],label="slope",c="tab:orange",alpha=0.5)
#    axes2.set_ylabel("slope")
#    axes2.legend()
#    
#plt.gcf().autofmt_xdate()
#plt.title("good and bad")
#plt.savefig("good_bad.png")

