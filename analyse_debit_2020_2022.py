import load_hydroportail as lh
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import file_manager as fm
import pandas as pd

start = "01/01/2020"
end = "01/01/2022"

stations = [lh.station_Agly_Riveslates,lh.station_Baillaury_Banyuls,lh.station_Tech_Argeles,lh.station_Tet_Perpignan]

l_hs = []
for station in stations:
    l_hs.append(lh.HydroSeries(station=station,start=start,end=end))
    
names = [hs.name for hs in l_hs]
codes = [hs.station_code for hs in l_hs]
l_df = [hs.df_hydro_series for hs in l_hs if hs.status_code==200]

for i in range(len(codes)):
    l_df[i].rename(columns={"volume_l_s":f"volume_l_s_{codes[i]}"},inplace=True)
    l_df[i].drop(columns=["datetime"],inplace=True)
    l_df[i].set_index("mdates",inplace=True)
    
df_join = pd.DataFrame(index=l_df[0].index).join(l_df)

df_percent = df_join.divide(df_join.sum(axis=1), axis=0)*100


#%% graph
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))

for i in range(len(l_df)):
    ax.plot(l_df[i].index,l_df[i].iloc[:,0],label=names[i])
    
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.legend()
ax.set_ylabel("débit moyen journalier (l/s)")
    
fig.set_dpi(150.0)
fig.autofmt_xdate()
title = f"comparaison des débits des fleuves côtiers en P.O."
fig.suptitle(title)
fig.savefig("fig/"+fm.to_img_fname(title))

#%% graph stackplot
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))

ax.stackplot(df_join.index,df_join.iloc[:,0],df_join.iloc[:,1],df_join.iloc[:,2],df_join.iloc[:,3],labels=names,baseline="zero")
ax.set_ylabel("débits moyens journaliers cumulés (l/s)")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.legend()
#ax.semilogy()
    
fig.set_dpi(150.0)
fig.autofmt_xdate()
title = f"debits moyens journaliers cumulés des fleuves côtiers des P.O. (l/s)"
fig.suptitle(title)
fig.savefig("fig/"+fm.to_img_fname(title))

#%% graph stackplot
fig, ax = plt.subplots()
fig.set_size_inches(29.7*(25/64),21*(25/64))

ax.stackplot(df_percent.index,df_percent.iloc[:,0],df_percent.iloc[:,1],df_percent.iloc[:,2],df_percent.iloc[:,3],labels=names,baseline="zero")
ax.set_ylabel("Rapport (%)")
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
ax.legend()
    
fig.set_dpi(150.0)
fig.autofmt_xdate()
title = f"Rapport de débits moyens journaliers des fleuves côtiers en P.O."
fig.suptitle(title)
fig.savefig("fig/"+fm.to_img_fname(title))