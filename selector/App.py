import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.widgets import RectangleSelector, Button, SpanSelector
from matplotlib.patches import Rectangle
import read_ctd
import matplotlib.dates as mdates
from matplotlib.backend_tools import ToolToggleBase
from matplotlib.backend_managers import ToolManager
import os
from six.moves import input as raw_input

class App:
    def __init__(self,data,df_good):
        self.data = data
        self.df_good = df_good
        self.fig, self.ax = plt.subplots()
        self.display = Display(self)
        self.display.display()    
        self.buttonstatus = ButtonStatus()
        self.select = Selector(self)
        self.display.update()
        self.axbadd = plt.axes([0,0.95,0.1, 0.05])
        self.badd = Button(self.axbadd, 'Add')
        self.badd.on_clicked(self.buttonstatus.set_draw)
        self.axbrem = plt.axes([0.1,0.95,0.1, 0.05])
        self.brem = Button(self.axbrem, 'Remove')
        self.brem.on_clicked(self.buttonstatus.set_erase)
        self.axbsave = plt.axes([0.2,0.95,0.1, 0.05])
        self.bsave = Button(self.axbsave, 'Save')
        self.bsave.on_clicked(self.select.save)

class Display:
    def __init__(self,app):
        self.app = app
        self.fig = app.fig
        self.ax = app.ax
        self.data = app.data
        self.gooddata = None
        self.baddata = None
        
    def display(self):
        self.gooddata, = self.ax.plot(data.getx(),data.gety(),label="good data")
        self.baddata, = self.ax.plot(data.getx(),data.gety(),label="bad data")
        self.ax.grid(True)
        self.ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
        self.ax.xaxis.set_minor_locator(mdates.MonthLocator())
        self.ax.set_title(self.data.coly)
        self.ax.legend()
        self.fig.autofmt_xdate()
        self.fig.canvas.draw()
        
    def update(self):
        selected = self.app.select.selected
        self.gooddata.set_ydata(self.data.gety().where(selected[self.data.coly]))
        self.baddata.set_ydata(self.data.gety().where(~selected[self.data.coly]))
        self.fig.canvas.draw()
        pass

class ButtonStatus:
    def __init__(self):
        self.draw = True
        self.print_status()
        
    def toggle(self,id):
        print("buttonstatus toggle")
        self.draw = not self.draw
        self.print_status()
        
    def set_draw(self,id):
        self.draw = True
        self.print_status()
    
    def set_erase(self,id):
        self.draw = False
        self.print_status()
        
    def print_status(self):
        if self.draw :
            print("buttonstatus : set on draw")
        else :
            print("buttonstatus : set on erase")
        
class Selector:
    def __init__(self,app):
        self.app = app
        self.data = app.data
#        self.selected = pd.DataFrame(index=self.data.getdata().index)
#        self.selected[self.data.coly]=True
#        print(self.selected)
        self.selected = self.app.df_good
        self.span = SpanSelector(app.ax, self.oncall, direction="horizontal",rectprops=dict(facecolor='blue', alpha=0.3))
        self.status = app.buttonstatus
    def oncall(self,xmin,xmax):
        print("selector call")
        x0 = self.indexof(xmin)
        x1 = self.indexof(xmax)
        self.selected[self.data.coly][range(int(x0),int(x1+1))] = self.status.draw
        print(self.selected)
        self.app.display.update()
    def indexof(self,mdate):
        l = len(self.data.getx())
        x0 = self.data.getx()[0]
        x1 = self.data.getx()[l-1]
        step = (x1-x0)/l
        i = (mdate-x0)/step
        return i
    def save(self,id=0):
        #print(self.selected)
        if raw_input("Choose fname to save filter (y/n):") == 'y':
            fname = raw_input("fname : ")
        else :
            fname = "df_good_manual.csv"
        folder = "filter_save"
        current = os.getcwd()
        fullfolder = os.path.join(current,folder)
        fmanual = os.path.join(fullfolder,fname)
        print(f"Save filter in :\n{fmanual}")
        self.selected.to_csv(fmanual)
        
class Data:
    def __init__(self,df,colx,coly):
        self.df = df
        self.colx = colx
        self.coly = coly
    def getx(self):
        return self.df[self.colx]
    def gety(self):
        return self.df[self.coly]
    def getdata(self):
        return self.df[[self.colx,self.coly]]

if __name__ == '__main__':
    # try to load filter data
    folder = "filter_save"
    current = os.getcwd()
    fullfolder = os.path.join(current,folder)
    fgood = os.path.join(fullfolder,"df_good.csv")
    fpassed = os.path.join(fullfolder, "df_passed.csv")
    fmanual = os.path.join(fullfolder,"df_good_manual.csv")
            
    #if os.path.exists(fpassed):
    #    df_passed = pd.read_csv(fpassed.copy())
    
    plt.ion()
    data_ctd=read_ctd.ctd(read_ctd.fname_ctd_2020_med5min)
    data_ctd["mdates"] = [mdates.date2num(e) for e in data_ctd.datetime]
    colx = "mdates"
    #coly = "temperature_degc"
    
    datetime_name = "datetime"
    keys = list(data_ctd.keys())
    keys.remove(datetime_name)
    keys.remove("mdates")
    
    req = "Which param to work on ?\n"
    for i,key in enumerate(keys):
        stmp = f"{i} >\t{key}\n"
        req += stmp
    req +=":"
      
    index = int(raw_input(req))
    coly = keys[index]
    
    print(f"Working on {coly}...")
    
    
    if os.path.exists(fgood) and raw_input("Load filter from validator (y/n) (May Erase Everything): ")=='y' :
            df_good = pd.read_csv(fgood)
    elif os.path.exists(fmanual) and raw_input("Load filter from manual app (y/n): ")=='y' :
            df_good = pd.read_csv(fmanual)
    else :
        df_good = pd.DataFrame(True,index=data_ctd.index,columns=data_ctd.columns)
    
    data = Data(data_ctd,colx,coly)
    app = App(data,df_good)