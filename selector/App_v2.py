import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.widgets import RectangleSelector, Button, SpanSelector
from matplotlib.patches import Rectangle
import read_ctd
import matplotlib.dates as mdates
from matplotlib.backend_tools import ToolToggleBase
from matplotlib.backend_managers import ToolManager
import os
from six.moves import input as raw_input

class App:
    def __init__(self,fig,ax,data,df_good):
        self.data = data
        self.df_good = df_good
        self.fig = fig
        self.ax = axaxbadd
        self.display = Display(self)
        self.display.display()    
        self.buttonstatus = ButtonStatus()
        self.select = Selector(self)
        self.display.update()
        self.axbadd = plt.axes([0,0.95,0.1, 0.05])
        self.badd = Button(self.axbadd, 'Add')
        self.badd.on_clicked(self.buttonstatus.set_draw)
        self.axbrem = plt.axes([0.1,0.95,0.1, 0.05])
        self.brem = Button(self.axbrem, 'Remove')
        self.brem.on_clicked(self.buttonstatus.set_erase)
        self.axbsave = plt.axes([0.2,0.95,0.1, 0.05])
        self.bsave = Button(self.axbsave, 'Save')
        self.bsave.on_clicked(self.select.save)

class Display:
    def __init__(self,app):
        self.app = app
        self.fig = app.fig
        self.ax = app.ax
        self.data = app.data
        self.disp_selected = None
        
    def display(self):
        placeholder = self.data.gety().values
        vmax = placeholder.max()
        placeholder = placeholder/vmax
        self.disp_selected = self.ax.imshow([placeholder],extent=(list(self.ax.get_xlim()) + list(self.ax.get_ylim())),alpha=0.15,zorder=-20,cmap=plt.cm.get_cmap("bwr").reversed())
        self.ax.set_aspect("auto")
        self.fig.canvas.draw()
        
        
    def update(self):
        selected = self.app.select.selected
        self.disp_selected.set_data([selected])
        self.ax.set_aspect("auto")
        self.fig.canvas.draw()
        
class ButtonStatus:
    def __init__(self):
        self.draw = True
        self.print_status()
        
    def toggle(self,id):
        #print("buttonstatus toggle")
        self.draw = not self.draw
        self.print_status()
        
    def set_draw(self,id):
        self.draw = True
        self.print_status()
    
    def set_erase(self,id):
        self.draw = False
        self.print_status()
        
    def print_status(self):
        if self.draw :
            #print("buttonstatus : set on draw")
            pass
        else :
            #print("buttonstatus : set on erase")
            pass
        
class Selector:
    def __init__(self,app):
        self.app = app
        self.data = app.data
#        self.selected = pd.DataFrame(index=self.data.getdata().index)
#        self.selected[self.data.coly]=True
#        print(self.selected)
        self.selected = self.app.df_good
        self.span = SpanSelector(app.ax, self.oncall, direction="horizontal",rectprops=dict(facecolor='blue', alpha=0.3))
        self.status = app.buttonstatus
    def oncall(self,xmin,xmax):
        #print("selector call")
        x0 = self.indexof(xmin)
        x1 = self.indexof(xmax)
        self.selected[self.data.coly][range(int(x0),int(x1+1))] = self.status.draw
        #print(self.selected)
        self.app.display.update()
    def indexof(self,mdate):
        l = len(self.data.getx())
        x0 = self.data.getx()[0]
        x1 = self.data.getx()[l-1]
        step = (x1-x0)/l
        i = (mdate-x0)/step
        return i
    def save(self,id=0):
        #print(self.selected)
        if raw_input("Choose fname to save filter (y/n):") == 'y':
            fname = raw_input("fname : ")
        else :
            fname = "df_good_manual.csv"
        folder = "filter_save"
        current = os.getcwd()
        fullfolder = os.path.join(current,folder)
        fmanual = os.path.join(fullfolder,fname)
        #print(f"Save filter in :\n{fmanual}")
        self.selected.to_csv(fmanual)
        
class Data:
    def __init__(self,df,colx,coly):
        self.df = df
        self.colx = colx
        self.coly = coly
    def getx(self):
        return self.df[self.colx]
    def gety(self):
        return self.df[self.coly]
    def getdata(self):
        return self.df[[self.colx,self.coly]]

def test():
    v=np.zeros(10)
    v[[np.arange(1,9,2)]]=1
    
    x = np.arange(0,9,1)
    y = x
    
    fig, ax = plt.subplots()
    ax.plot(x,y,marker="x")
    df = pd.DataFrame()
    df["x"]=x
    df["y"]=y
    good = pd.DataFrame(True,index=df.index,columns=["y"])
    
    data = Data(df,"x","y")
    #print("start")
    app = App(fig,ax,data,good)
    selected = app.select.selected

if __name__ == '__main__':
    print(test())