import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize
from matplotlib.widgets import  Button, SpanSelector
from matplotlib.colors import ListedColormap
from time import sleep
import shutil

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm

from size import A4_paysage

# =============================================================================
# FUNCTIONS
# =============================================================================
def num2str(num):
    return mdates.num2date(num).strftime("%Y-%m-%dT%H:%M")

def onselect(*args):
    print(args)
    print([ num2str(e) for e in args])

# =============================================================================
# LOAD DATA
# =============================================================================

data = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)
#data = ctd.ctd(ctd.fname_SOLA_ctd_2021)
#data = cmp.cmp(cmp.fname_POEM_ctd_2020_2021_good)

#%%

x = data.datetime
y = data.salinity

# =============================================================================
# SELECTOR
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(*A4_paysage)

ax.plot(x,y)

ax.grid(True)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
#x.xaxis.set_major_locator(mdates.MonthLocator())
#ax.xaxis.set_minor_locator(mdates.MonthLocator())
#ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)
fig.suptitle("Selector")

ss = SpanSelector(ax, onselect, "horizontal")


