import load_copernicus as coper
import pandas as pd

timeframe = pd.to_datetime(["2021-09-01","2021-10-01"])

satds = coper.SatDataSet(dataset=coper.dataset_med_cmcc_ssh_rean_d)

zone = [43.671118 , 5.163132, 42.463854 , 2.923778]

satds.getparams()

#print(satds.getchunks('zos', *zone, *timeframe))
            
print(satds.iszone_inbounds(zone))

print(satds.getgeobounds())

print(satds.istimeframe_inbounds(timeframe))

print(satds.gettimebounds())