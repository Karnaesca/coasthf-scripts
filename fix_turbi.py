import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os
import sys
from matplotlib.colors import Normalize

import load_hydroportail as hydro
import read_ctd as ctd
import read_met as met
import read_chemistry as chem
import read_profil_ctd as pctd
import read_composite as cmp
import file_manager as fm
import tools as tl
from size import A4_paysage, A4_portait, cm
from matplotlib.patches import Rectangle

import pybaselines 


# =============================================================================
# LOAD DATA
# =============================================================================

data = ctd.ctds(ctd.fname_POEM_ctd_2020,ctd.fname_POEM_ctd_2021)[["datetime","turbidity_ntu"]]

#%%
# =============================================================================
# SELECT THE ZONE TO FIX
# =============================================================================

cuts = [
    ['2020-10-06T07:51', '2020-11-04T17:02']
    ]

meds = [
        ['2020-10-03T04:51', '2020-10-06T05:07'],
        ['2020-10-06T11:01', '2020-10-09T08:21']
        ]

# =============================================================================
# APPLY FIX
# =============================================================================

data_fixed = data.copy()

cut = cuts[0]
med0 = tl.timecut(data, *meds[0]).turbidity_ntu.median()
med1 = tl.timecut(data, *meds[1]).turbidity_ntu.median()
offset = float(med1-med0)
select = ((data_fixed.datetime>=cut[0]) & (data_fixed.datetime<cut[1]))
data_fixed.turbidity_ntu[select] = data_fixed.turbidity_ntu[select] - offset

# =============================================================================
# SAVE FIX
# =============================================================================

root = os.getcwd()
fname = fm.build_path(root,"data","recalibrated","POEM_turbi_fix",ext=".csv")
data_fixed.to_csv(fname,index=False)

# =============================================================================
# GRAPH
# =============================================================================

fig, ax = plt.subplots()
fig.set_size_inches(21*cm,7*cm)

ax.plot(data.datetime,data.turbidity_ntu,label="raw")
ax.plot(data_fixed.datetime,data_fixed.turbidity_ntu,label="fixed")
# ax.set_ylabel(tl.col2name("turbidity_ntu"))

ax.grid(True)
# ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
# ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.legend()

fig.autofmt_xdate()
fig.tight_layout(pad=0)